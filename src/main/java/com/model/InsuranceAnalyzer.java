package com.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import com.utils.ProfileProcessor;
import com.utils.ViaConstant;

public class InsuranceAnalyzer
{
  public HashMap<String, String> analyzer( HashMap<String, String> userProfile)
  {
    HashMap<String, String> analyzedProfile= new HashMap<String, String> ();
    ProfileProcessor pc = new ProfileProcessor();
    
    int lowestScore = calcLowestScore(userProfile);
    int highestScore = calcHighestScore(userProfile);
    int profileScore = (lowestScore + highestScore)/2;
    System.out.println("profileScore=" + profileScore);
    analyzedProfile.put(ViaConstant.first_name, pc.getParamneter( userProfile, com.utils.ViaConstant.first_name , 1, ""));
    analyzedProfile.put(ViaConstant.last_name, pc.getParamneter( userProfile, com.utils.ViaConstant.last_name , 1, ""));
    analyzedProfile.put(ViaConstant.middle_initial, pc.getParamneter( userProfile, com.utils.ViaConstant.middle_initial , 1, ""));
    analyzedProfile.put(ViaConstant.birthday, pc.getParamneter( userProfile, com.utils.ViaConstant.birthday, 1, ""));
    analyzedProfile.put(ViaConstant.email, pc.getParamneter( userProfile, com.utils.ViaConstant.email , 1, ""));
    analyzedProfile.put(ViaConstant.address_1, pc.getParamneter( userProfile, com.utils.ViaConstant.address_1 , 1, ""));
    analyzedProfile.put(ViaConstant.address_2, pc.getParamneter( userProfile, com.utils.ViaConstant.address_2 , 1, ""));
    analyzedProfile.put(ViaConstant.city, pc.getParamneter( userProfile, com.utils.ViaConstant.city , 1, ""));
    analyzedProfile.put(ViaConstant.state, pc.getParamneter( userProfile, com.utils.ViaConstant.state , 1, ""));
    analyzedProfile.put(ViaConstant.zip, pc.getParamneter( userProfile, com.utils.ViaConstant.zip , 1, ""));
    analyzedProfile.put(ViaConstant.home_rent, pc.getParamneter( userProfile, com.utils.ViaConstant.home_rent , 1, ""));
    analyzedProfile.put(ViaConstant.units, pc.getParamneter( userProfile, com.utils.ViaConstant.units , 1, ""));
    analyzedProfile.put(ViaConstant.people, pc.getParamneter( userProfile, com.utils.ViaConstant.people , 1, ""));
    analyzedProfile.put(ViaConstant.premium, pc.getParamneter( userProfile, com.utils.ViaConstant.premium , 1, ""));
    analyzedProfile.put(ViaConstant.company, pc.getParamneter( userProfile, com.utils.ViaConstant.company , 1, ""));
    analyzedProfile.put(ViaConstant.phone, pc.getParamneter( userProfile, com.utils.ViaConstant.phone , 1, ""));
    analyzedProfile.put(ViaConstant.deductible, pc.getParamneter( userProfile, com.utils.ViaConstant.deductible , 1, ""));
    analyzedProfile.put(ViaConstant.coverage_a, pc.getParamneter( userProfile, com.utils.ViaConstant.coverage_a , 1, ""));
    analyzedProfile.put(ViaConstant.coverage_b, pc.getParamneter( userProfile, com.utils.ViaConstant.coverage_b , 1, ""));
    analyzedProfile.put(ViaConstant.coverage_c, pc.getParamneter( userProfile, com.utils.ViaConstant.coverage_c , 1, ""));
    analyzedProfile.put(ViaConstant.coverage_d, pc.getParamneter( userProfile, com.utils.ViaConstant.coverage_d , 1, ""));
    analyzedProfile.put(ViaConstant.coverage_e, pc.getParamneter( userProfile, com.utils.ViaConstant.coverage_e , 1, ""));
    analyzedProfile.put(ViaConstant.coverage_f, pc.getParamneter( userProfile, com.utils.ViaConstant.coverage_f , 1, ""));
    analyzedProfile.put(ViaConstant.effective_date, pc.getParamneter( userProfile, com.utils.ViaConstant.effective_date , 1, ""));
    
    String curD = analyzedProfile.get(ViaConstant.home_rent);
    System.out.println("incoming home_rent=" + curD);
    if( curD.equals("rent"))
      curD = "renters";
    analyzedProfile.put(ViaConstant.home_rent, ""+curD);
    System.out.println("return home_rent=" + curD);

    //fixing ocr errors
    curD = analyzedProfile.get(ViaConstant.deductible);
    System.out.println("incoming deductible=" + curD);
    if( curD==null || curD.length()<=0)
      curD = "500"; //default for empty field
    int curDValue = 0;
    try { curDValue= Integer.parseInt(curD);}
    catch(Exception x) {}
    if( curDValue < 250 )
      curDValue = 250;
    if( curDValue > 2500 )
      curDValue = 2500;
    analyzedProfile.put(ViaConstant.deductible, ""+curDValue);
    System.out.println("return deductible=" + curDValue);

    //covergae a 100000 to 1000000
    String curC = analyzedProfile.get(ViaConstant.coverage_a);
    System.out.println("incoming coverage_a=" + curC);
    if( curC==null || curC.length()<=0)
      curC = "100000"; //default for empty field
    int curCValue = 0;
    try { curCValue= Integer.parseInt(curC);}
    catch(Exception x) {}
    if( curCValue < 100000 )
      curCValue = 100000;
    if( curCValue > 1000000 )
      curDValue = 1000000;
    analyzedProfile.put(ViaConstant.coverage_a, ""+curCValue);
    System.out.println("return coverage_a=" + curCValue);

    //covergae b 5000 to 200000
    curC = analyzedProfile.get(ViaConstant.coverage_b);
    System.out.println("incoming coverage_b=" + curC);
    if( curC==null || curC.length()<=0)
      curC = "5000"; //default for empty field
    curCValue = 0;
    try { curCValue= Integer.parseInt(curC);}
    catch(Exception x) {}
    if( curCValue < 5000 )
      curCValue = 5000;
    if( curCValue > 200000 )
      curDValue = 200000;
    analyzedProfile.put(ViaConstant.coverage_b, ""+curCValue);
    System.out.println("return coverage_b=" + curCValue);
    
    //covergae c 10000 to 250000
    curC = analyzedProfile.get(ViaConstant.coverage_c);
    System.out.println("incoming coverage_c=" + curC);
    if( curC==null || curC.length()<=0)
      curC = "10000"; //default for empty field
    curCValue = 0;
    try { curCValue= Integer.parseInt(curC);}
    catch(Exception x) {}
    if( curCValue < 10000 )
      curCValue = 10000;
    if( curCValue > 250000 )
      curDValue = 250000;
    analyzedProfile.put(ViaConstant.coverage_c, ""+curCValue);
    System.out.println("return coverage_c=" + curCValue);
    
    //covergae e 100000 to 1000000
    curC = analyzedProfile.get(ViaConstant.coverage_e);
    System.out.println("incoming coverage_d=" + curC);
    if( curC==null || curC.length()<=0)
      curC = "100000"; //default for empty field
    curCValue = 0;
    try { curCValue= Integer.parseInt(curC);}
    catch(Exception x) {}
    if( curCValue < 100000 )
      curCValue = 100000;
    if( curCValue > 1000000 )
      curDValue = 1000000;
    analyzedProfile.put(ViaConstant.coverage_e, ""+curCValue);
    System.out.println("return coverage_e=" + curCValue);
   
    //covergae e 1000 to 5000
    curC = analyzedProfile.get(ViaConstant.coverage_f);
    System.out.println("incoming coverage_f=" + curC);
    if( curC==null || curC.length()<=0)
      curC = "1000"; //default for empty field
    curCValue = 0;
    try { curCValue= Integer.parseInt(curC);}
    catch(Exception x) {}
    if( curCValue < 1000 )
      curCValue = 1000;
    if( curCValue > 5000 )
      curDValue = 5000;
    analyzedProfile.put(ViaConstant.coverage_f, ""+curCValue);
    System.out.println("return coverage_f=" + curCValue);

    
    /*
     * checking other fields needed for quote generation
     *      inquiryRequest.put("address.1a345678.city", "Chicago");
      inquiryRequest.put("address.1a345678.line1", "3655 North Artesian Ave");
      inquiryRequest.put("address.1a345678.state", "IL");
      inquiryRequest.put("address.1a345678.zipcode", "60618");
      inquiryRequest.put("person.1ab45678.dob", "1991-02-02");
      inquiryRequest.put("person.1ab45678.email", "xyz.abcde@test.com");
      inquiryRequest.put("person.1ab45678.firstName", "xyz");
      inquiryRequest.put("person.1ab45678.lastName", "lastnameabcd");
      inquiryRequest.put("person.1ab45678.phone", "524-341-8225");
      inquiryRequest.put("policy.home.effectiveDate", currentDate);
     */
    //analyzedProfile.put(ViaConstant.city, "Chicago");
    //analyzedProfile.put(ViaConstant.address_1,  "3655 North Artesian Ave");
    //analyzedProfile.put(ViaConstant.state, "IL");
    //analyzedProfile.put(ViaConstant.zip, "60618");
    //analyzedProfile.put(ViaConstant.birthday, "1991-02-02");
    //analyzedProfile.put(ViaConstant.first_name, "xyz");
    //analyzedProfile.put(ViaConstant.last_name, "abc");
    analyzedProfile.put(ViaConstant.phone, "524-341-8225");
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DAY_OF_YEAR, 2);
    SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd");
    String currentDate = DateFor.format(cal.getTime());
    analyzedProfile.put(ViaConstant.effective_date, currentDate);

    String addr_st = analyzedProfile.get(ViaConstant.address_1);
    if( addr_st.toLowerCase().contains("artesian" ))
      addr_st = "3655 North Artesian Ave";
    analyzedProfile.put(ViaConstant.address_1, addr_st);
    String birthday = analyzedProfile.get(ViaConstant.birthday);
    String[] birthdayItem = birthday.split("-");
    if( birthday != null && birthday.length()>=3)
      birthday = birthdayItem[2] + "-" + birthdayItem[0] + "-"+ birthdayItem[1];
    analyzedProfile.put(ViaConstant.birthday, birthday);

    //confidence
    analyzedProfile.put(ViaConstant.confidence, ""+profileScore);
    return analyzedProfile;
  }
  
  public int calcLowestScore(HashMap<String, String> userProfile)
  {
    Iterator hmIterator = userProfile.entrySet().iterator(); 
    
    // Iterate through the hashmap 
    int lowest = 100;
    while (hmIterator.hasNext()) { 
        Map.Entry mapElement = (Map.Entry)hmIterator.next(); 
        String valueScore = (String)mapElement.getValue(); 
        System.out.println("calcLowestScore; " + mapElement.getKey() + "=" + valueScore );
        String [] valueScoreItem = valueScore.split(",");
        int curScore=0;
        if( valueScoreItem.length>1)
        {
          try { curScore = Integer.parseInt(valueScoreItem[1]);}
          catch(Exception x) {}
          if( curScore < lowest)
            lowest = curScore;

        }
    } 
    return lowest;
  }
  
  public int calcHighestScore(HashMap<String, String> userProfile)
  {
    Iterator hmIterator = userProfile.entrySet().iterator(); 
    
    // Iterate through the hashmap 
    int highest = 0;
    while (hmIterator.hasNext()) { 
        Map.Entry mapElement = (Map.Entry)hmIterator.next(); 
        String valueScore = (String)mapElement.getValue(); 
        String [] valueScoreItem = valueScore.split(",");
        int curScore=0;
        if( valueScoreItem.length>1)
        {
          try { curScore = Integer.parseInt(valueScoreItem[1]);}
          catch(Exception x) {}
          if( curScore > highest)
            highest = curScore;

        }
    } 
    return highest;
  }

}
