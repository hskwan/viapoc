package com.model;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rsocket.QuoteMessage;
//import com.rsocket.Rclient;
import com.utils.HttpClient;
//import com.utils.ProfileProcessor;
import com.utils.ViaConstant;
//import io.rsocket.Payload;
//import io.rsocket.RSocket;
//import io.rsocket.core.RSocketConnector;
//import io.rsocket.transport.netty.client.TcpClientTransport;
//import io.rsocket.util.DefaultPayload;
import net.minidev.json.JSONObject;

public class QuoteGenerator
{
  public HashMap<String, String> genQuote( HashMap<String, String> analyzedProfile)
  
  {
    return doQuoteMsg(analyzedProfile);
  }
  
  public HashMap<String, String> doQuoteMsg(HashMap<String, String> analyzedProfile)
  {

    //Calcuating effectiveDate
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DAY_OF_YEAR, 1);
    SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd");
    String currentDate = DateFor.format(cal.getTime());

    //Building a json object (Can be done any way you like as long as the result is a JSON string.)
    JSONObject inquiryRequest = new JSONObject();
    // if genQuote is for REGEN :
    // then only put offerSetId and quoteSetID
    inquiryRequest.put("address.1a345678.city", analyzedProfile.get(ViaConstant.city));
    inquiryRequest.put("address.1a345678.line1",  analyzedProfile.get(ViaConstant.address_1));
    inquiryRequest.put("address.1a345678.state",  analyzedProfile.get(ViaConstant.state));
    inquiryRequest.put("address.1a345678.zipcode",  analyzedProfile.get(ViaConstant.zip));
    inquiryRequest.put("person.1ab45678.dob",  analyzedProfile.get(ViaConstant.birthday));
    inquiryRequest.put("person.1ab45678.email",  analyzedProfile.get(ViaConstant.email));
    inquiryRequest.put("person.1ab45678.firstName",  analyzedProfile.get(ViaConstant.first_name));
    inquiryRequest.put("person.1ab45678.lastName",  analyzedProfile.get(ViaConstant.last_name));
    inquiryRequest.put("person.1ab45678.phone",  analyzedProfile.get(ViaConstant.phone));
    inquiryRequest.put("policy.home.effectiveDate", analyzedProfile.get(ViaConstant.effective_date));
    inquiryRequest.put("primaryInsured.person.creditDisclosureFlag", "true");
    //inquiryRequest.put("product.lob",  analyzedProfile.get(ViaConstant.home_rent));
    inquiryRequest.put("product.lob",  analyzedProfile.get(ViaConstant.home_rent));

/*
 *  "partner.key": "7e7db4b4-d5ee-4ccc-9836-cd68a079de1a",
    "partner.channel": "Mobile",
    "address.1a345678.city": "Chicago",
    "address.1a345678.line1": "3655 North Artesian Ave",
    "address.1a345678.line2": "",
    "address.1a345678.state": "IL",
    "address.1a345678.zipcode": "60618",
    "exteriorWall.8atcioxv.constructionMaterial": "Vinyl siding",
    "exteriorWall.8atcioxv.constructionMaterialPercentage": "100",
    "floor.8atcioxv.percentage": "100",
    "floor.8atcioxv.type": "Hardwood",
    "foundation.8atcioxv.foundationType": "Slab",
    "foundation.8atcioxv.percentage": "100",
    "heatingSource.1a345678.material": "Prefabricated/Zero Clearance",
    "heatingSource.1a345678.type": "Built-in fireplace",
    "interiorWall.8atcioxv.material": "Drywall/Veneer Plaster",
    "interiorWall.8atcioxv.percentage": "100",
    "person.1ab45678.dob": "1991-02-02",
    "person.1ab45678.email": "xyz.abcde@test.com",
    "person.1ab45678.firstName": "xyz",
    "person.1ab45678.lastName": "lastnameabcd",
    "person.1ab45678.phone": "524-341-8225",
    "policy.renters.effectiveDate": "2020-07-29",
    "primaryInsured.address.lock": "true",
    "primaryInsured.address.ref": "address.1a345678",
    "primaryInsured.person.lock": "true",
    "primaryInsured.person.ref": "person.1ab45678",
    "property.airConditioning": "true",
    "property.ceiling.cathedralOrVaulted": "0",
    "property.ceiling.height": "8 feet or less (most common)",
    "property.ceiling.roomsWithCrownMolding": "0",
    "property.exteriorWall.ref": "exteriorWall.8atcioxv",
    "property.floor.ref": "floor.8atcioxv",
    "property.foundation.percentageFinished": "0",
    "property.foundation.ref": "foundation.8atcioxv",
    "property.fullBaths": "2",
    "property.garage.size": "1 Car",
    "property.garage.type": "Attached",
    "property.halfBaths": "0",
    "property.heating.primaryType": "Electric",
    "property.heatingSource.ref": "heatingSource.1a345678",
    "property.homeBuiltOnSlope": "false",
    "property.interiorWall.ref": "interiorWall.8atcioxv",
    "property.kitchenCountertopMaterial": "Tile",
    "property.livingArea": "1900",
    "property.numberOfStories": "One",
    "property.roof.installOrReplace": "2012",
    "property.roof.material": "Slate",
    "property.roof.shape": "Gable",
    "property.singleOrMultiFamily": "1 Single Family",
    "property.styleOfHome": "Colonial",
    "property.yearBuilt": "1991",
    "product.lob": "renters",
    "property.fireplace": "no",
    "primaryInsured.person.creditDisclosureFlag": "true"
 */
    //Connect to the quote generator and send the payload (JSON)
    HttpClient hc = new HttpClient();
    String url = "http://localhost:8081/v1/quotegenerator/get-quote";
    String contentType = "application/json";
    String body = inquiryRequest.toString();
    boolean debug = true;
    String quoteResponse  = hc.doPostContentType(  url,  body,  contentType, debug);
    System.out.println("QuoteGenerator: quoteResponse=" + quoteResponse);

    QuoteMessage newQuote = null;
    try {
    //Map the resulting string into an object for easy access
    newQuote = new ObjectMapper().readValue(quoteResponse, QuoteMessage.class);
    }catch(Exception x)
    {
      x.printStackTrace();
    }
    if( newQuote!=null)
    {
      if( newQuote.errors != null)
      {
        analyzedProfile.put( ViaConstant.status, "ERROR:" + newQuote.errors.toString());
        analyzedProfile.put( ViaConstant.premium, "");
      }
      else
      {
        analyzedProfile.put( ViaConstant.status, "OK");
        DecimalFormat df2 = new DecimalFormat("#.##");
        String newPremium = df2.format(newQuote.premium);
        // inserted the new generated values of following from quote Generator response - GK - 08/07/2020	
        String coverage_a = df2.format(newQuote.coverage_a);
        String coverage_b = df2.format(newQuote.coverage_b);
        String coverage_c = df2.format(newQuote.coverage_c);
        String coverage_d = df2.format(newQuote.coverage_d);
        String coverage_e = df2.format(newQuote.coverage_e);
        String coverage_f = df2.format(newQuote.coverage_f);
        //df2.format....then deductible null
        String deductible = newQuote.deductible;
        String inquiryId = newQuote.inquiryId;
        String offerSetId = newQuote.offerSetId;
        
        String[] coverage_a_option = newQuote.coverage_a_option;
        String[] coverage_b_option = newQuote.coverage_b_option;
        String[] coverage_c_option = newQuote.coverage_c_option;
        String[] coverage_d_option = newQuote.coverage_d_option;
        String[] coverage_e_option = newQuote.coverage_e_option;
        String[] coverage_f_option = newQuote.coverage_f_option;
        //check if need to df2.format
        //String coverage_c_option = newQuote.coverage_c_option;
        //String[] coverage_c_option = newQuote.coverage_c_option;
        if( analyzedProfile.get(ViaConstant.home_rent).equals("renters"))
        {
          analyzedProfile.put( ViaConstant.premium, newPremium + "/year");
          // inserted the new generated values of following from quote Generator response - GK - 08/07/2020
          analyzedProfile.put( ViaConstant.coverage_a, coverage_a);
          if (coverage_b != null && !coverage_b.isEmpty()) analyzedProfile.put( ViaConstant.coverage_b, coverage_b);
          else analyzedProfile.put( ViaConstant.coverage_b, "");
          
          analyzedProfile.put( ViaConstant.coverage_c, coverage_c);
          analyzedProfile.put( ViaConstant.coverage_d, coverage_d);
          analyzedProfile.put( ViaConstant.coverage_e, coverage_e);
          analyzedProfile.put( ViaConstant.coverage_f, coverage_f);
          //analyzedProfile.put( ViaConstant.deductible, deductible);
          //if (deductible != null && !deductible.isEmpty()) analyzedProfile.put( ViaConstant.deductible, deductible);
          //else analyzedProfile.put( ViaConstant.deductible, "");
          if (inquiryId != null && !inquiryId.isEmpty()) analyzedProfile.put( ViaConstant.inquiryId, inquiryId);
          else analyzedProfile.put( ViaConstant.inquiryId, "");
          if (offerSetId != null && !offerSetId.isEmpty()) analyzedProfile.put( ViaConstant.offerSetId, offerSetId);
          else analyzedProfile.put( ViaConstant.offerSetId, "");
          //if (coverage_c_option != null) analyzedProfile.put( ViaConstant.coverage_c_option, coverage_c_option);
          //else analyzedProfile.put( ViaConstant.coverage_c_option, {""});
          if (deductible != null && !deductible.isEmpty()) analyzedProfile.put( ViaConstant.deductible, deductible);
          else analyzedProfile.put( ViaConstant.deductible, "");
          // covert to string since coverage_f_option is an array
          //analyzedProfile.put( ViaConstant.coverage_f_option, Arrays.toString(coverage_f_option));
          analyzedProfile.put( ViaConstant.coverage_f_option, Arrays.toString(new String[4]));
        }
        else
        {
          analyzedProfile.put( ViaConstant.premium, newPremium + "/year");
          // inserted the new generated values of following from quote Generator response - GK - 08/07/2020
          
          analyzedProfile.put( ViaConstant.coverage_a, coverage_a);
          //analyzedProfile.put( ViaConstant.coverage_b, coverage_b);
          if (coverage_b != null && !coverage_b.isEmpty()) analyzedProfile.put( ViaConstant.coverage_b, coverage_b);
          else analyzedProfile.put( ViaConstant.coverage_b, "");
          analyzedProfile.put( ViaConstant.coverage_c, coverage_c);
          analyzedProfile.put( ViaConstant.coverage_d, coverage_d);
          analyzedProfile.put( ViaConstant.coverage_e, coverage_e);
          analyzedProfile.put( ViaConstant.coverage_f, coverage_f);
          if (inquiryId != null && !inquiryId.isEmpty()) analyzedProfile.put( ViaConstant.inquiryId, inquiryId);
          else analyzedProfile.put( ViaConstant.inquiryId, "");
          //analyzedProfile.put( ViaConstant.deductible, deductible);
          if (offerSetId != null && !offerSetId.isEmpty()) analyzedProfile.put( ViaConstant.offerSetId, offerSetId);
          else analyzedProfile.put( ViaConstant.offerSetId, "");
          if (deductible != null && !deductible.isEmpty()) analyzedProfile.put( ViaConstant.deductible, deductible);
          else analyzedProfile.put( ViaConstant.deductible, "");
          
          if (coverage_a_option != null && coverage_a_option.length != 0) analyzedProfile.put( ViaConstant.coverage_a_option, Arrays.toString(coverage_a_option));
          else analyzedProfile.put( ViaConstant.coverage_a_option, "");
          
          if (coverage_b_option != null && coverage_b_option.length != 0) analyzedProfile.put( ViaConstant.coverage_b_option, Arrays.toString(coverage_b_option));
          else analyzedProfile.put( ViaConstant.coverage_b_option, "");
          
          if (coverage_c_option != null && coverage_c_option.length != 0) analyzedProfile.put( ViaConstant.coverage_c_option, Arrays.toString(coverage_c_option));
          else analyzedProfile.put( ViaConstant.coverage_c_option, "");
          
          if (coverage_d_option != null && coverage_d_option.length != 0) analyzedProfile.put( ViaConstant.coverage_d_option, Arrays.toString(coverage_d_option));
          else analyzedProfile.put( ViaConstant.coverage_d_option, "");
          
          if (coverage_e_option != null && coverage_e_option.length != 0) analyzedProfile.put( ViaConstant.coverage_e_option, Arrays.toString(coverage_e_option));
          else analyzedProfile.put( ViaConstant.coverage_e_option, "");
          
          if (coverage_f_option != null && coverage_f_option.length != 0) analyzedProfile.put( ViaConstant.coverage_f_option, Arrays.toString(coverage_f_option));
          else analyzedProfile.put( ViaConstant.coverage_f_option, "");
          //analyzedProfile.put( ViaConstant.coverage_f_option, Arrays.toString(new String[4]));
           
           
        }
      }
      
    }
    else
    {
      analyzedProfile.put( ViaConstant.status, "ERROR");
      analyzedProfile.put( ViaConstant.premium, "");
    }
     analyzedProfile.put( ViaConstant.company, "Homesite-G");
     return analyzedProfile;

  }

}
