package com.bean;


public class UserContext {

  
  
  private String firstName;
  private String lastName;
  private String email;
  private String birthday;
  private String zip;
  private String city;
  private String state;
  private String address1;
  private String address2;
  private int personal;
  private int deductible;
  private int liability;
  private float quote;
  
   

  public UserContext() {
    firstName="";
    lastName="";
    email="";
    birthday="";
    zip="";
    city="";
    state="";
    address1="";
    address2="";
    personal=0;
    deductible=0;
    liability=0;
    quote= (float) 0.0;
       
  }
   
  public Float getQuote() {
      return quote;
  }

  public void setQuote(Float quote) {
      this.quote = quote;
  }

  public int getLiability() {
      return liability;
  }

  public void setLiability(int liability) {
      this.liability = liability;
  }

  public int getDeductible() {
      return deductible;
  }

  public void setDeductible(int deductible) {
      this.deductible = deductible;
  }

  public int getpersonal() {
      return personal;
  }

  public void setPersonal(int personal) {
      this.personal = personal;
  }

  public String getAddress2() {
      return address2;
  }

  public void setAddress2(String address2) {
      this.address2 = address2;
  }
  
  public String getAddress1() {
    return address1;
}

public void setAddress1(String address1) {
    this.address1 = address1;
}

  public String getCity() {
      return city;
  }

  public void setCity(String city) {
      this.city = city;
  }
  public String getZip() {
      return zip;
  }

  public void setZip(String zip) {
      this.zip = zip;
  }

  public String getState() {
      return state;
  }

  public void setState(String state) {
      this.state = state;
  }
  public String getEmail() {
      return email;
  }

  public void setEmail(String email) {
      this.email = email;
  }


  
  public String getBirthday() {
      return birthday;
  }

  public void setBirthday(String birthday) {
      this.birthday = birthday;
  }
  
  public String getFirstName() {
    return firstName;
}

  public void setFirstName(String firstName) {
    this.firstName = firstName;
}
  
  public String getLastName() {
    return lastName;
}

  public void setLastName(String lastName) {
    this.lastName = lastName;
}

}
