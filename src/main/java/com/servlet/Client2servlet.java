package com.servlet;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import com.utils.Command;
import com.utils.HttpClient;
import com.utils.Initialize;
import com.utils.ViaConstant;
import com.utils.Vjson;

@WebServlet(urlPatterns = { "/client2servlet" })
public class Client2servlet extends HttpServlet 
{
  private static final long serialVersionUID = 1L;

  // location to store file uploaded
  //private static final String UPLOAD_DIRECTORY = "../../data";

  // upload settings
  private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
  private static final int MAX_FILE_SIZE      = 1024 * 1024 * 1000; // 100MB
  private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 1000; // 100MB

  /**
   * Upon receiving file upload submission, parses the request to read
   * upload data and saves the file on disk.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException 
  {
    String errorString ="";
    String debugString ="";
    String statusString="";
    String translate="";
    String translateCommand = "asyncrecognize";
    String mode="";
    //String uploadFileAction = request.getParameter("uploadFileAction");
    //debugString += "DEBUG: uploadFileAction=" + uploadFileAction;

    // checks if the request actually contains upload file

    if (!ServletFileUpload.isMultipartContent(request)) {
      // if not, we stop here
      PrintWriter writer = response.getWriter();
      System.out.println("Error: Form must has enctype=multipart/form-data.");
      System.out.println("Error: Form must has enctype=multipart/form-data.");
      return;
    }
    /*
        Command curc = new Command();
        String ccc = "curl --request POST https://sandbox.attestiv.net/wapi/v1.0/keyvalue";
        ArrayList<String> s = curc.execCommand( ccc); //"/Users/skwan/tomcat_8.5.37/data/cmd.sh");
        System.out.print("s=" + s.get(0));
        if( true) return;
     */
    // configures upload settings
    DiskFileItemFactory factory = new DiskFileItemFactory();
    // sets memory threshold - beyond which files are stored in disk
    factory.setSizeThreshold(MEMORY_THRESHOLD);
    // sets temporary location to store files
    factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

    ServletFileUpload upload = new ServletFileUpload(factory);

    // sets maximum size of upload file
    upload.setFileSizeMax(MAX_FILE_SIZE);

    // sets maximum size of request (include file + form data)
    upload.setSizeMax(MAX_REQUEST_SIZE);

    // constructs the directory path to store upload file
    // this path is relative to application's directory
    String uploadPath="";
    Initialize curInit = new Initialize();
    uploadPath = curInit.checkInitDir(request);

    System.out.println("initial uploadPath=" + uploadPath);
    // creates the directory if it does not exist
    File uploadDir = new File(uploadPath);
    if (!uploadDir.exists()) {
      uploadDir.mkdir();
    }
    {
      boolean debug=false;
      String upfileName="";
      String folderName="";
      java.io.InputStream fileStream = null;
      String fileName = "";

      try 
      {
        // parses the request's content to extract file data
        @SuppressWarnings("unchecked")
        List<FileItem> formItems = upload.parseRequest(request);

        if (formItems != null && formItems.size() > 0) 
        {
          // iterates over form's fields
          FileItem curitem=null;
          for (FileItem item : formItems) 
          {
            // processes only fields that are not form fields
            if (!item.isFormField()) 
            {
              upfileName = new File(item.getName()).getName();
              curitem = item;
              fileStream = item.getInputStream();
            }
            else //if (item.isFormField()) 
            {

              String name = item.getFieldName();//text1
              String nameValue = item.getString();
              System.out.println("name=" + name + " nameValue=" + nameValue);
              if( name.equals("folder"))
              {
                uploadPath += File.separator + nameValue;
                folderName = nameValue;
                debugString += " DEBUG: uploadPath=" + uploadPath;
                System.out.println(debugString);
                uploadDir = new File(uploadPath);
                nameValue = ""; //reset the foldername
              }
            }
          }  //end of for item
          String filePath = "";
          filePath = uploadPath + File.separator + "public/" + upfileName;
          if( filePath.length()>0)
          {
            File storeFile = new File(filePath);
            // saves the file on disk
            curitem.write(storeFile);
            System.out.println("Status: Upload " +upfileName + " to " + filePath + " successful!");

            String imageFilepath = filePath; 
            String imageFilename = upfileName;
            HttpClient httpc = new HttpClient();
            String url = "https://sandbox.attestiv.net/wapi/v1.0/keyvalue";
            statusString = httpc.doPostImageFile2(url, imageFilepath, imageFilename);
          }
          //statusString = "fileName=" + upfileName + " " + statusString;
          System.out.println(statusString);
          //{"confidences":{"address":98.7222441941368,"coverage_d":99.59361267089844,"coverage_e":99.6322250366211,
          //"coverage_f":99.63063049316406,"effective_date":98.1688232421875,
          //"policy":99.77876281738281,"premium":98.46903991699219},
          //"fields":{"address":"3655 North Artesian Ave, Chicago IL 60618","coverage_d":"$95,000","coverage_e":"$100,000","coverage_f":"$1,000","effective_date":"2020-07-20","policy":"LP123456","premium":"1400.00 / year"},"tables":{"coverage":[[["COVERAGE","SUMMARY"],[],[]],[["COVERAGE"],["MAXIMUM","AMOUNT"],["COST"]],[["Dwelling"],[],["Included"]],[["Personal","Property"],["$"],["Included"]],[["Loss","Of","Use"],["$95,000"],["Included"]],[["Personal","Liability"],["$1,000,000"],["Included"]],[["Medical","Payments","To","Others"],["$1,000"],["Included"]],[["Changes","to","Your","Policy","(See","next","page)"],[],["$2.84"]],[["Deductible"],["$250.00"],["Included"]],[["Total","Premium"],["1400.00"],["/"]]]}}
          String [] vconfidenceNameToGet = 
              //{ "","hits:hits::index:" };
            { "","confidences", 
                "address:", 
                "coverage_a:", 
                "coverage_b:", 
                "coverage_c:", 
                "coverage_d:", 
                "coverage_e:",
                "coverage_f:",
                "effective_date:",
                "policy:",
                "premium:"
                };
          Vjson j2 = new Vjson();
          String[] ocrList = j2.getRecurJsonFields( statusString, false, vconfidenceNameToGet);
          int hit=0;
          double value=0.0;
          int ivalue =0;
          HashMap<String, Integer> confMap = new HashMap<String, Integer>();
          if( ocrList != null && ocrList.length>1)
          {
            String[] confList = ocrList[1].split("\t");
            value=0.0;
            try { value = Double.parseDouble(confList[1]);}
            catch( Exception x) {}
            ivalue = (int) value;
            confMap.put(ViaConstant.address_1, ivalue);
            confMap.put(ViaConstant.address_2, ivalue);
            confMap.put(ViaConstant.city, ivalue);
            confMap.put(ViaConstant.state, ivalue);
            confMap.put(ViaConstant.zip, ivalue);
            value=0.0;
            try { value = Double.parseDouble(confList[2]);}
            catch( Exception x) {}
            ivalue = (int) value;
            confMap.put(ViaConstant.coverage_a, ivalue);
            value=0.0;
            try { value = Double.parseDouble(confList[3]);}
            catch( Exception x) {}
            ivalue = (int) value;
            confMap.put(ViaConstant.coverage_b, ivalue);
            value=0.0;
            try { value = Double.parseDouble(confList[4]);}
            catch( Exception x) {}
            ivalue = (int) value;
            confMap.put(ViaConstant.coverage_c, ivalue);
            value=0.0;
            try { value = Double.parseDouble(confList[5]);}
            catch( Exception x) {}
            ivalue = (int) value;
            confMap.put(ViaConstant.coverage_d, ivalue);
            value=0.0;
            try { value = Double.parseDouble(confList[6]);}
            catch( Exception x) {}
            ivalue = (int) value;
            confMap.put(ViaConstant.coverage_e, ivalue);
            value=0.0;
            try { value = Double.parseDouble(confList[7]);}
            catch( Exception x) {}
            ivalue = (int) value;
            confMap.put(ViaConstant.coverage_f, ivalue);
            value=0.0;
            try { value = Double.parseDouble(confList[8]);}
            catch( Exception x) {}
            ivalue = (int) value;
            confMap.put(ViaConstant.effective_date, ivalue);
            value=0.0;
            try { value = Double.parseDouble(confList[9]);}
            catch( Exception x) {}
            ivalue = (int) value;
            confMap.put(ViaConstant.home_rent, ivalue);
            value=0.0;
            try { value = Double.parseDouble(confList[10]);}
            catch( Exception x) {}
            ivalue = (int) value;
            confMap.put(ViaConstant.premium, ivalue);
            
          }
          
          String []vfieldNameToGet = 
            { "","fields", 
                "address:", 
                "coverage_a:", 
                "coverage_b:", 
                "coverage_c:", 
                "coverage_d:", 
                "coverage_e:",
                "coverage_f:",
                "effective_date:",
                "policy:",
                "premium:"
                };
          ocrList = j2.getRecurJsonFields( statusString, false, vfieldNameToGet);

          HashMap<String, String> userProfile = new HashMap< String,String>();
          if( ocrList != null && ocrList.length>1)
          {
            String[] confList = ocrList[1].split("\t");
            value=0.0;
            //3655 North Artesian Ave, Chicago IL 60618
            String[] addressParts = confList[1].trim().split(" ");
            //the last part is the zip
            userProfile.put(ViaConstant.zip, 
                addressParts[ addressParts.length-1]+"," + confMap.get(ViaConstant.zip));
            userProfile.put(ViaConstant.state, 
                addressParts[ addressParts.length-2]+"," + confMap.get(ViaConstant.state));
            String city = addressParts[ addressParts.length-3];
            String address1 = "";
            if( !addressParts[ addressParts.length-4].endsWith(","))
            {
              city = addressParts[ addressParts.length-4] + " " + city;
              for(int k=0; k<addressParts.length-4; k++)
              {
                if( address1.length() > 0)
                  address1 += " ";
                address1 += addressParts[k];
              }
              if( address1.endsWith(","))
                address1 = address1.substring(0, address1.length()-1);

            }
            else
            {
              //addressParts[ addressParts.length-4] = addressParts[ addressParts.length-4].substring(0,addressParts[ addressParts.length-4].length()-1 );
              for(int k=0; k<addressParts.length-3; k++)
              {
                if( address1.length() > 0)
                  address1 += " ";
                address1 += addressParts[k];
              }
              if( address1.endsWith(","))
                address1 = address1.substring(0, address1.length()-1);
            }
            userProfile.put(ViaConstant.city, 
                city+"," + confMap.get(ViaConstant.city));
            userProfile.put(ViaConstant.address_1, 
                address1+"," + confMap.get(ViaConstant.address_1));
            if( !confList[2].equals("NONE"))
              userProfile.put(ViaConstant.coverage_a, 
                confList[2]+"," + confMap.get(ViaConstant.coverage_a));
            if( ! confList[3].equals("NONE"))
              userProfile.put(ViaConstant.coverage_b, 
                confList[3]+"," + confMap.get(ViaConstant.coverage_b));
            if( ! confList[4].equals("NONE"))
              userProfile.put(ViaConstant.coverage_c, 
                confList[4]+"," + confMap.get(ViaConstant.coverage_c));
            if( ! confList[5].equals("NONE"))
              userProfile.put(ViaConstant.coverage_d, 
                confList[5]+"," + confMap.get(ViaConstant.coverage_d));
            if( ! confList[6].equals("NONE"))
              userProfile.put(ViaConstant.coverage_e, 
                confList[6]+"," + confMap.get(ViaConstant.coverage_e));
            if( ! confList[7].equals("NONE"))
              userProfile.put(ViaConstant.coverage_f, 
                confList[7]+"," + confMap.get(ViaConstant.coverage_f));
            if( ! confList[8].equals("NONE"))
              userProfile.put(ViaConstant.effective_date, 
                confList[8]+"," + confMap.get(ViaConstant.effective_date));
            //if( confList[9].equals("NONE"))
            //  confList[9] = "";
            //userProfile.put(ViaConstant.policy, confList[9]);
            if( ! confList[10].equals("NONE"))
              userProfile.put(ViaConstant.premium, 
                confList[10]+"," + confMap.get(ViaConstant.premium));
            userProfile.put(ViaConstant.home_rent, 
                "home"+"," + "100");
            userProfile.put(ViaConstant.birthday, 
                "12-31-1990"+"," + "100");
            
          }
          Client cc = new Client();
          statusString = cc.processUserProfile( userProfile);
          request.getSession().setAttribute(ViaConstant.statusString, statusString);


        }
      } catch (Exception ex) 
      {
        ex.printStackTrace();
        errorString = " ERROR: " + ex.getMessage();
        System.out.println(errorString);
      }
      {
        // redirects client to message page
        System.out.println(debugString);
        System.out.println(errorString);
        RequestDispatcher dispatcher
        = this.getServletContext().getRequestDispatcher("/client2.jsp");

        dispatcher.forward(request, response);
      }
    }
  }
  @Override
  protected void doPost(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException 
  {
    doGet(request, response);
  }

  public String uploadCurl(String filePath, String upfileName)
  {
    //System.out.println("Status: Upload " +upfileName + " to " + filePath + " successful!");
    //curl --request POST 'https://sandbox.attestiv.net/wapi/v1.0/keyvalue' --form 'image=@/Users/skwan/homesite/poc/Declarations/Lemonade[1].jpg' --form 'workflow="\"coverage_workflow\""'
    String ocrCommand=
        "curl --request POST https://sandbox.attestiv.net/wapi/v1.0/keyvalue " +
            "--form image=@" + filePath +" --form workflow=\"coverage_workflow\"";
    Command cc = new Command();
    ArrayList<String> ocrRsp = new ArrayList<String>();
    ocrRsp = cc.execCommand(ocrCommand);
    String ocrString ="";
    for(int i=0; ocrRsp!=null&&i<ocrRsp.size(); i++)
    {
      ocrString += ocrRsp.get(i);
    }
    return ocrString;
  }
}
