package com.servlet;


import java.io.File;

//public class AutocompleteReq {

import java.io.IOException;
import java.net.InetAddress;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import utils.Initialize;
import com.utils.*;


//public class FeaturefileReq {
@WebServlet(urlPatterns = { "/ibot" })

public class Ibot extends HttpServlet {
    private static final long serialVersionUID = 1L; 
    private static StringBuffer sb  = new StringBuffer();
    
    public Ibot() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
      String debugString = "";
      request.setAttribute( ViaConstant.debugString, debugString );
      boolean debug = false;
      String errorString = "";
      //read in the query file
      String uploadPath = "";
      Initialize curInit = new Initialize();
      String baseDataDirectory = curInit.checkInitDir(request);
      uploadPath = baseDataDirectory;
      uploadPath += File.separator + "public/ibot_city_state_uniq_length.txt";
      String statusString ="";
      if( sb.length() <= 0 )
      {
        File downloadFile = new File(uploadPath );
        Scanner scan= new Scanner(downloadFile, "UTF-8"); 
        int count = 0;
        int countMax = 500000;
        String line=""; 
        line = scan.nextLine(); //skip header row

        for( ; count<countMax && scan.hasNextLine(); count++)
        {
          line = scan.nextLine(); //skip header row
          if( line != null && line.length() > 0 )
          {
            String query = line.toLowerCase();
            String [] lineList = query.split("\t");
            if( lineList.length >= 3)
              sb.append(  "\"" + lineList[0] + ", "+ lineList[1] + ", " + lineList[2] + "\"," );
          }
        }
        scan.close();
        debugString = "query size=" + count;
      }
      else
        debugString = "skip loading loc_name_uniq_length.txt";
      if( debug )
        statusString += debugString;
      String serverUrl = "https://ec2-18-220-132-134.us-east-2.compute.amazonaws.com";
      String serverLocalUrl = "http://ec2-18-220-132-134.us-east-2.compute.amazonaws.com";
      String ip = request.getRemoteAddr();
      if (ip.equalsIgnoreCase("0:0:0:0:0:0:0:1")) 
          {
              InetAddress inetAddress = InetAddress.getLocalHost();
              String hostname = inetAddress.getHostName();
              if( hostname.contains( "hsks-MacBook-Pro") || hostname.contains( "hsks-MBP"))
                serverLocalUrl = "http://localhost";  //change server url to local host because i am testing on my laptop
              
          }
      request.getSession().setAttribute("serverUrl", serverUrl);
      request.getSession().setAttribute("serverLocalUrl", serverLocalUrl);

      
      request.getSession().setAttribute(ViaConstant.autocompletequery, sb.toString() );
      request.setAttribute(ViaConstant.autocompletequery, sb.toString() );
      //request.getSession().setAttribute( "userSearchList", "");
      //request.getSession().setAttribute( "userSearchResponse", "");


      request.getSession().setAttribute(ViaConstant.statusString, statusString);
      if( debug )
      {
        request.getSession().setAttribute(ViaConstant.debugString, debugString);
      }

      RequestDispatcher dispatcher 
      = this.getServletContext().getRequestDispatcher("/ibot.jsp");

      dispatcher.forward(request, response);
    } 
     
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}
