package com.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.messaging.rsocket.RSocketRequester;
import com.fasterxml.jackson.databind.ser.std.StdKeySerializers.Default;
import com.model.InsuranceAnalyzer;
import com.model.QuoteGenerator;
import com.rsocket.HelloWorldClient;
import com.utils.*;
import io.pivotal.rsocketclient.RSocketShellClient;




/**
 * @author imssbora
 */
@WebServlet("/client")
public class Client extends HttpServlet {
   PrintWriter HttpOut;  // = response.getWriter();
   static HashMap<String, String> quoteByZipMap = new HashMap<String, String>();

   private static final long serialVersionUID = 1L;

   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
         throws ServletException, IOException 
   {
     resp.addHeader("Access-Control-Allow-Origin", "*");
     resp.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
     resp.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
     resp.addHeader("Access-Control-Max-Age", "3000");
     
     HttpOut = resp.getWriter();
     String rc = req.getParameter("c") ;
     {
       if( rc != null && rc.length()>0)
       {
         if( rc.equals("true"))
         {
           HelloWorldClient hc = new HelloWorldClient();
           double dpremium = hc.doMsg();
           resp.getWriter().println("" + dpremium);
           
         }
       }
     }
     //initKnowledgeEngine( req, resp);
     resp.setContentType("text/plain");
     //resp.getWriter().write("Via API Parameters:\n");
     //resp.getWriter().write(req.getQueryString() + "\n");
     String t = req.getParameter("t");
     if( t != null  && t.length()>0 )
     {
       float threshold = ViaConstant.confidenceThreshold;
       try {
         threshold = Float.parseFloat(t);
         ViaConstant.confidenceThreshold = threshold;
       }
       catch(Exception x) {}
     }
     Map<String, String> parameters = getParametersHttpGet(req, resp);
     resp.setContentType("application/json");
     //resp.getWriter().write("response:\n");
     doQuote(req, resp, parameters, HttpOut);
     HttpOut.close();
   }

   private Map<String, String> getParametersHttpGet(HttpServletRequest request, HttpServletResponse resp) 
   {
       Map<String, String> parameters = new HashMap<>();

       Enumeration<String> parameterNames = request.getParameterNames();
       if( parameterNames== null || !parameterNames.hasMoreElements() ) {
           return null;
       }
    
       while (parameterNames.hasMoreElements()) {
           String paramName = parameterNames.nextElement();
           String paramValue = request.getParameter(paramName);
           parameters.put(paramName, paramValue);
           System.out.println(paramName + "=" + paramValue);
       }

       return parameters;
   }

   @Override
   protected void doPost(HttpServletRequest req, HttpServletResponse resp)
           throws ServletException, IOException {

     doGet( req,  resp) ;
     //doQuote(req, resp, getParametersHttpGet(req, resp));
   }

   public void doQuote(
         HttpServletRequest request, 
         HttpServletResponse response, 
         Map<String, String> parameters,
         PrintWriter HttpOut) 
   {
     try
     {
       String command= request.getParameter(ViaConstant.command);
       String debug= request.getParameter(ViaConstant.debug);
       String status="";
       String viaRsp="";

       if( command == null || !command.equals("get_quote"))
       {
         status="ERROR: illegal command: " + command;
         viaRsp="{" + "\"status\":" +  "\"" + status +  "\"" + "}";
         response.getWriter().write( viaRsp + "\n" );

         
       }
       else
       {
         if( debug!=null && debug.toLowerCase().equals("true"))
         {
           response.getWriter().write("Via API Parameters " + ViaConstant.version + " :\n");
           response.getWriter().write(request.getQueryString() + "\n");
           response.getWriter().write("response:\n");

         }
         HashMap<String, String> userProfile = new HashMap< String,String>();
         // copy the profile from the client
         userProfile.put(ViaConstant.first_name, getParamneter( parameters, com.utils.ViaConstant.first_name , 0, ""));
         userProfile.put(ViaConstant.last_name, getParamneter( parameters, com.utils.ViaConstant.last_name , 0, ""));
         userProfile.put(ViaConstant.middle_initial, getParamneter( parameters, com.utils.ViaConstant.middle_initial , 0, ""));
         userProfile.put(ViaConstant.birthday, getParamneter( parameters, com.utils.ViaConstant.birthday, 0, ""));
         userProfile.put(ViaConstant.email, getParamneter( parameters, com.utils.ViaConstant.email , 0, ""));
         userProfile.put(ViaConstant.address_1, getParamneter( parameters, com.utils.ViaConstant.address_1 , 0, ""));
         userProfile.put(ViaConstant.address_2, getParamneter( parameters, com.utils.ViaConstant.address_2 , 0, ""));
         userProfile.put(ViaConstant.city, getParamneter( parameters, com.utils.ViaConstant.city , 0, ""));
         userProfile.put(ViaConstant.state, getParamneter( parameters, com.utils.ViaConstant.state , 0, ""));
         userProfile.put(ViaConstant.zip, getParamneter( parameters, com.utils.ViaConstant.zip , 0, ""));
         userProfile.put(ViaConstant.home_rent, getParamneter( parameters, com.utils.ViaConstant.home_rent , 0, ""));
         userProfile.put(ViaConstant.units, getParamneter( parameters, com.utils.ViaConstant.units , 0, ""));
         userProfile.put(ViaConstant.people, getParamneter( parameters, com.utils.ViaConstant.people , 0, ""));
         userProfile.put(ViaConstant.premium, getParamneter( parameters, com.utils.ViaConstant.premium , 0, ""));
         userProfile.put(ViaConstant.company, getParamneter( parameters, com.utils.ViaConstant.company , 0, ""));
         userProfile.put(ViaConstant.deductible, getParamneter( parameters, com.utils.ViaConstant.deductible , 0, ""));
         userProfile.put(ViaConstant.coverage_a, getParamneter( parameters, com.utils.ViaConstant.coverage_a , 0, ""));
         userProfile.put(ViaConstant.coverage_b, getParamneter( parameters, com.utils.ViaConstant.coverage_b , 0, ""));
         userProfile.put(ViaConstant.coverage_c, getParamneter( parameters, com.utils.ViaConstant.coverage_c , 0, ""));
         userProfile.put(ViaConstant.coverage_d, getParamneter( parameters, com.utils.ViaConstant.coverage_d , 0, ""));
         userProfile.put(ViaConstant.coverage_e, getParamneter( parameters, com.utils.ViaConstant.coverage_e , 0, ""));
         userProfile.put(ViaConstant.coverage_f, getParamneter( parameters, com.utils.ViaConstant.coverage_f , 0, ""));
         userProfile.put(ViaConstant.effective_date, getParamneter( parameters, com.utils.ViaConstant.effective_date , 0, ""));
         viaRsp = processUserProfile( userProfile);
         String result = viaRsp;
         response.getWriter().write( result + "\n" );
         //ViaIo.writeToHttpOut(result, HttpOut);
       }  //end of getQuote command
     }
     catch( Exception x )
     {
       x.printStackTrace();
     }
   }
   
   public String processUserProfile( HashMap< String,String>userProfile)
   {
     InsuranceAnalyzer ia = new InsuranceAnalyzer();
     HashMap<String, String> analyzedProfile = ia.analyzer(userProfile);
     
     String confidence = analyzedProfile.get(ViaConstant.confidence);
     float fConfidence = (float)0.0;
     try { fConfidence = Float.parseFloat(confidence); }
     catch( Exception x ) {}
     String viaRsp="";
     if( fConfidence < ViaConstant.confidenceThreshold)
     {
       viaRsp="{";
       viaRsp+= "\"status\":"  + "\"ERROR- confidence is too low at " + fConfidence + "\"" + "}";
     }
     else
     {
     // prepare to getQuote from QuoteGenerator
     if( analyzedProfile.get(ViaConstant.first_name) == null || analyzedProfile.get(ViaConstant.first_name).length() <= 0 )
       analyzedProfile.put(ViaConstant.first_name, "test");
     if( analyzedProfile.get(ViaConstant.last_name )== null || analyzedProfile.get(ViaConstant.last_name).length() <= 0 )
       analyzedProfile.put(ViaConstant.last_name, "test");
     if( analyzedProfile.get(ViaConstant.birthday) == null || analyzedProfile.get(ViaConstant.birthday).length() <= 0 )
       analyzedProfile.put(ViaConstant.birthday, "12-31-1990");
     if( analyzedProfile.get(ViaConstant.email) == null || analyzedProfile.get(ViaConstant.email).length() <= 0 )
       analyzedProfile.put(ViaConstant.email, "test@test.com");
     if( analyzedProfile.get(ViaConstant.units) == null || analyzedProfile.get(ViaConstant.units).length() <= 0 )
       analyzedProfile.put(ViaConstant.units, "1");
     if( analyzedProfile.get(ViaConstant.people) == null || analyzedProfile.get(ViaConstant.people).length() <= 0 )
       analyzedProfile.put(ViaConstant.people, "2");
     if( analyzedProfile.get(ViaConstant.deductible) == null || analyzedProfile.get(ViaConstant.deductible).length() <= 0 )
       analyzedProfile.put(ViaConstant.deductible, "1000");
     if( analyzedProfile.get(ViaConstant.coverage_c) == null || analyzedProfile.get(ViaConstant.coverage_c).length() <= 0 )
       analyzedProfile.put(ViaConstant.coverage_c, "40000");
     if( analyzedProfile.get(ViaConstant.coverage_e) == null || analyzedProfile.get(ViaConstant.coverage_e).length() <= 0 )
       analyzedProfile.put(ViaConstant.coverage_e, "300000");
     if( analyzedProfile.get(ViaConstant.effective_date) == null || analyzedProfile.get(ViaConstant.effective_date).length() <= 0 )
       analyzedProfile.put(ViaConstant.effective_date, "09-01-2020");
     
     QuoteGenerator qg = new QuoteGenerator();
     HashMap<String, String> newQuote = qg.genQuote(analyzedProfile);
     viaRsp="{";
     viaRsp+= "\"status\":"  + "\"" + newQuote.get(ViaConstant.status ) +"\"" + ",";
     viaRsp += 
             "\"" +ViaConstant.premium + "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.premium ) +"\"" +"," +
             "\"" +ViaConstant.first_name+ "\"" + ":" + 
             "\"" + newQuote.get(ViaConstant.first_name ) +"\"" +"," +
             "\"" +ViaConstant.last_name+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.last_name ) +"\"" +"," +
             "\"" +ViaConstant.address_1 + "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.address_1 ) +"\"" +"," +
             "\"" +ViaConstant.address_2+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.address_2 ) +"\"" +"," +
             "\"" +ViaConstant.city+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.city ) +"\"" +"," +
             "\"" +ViaConstant.state+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.state ) +"\"" +"," +
             "\"" +ViaConstant.zip+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.zip ) +"\"" +"," +
             "\"" +ViaConstant.home_rent+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.home_rent ) +"\"" +"," +
             "\"" +ViaConstant.company + "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.company ) +"\"" +"," +
             "\"" +ViaConstant.deductible+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.deductible ) +"\"" +"," +
             "\"" +ViaConstant.coverage_a+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.coverage_a ) +"\"" +"," +
             "\"" +ViaConstant.coverage_b+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.coverage_b ) +"\"" +"," +
             "\"" +ViaConstant.coverage_c+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.coverage_c ) +"\"" +"," +
             "\"" +ViaConstant.coverage_d+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.coverage_d ) +"\"" +"," +
             "\"" +ViaConstant.coverage_e+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.coverage_e ) +"\"" +"," +
             "\"" +ViaConstant.coverage_f+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.coverage_f ) +"\"" +"," +
             "\"" +ViaConstant.effective_date+ "\"" + ":" + 
             "\"" + newQuote.get( ViaConstant.effective_date ) +"\""+
             "}";
     }
     System.out.println("client viaRsp:" + viaRsp);
     return viaRsp;

   }
   private boolean validParameters(Map<String, String> parameters, HttpServletResponse response) {

       boolean valid = true;

       if (parameters != null && !parameters.isEmpty()) {
           for (Map.Entry<String, String> parameter : parameters.entrySet()) {
               if(!validParameter(parameter.getKey())) {
                   valid = false;
                   break;
               }
           }
       } else {
           valid = false;
       }
/*
       if(!valid) {
           //Scanner scanner = ViaIo.readWebContentFile(getServletContext(), "help.txt");
           ViaIo.writeToHttpOut(scanner, response);
       }
*/
       return valid;
   }
   
   /*
    * for the given hashMap, retrieve the value of the key, 
    * check the score, and replace it with replacement if it is lower than the threshold
    * return the final value
    */
   private String getParamneter( Map<String, String> parameters, String key, int threshold, String replacement)
   {
     String finalValue="";
     String valueScore = parameters.get(key);
     if( valueScore == null || valueScore.length()<=0)
       finalValue = replacement;
     else if( threshold == 0 )
     {
       finalValue = valueScore; // if no threshold, just copy the value and score and return;
     }
     else
     {
       Integer score = 0;
       String[] valueScoreList = valueScore.split(",");
       if( valueScoreList.length>0 )
         valueScoreList[0] = valueScoreList[0].trim();
       if( valueScoreList.length>1 )
       {
         valueScoreList[1] = valueScoreList[1].trim();
         try
         { 
           score = Integer.parseInt(valueScoreList[1]);
         }
         catch( Exception x)
         {}
       }
       if( score < threshold)
         valueScoreList[0] = replacement; 
       finalValue = valueScoreList[0];
     }
     return finalValue;
   }


   private boolean validParameter(String parameter) {
       switch (parameter) {
           case ViaConstant.address_1:
           case ViaConstant.address_2:
           case ViaConstant.birthday:
           case ViaConstant.city:
           case ViaConstant.company:
           case ViaConstant.coverage_a:
           case ViaConstant.coverage_b:
           case ViaConstant.coverage_c:
           case ViaConstant.coverage_d:
           case ViaConstant.coverage_e:
           case ViaConstant.coverage_f:
           case ViaConstant.deductible:
           case ViaConstant.effective_date:
           case ViaConstant.email:
           case ViaConstant.first_name:
           case ViaConstant.home_rent:
           case ViaConstant.last_name:
           case ViaConstant.middle_initial:
           case ViaConstant.people:
           case ViaConstant.premium:
           case ViaConstant.state:
           case ViaConstant.units:
             return true;
           default:
               return false;
       }
   }

   private void initKnowledgeEngine( 
       HttpServletRequest request, 
       HttpServletResponse response
       )
   {
     try
     {
       if( ! quoteByZipMap.isEmpty())
         return;
       String serverDataPath = "";
       String serverPath = getServletContext().getRealPath("");
       if( serverPath.endsWith("/"))
         serverDataPath= serverPath +
         "../../" +
         ViaConstant.dataDirectory;
       else
         serverDataPath= serverPath
         + File.separator + 
         "../../" +
         ViaConstant.dataDirectory;

       String curDict = serverDataPath + File.separator + "insurance_calculator.tsv";
       Scanner scanner = new Scanner(new File(curDict));
       scanner.nextLine(); //skip the header line
       while (scanner.hasNext()) 
       {
         String curline = scanner.nextLine();
         String[] fields = curline.split("\t");
         if(fields.length>=2 )
           quoteByZipMap.put(fields[0], curline);
       }
       scanner.close();
     }
     catch( Exception x)
     {
       x.printStackTrace();
     }

   }
}

/*
Float newPremium = (float) 12.00;
String zip = getParamneter( parameters, com.utils.ViaConstant.zip , 50, "");
String insuranceQuote = quoteByZipMap.get( zip);
String[] insuranceQuoteFields= null;
if( insuranceQuote != null )
  insuranceQuoteFields = insuranceQuote.split( "\t" );
//zipcode  city  state Property_Coverage(c) Deductible  Liability(e) high  low average
Float propertyCoverage= (float)0.0;
Float deductible = (float)0.0; 
Float liability = (float)0.0;
Float high = (float)0.0; 
Float low = (float)0.0;
Float average = (float)0.0;
try
{
  propertyCoverage= Float.parseFloat(insuranceQuoteFields[3]);
  deductible = Float.parseFloat(insuranceQuoteFields[4]);
  liability = Float.parseFloat(insuranceQuoteFields[5]);
  high = Float.parseFloat(insuranceQuoteFields[6]); 
  low = Float.parseFloat(insuranceQuoteFields[7]);
  average = Float.parseFloat(insuranceQuoteFields[8]);
}
catch( Exception x) {}

System.out.println(propertyCoverage + " " + deductible + " " + liability + " " + high + " " + low + " " + average);

String newDeductible = getParamneter( parameters, com.utils.ViaConstant.deductible , 50, "");
String newCoverageC = getParamneter( parameters, com.utils.ViaConstant.coverage_c , 50, "");
String newCoverageE = getParamneter( parameters, com.utils.ViaConstant.coverage_e , 50, "");
try
{
  newPremium = Float.parseFloat( getParamneter( parameters, com.utils.ViaConstant.premium , 50, ""));
  if( newPremium>high )
  {
    newPremium = high;
    newDeductible = String.format("%.2f", deductible);
    newCoverageC = String.format("%.2f", propertyCoverage);
    newCoverageE = String.format("%.2f", liability);
  }
  else if( newPremium>average )
  {
    newPremium = average;
    newDeductible = String.format("%.2f", deductible);
    newCoverageC = String.format("%.2f", propertyCoverage);
    newCoverageE = String.format("%.2f", liability);
  }
  else if( newPremium>low )
  {
    newPremium = low;
    newDeductible = String.format("%.2f", deductible);
    newCoverageC = String.format("%.2f", propertyCoverage);
    newCoverageE = String.format("%.2f", liability);
  }
  //if( newPremium <= 0 )
  //  newPremium = (float) 5.00;
}
catch( Exception x )
{}
*/

