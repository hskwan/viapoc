package com.servlet;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import com.utils.Command;
import com.utils.HttpClient;
import com.utils.Initialize;
import com.utils.ViaConstant;

@WebServlet(urlPatterns = { "/uploadFile" })
public class FileUploadServlet extends HttpServlet 
{
  private static final long serialVersionUID = 1L;

  // location to store file uploaded
  //private static final String UPLOAD_DIRECTORY = "../../data";

  // upload settings
  private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
  private static final int MAX_FILE_SIZE      = 1024 * 1024 * 1000; // 100MB
  private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 1000; // 100MB

  /**
   * Upon receiving file upload submission, parses the request to read
   * upload data and saves the file on disk.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException 
  {
    String errorString ="";
    String debugString ="";
    String statusString="";
    String translate="";
    String translateCommand = "asyncrecognize";
    String mode="";
    //String uploadFileAction = request.getParameter("uploadFileAction");
    //debugString += "DEBUG: uploadFileAction=" + uploadFileAction;

    // checks if the request actually contains upload file

    if (!ServletFileUpload.isMultipartContent(request)) {
      // if not, we stop here
      PrintWriter writer = response.getWriter();
      System.out.println("Error: Form must has enctype=multipart/form-data.");
      System.out.println("Error: Form must has enctype=multipart/form-data.");
      return;
    }
    /*
        Command curc = new Command();
        String ccc = "curl --request POST https://sandbox.attestiv.net/wapi/v1.0/keyvalue";
        ArrayList<String> s = curc.execCommand( ccc); //"/Users/skwan/tomcat_8.5.37/data/cmd.sh");
        System.out.print("s=" + s.get(0));
        if( true) return;
     */
    // configures upload settings
    DiskFileItemFactory factory = new DiskFileItemFactory();
    // sets memory threshold - beyond which files are stored in disk
    factory.setSizeThreshold(MEMORY_THRESHOLD);
    // sets temporary location to store files
    factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

    ServletFileUpload upload = new ServletFileUpload(factory);

    // sets maximum size of upload file
    upload.setFileSizeMax(MAX_FILE_SIZE);

    // sets maximum size of request (include file + form data)
    upload.setSizeMax(MAX_REQUEST_SIZE);

    // constructs the directory path to store upload file
    // this path is relative to application's directory
    String uploadPath="";
    Initialize curInit = new Initialize();
    uploadPath = curInit.checkInitDir(request);

    System.out.println("initial uploadPath=" + uploadPath);
    // creates the directory if it does not exist
    File uploadDir = new File(uploadPath);
    if (!uploadDir.exists()) {
      uploadDir.mkdir();
    }
    {
      boolean debug=false;
      String upfileName="";
      String folderName="";
      java.io.InputStream fileStream = null;
      String fileName = "";

      try 
      {
        // parses the request's content to extract file data
        @SuppressWarnings("unchecked")
        List<FileItem> formItems = upload.parseRequest(request);

        if (formItems != null && formItems.size() > 0) 
        {
          // iterates over form's fields
          FileItem curitem=null;
          for (FileItem item : formItems) 
          {
            // processes only fields that are not form fields
            if (!item.isFormField()) 
            {
              upfileName = new File(item.getName()).getName();
              curitem = item;
              fileStream = item.getInputStream();
            }
            else //if (item.isFormField()) 
            {

              String name = item.getFieldName();//text1
              String nameValue = item.getString();
              System.out.println("name=" + name + " nameValue=" + nameValue);
              if( name.equals("folder"))
              {
                uploadPath += File.separator + nameValue;
                folderName = nameValue;
                debugString += " DEBUG: uploadPath=" + uploadPath;
                System.out.println(debugString);
                uploadDir = new File(uploadPath);
                nameValue = ""; //reset the foldername
              }
            }
          }  //end of for item
          String filePath = "";
          filePath = uploadPath + File.separator + "public/" + upfileName;
          if( filePath.length()>0)
          {
            File storeFile = new File(filePath);
            // saves the file on disk
            curitem.write(storeFile);
            System.out.println("Status: Upload " +upfileName + " to " + filePath + " successful!");

            String imageFilepath = filePath; 
            String imageFilename = upfileName;
            HttpClient httpc = new HttpClient();
            String url = "https://sandbox.attestiv.net/wapi/v1.0/keyvalue";
            statusString = httpc.doPostImageFile2(url, imageFilepath, imageFilename);
          }
          statusString = "fileName=" + upfileName + " " + statusString;
          System.out.println(statusString);
          request.getSession().setAttribute(ViaConstant.statusString, statusString);

        }
      } catch (Exception ex) 
      {
        ex.printStackTrace();
        errorString = " ERROR: " + ex.getMessage();
        System.out.println(errorString);
      }
      {
        // redirects client to message page
        System.out.println(debugString);
        System.out.println(errorString);
        RequestDispatcher dispatcher
        = this.getServletContext().getRequestDispatcher("/upload.jsp");

        dispatcher.forward(request, response);
      }
    }
  }
  @Override
  protected void doPost(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException 
  {
    doGet(request, response);
  }

  public String uploadCurl(String filePath, String upfileName)
  {
    //System.out.println("Status: Upload " +upfileName + " to " + filePath + " successful!");
    //curl --request POST 'https://sandbox.attestiv.net/wapi/v1.0/keyvalue' --form 'image=@/Users/skwan/homesite/poc/Declarations/Lemonade[1].jpg' --form 'workflow="\"coverage_workflow\""'
    String ocrCommand=
        "curl --request POST https://sandbox.attestiv.net/wapi/v1.0/keyvalue " +
            "--form image=@" + filePath +" --form workflow=\"coverage_workflow\"";
    Command cc = new Command();
    ArrayList<String> ocrRsp = new ArrayList<String>();
    ocrRsp = cc.execCommand(ocrCommand);
    String ocrString ="";
    for(int i=0; ocrRsp!=null&&i<ocrRsp.size(); i++)
    {
      ocrString += ocrRsp.get(i);
    }
    return ocrString;
  }
}
