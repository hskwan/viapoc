package com.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.utils.HttpClient;
import com.utils.ViaConstant;
import com.utils.Vjson;

@WebServlet(urlPatterns = { "/upload2nv"})
public class Upload2nvServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
		// Create a session to store and retrieve the parameters across all 5 servlets
		HttpSession session = request.getSession();
		
		// Retrieve the name of the page that redirected/forwarded to upload2nv.jsp
		StringBuffer previous_page =  (StringBuffer) session.getAttribute("previous_page");
		
		// remove error message if the redirection originated from upload2doc (happy path)
		// created a function "contains" since StringBuffer doesn't have "contains" 
		// It is used to identify the name of the page from the URL "previous_page"
		if(!contains(previous_page,"/upload2compare")) {
			session.removeAttribute("errorMessage");
		}
		
		// Store the file path and file name of the uploaded document
        String upfileName = (String) session.getAttribute("upfileName");
        String filePath = (String) session.getAttribute("filePath");
        String imageFilepath = filePath; 
        String imageFilename = upfileName;
        
        HttpClient httpc = new HttpClient();
        
        // End Point for Attestiv to retrieve the details from scanned document
        String url = "https://sandbox.attestiv.net/wapi/v1.0/keyvalue";

        // Send the request to Attestiv API to retrieve the details from scanned document
        String statusString = httpc.doPostImageFile2(url, imageFilepath, imageFilename);
        //	String statusString = new String("{\"confidences\":{\"address\":98.70458840476192,\"coverage_a\":99.82433319091797,\"coverage_c\":95.67581176757812,\"coverage_d\":99.62860870361328,\"coverage_e\":99.6322250366211,\"coverage_f\":88.32488250732422,\"effective_date\":98.16140747070312,\"policy\":99.74301147460938,\"premium\":98.4122543334961},\"fields\":{\"address\":\"3655 North Artesian Ave, Chicago IL 60618\",\"coverage_a\":\"$130,000\",\"coverage_c\":\"$190,000\",\"coverage_d\":\"$95,000\",\"coverage_e\":\"$100,000\",\"coverage_f\":\"$4,000\",\"effective_date\":\"2020-07-20\",\"policy\":\"LP123456\",\"premium\":\"1400.00 / year\"},\"tables\":{\"coverage\":[[[\"COVERAGE\"],[\"MAXIMUM\",\"AMOUNT\"],[\"COST\"]],[[\"Dwelling\"],[\"$130,000\"],[\"Included\"]],[[\"Personal\",\"Property\"],[\"$190,000\"],[\"Included\"]],[[\"Loss\",\"Of\",\"Use\"],[\"$95,000\"],[\"Included\"]],[[\"Personal\",\"Liability\"],[\"$1,000,000\"],[\"Included\"]],[[\"Medical\",\"Payments\",\"To\",\"Others\"],[\"$4,000\"],[\"Included\"]],[[\"Changes\",\"to\",\"Your\",\"Policy\",\"(See\",\"next\",\"page)\"],[],[\"$2.84\"]],[[\"Deductible\"],[\"$250.00\"],[\"Included\"]],[[],[],[]],[[\"Total\",\"Premium\"],[],[]]]}}");
 
        System.out.println(statusString);
        session.setAttribute(ViaConstant.statusString, statusString);
        
        // Only following selected fields are retrieved from the response of Attestiv API and 
     	// sent as body request of POST request to quote generator
        String [] vconfidenceNameToGet = 
              { "","confidences", 
                  "address:", 
                  "coverage_a:", 
                  "coverage_b:", 
                  "coverage_c:", 
                  "coverage_d:", 
                  "coverage_e:",
                  "coverage_f:",
                  "effective_date:",
                  "policy:",
                  "premium:",
                  "first_name:",
                  "last_name:",
                  "email:",
                  "deductible:",
                  "dob:",
                  "address_1:",
                  "city:",
                  "state:",
                  "postal_code:",//Attestiv API returns postal_code instead of "zip"
                  "address_2:"
                  };
            Vjson j2 = new Vjson();
            String[] ocrList = j2.getRecurJsonFields( statusString, false, vconfidenceNameToGet);
            int hit=0;
            double value=0.0;
            int ivalue =0;
            HashMap<String, Integer> confMap = new HashMap<String, Integer>();
            // Store the confidence scores of the above mentioned fields
            if( ocrList != null && ocrList.length>1)
            {
              String[] confList = ocrList[1].split("\t");
              // API now returns address fields separately

              value=0.0;
              try { value = Double.parseDouble(confList[2]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.coverage_a, ivalue);
              value=0.0;
              try { value = Double.parseDouble(confList[3]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.coverage_b, ivalue);
              value=0.0;
              try { value = Double.parseDouble(confList[4]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.coverage_c, ivalue);
              value=0.0;
              try { value = Double.parseDouble(confList[5]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.coverage_d, ivalue);
              value=0.0;
              try { value = Double.parseDouble(confList[6]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.coverage_e, ivalue);
              value=0.0;
              try { value = Double.parseDouble(confList[7]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.coverage_f, ivalue);
              value=0.0;
              try { value = Double.parseDouble(confList[8]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.effective_date, ivalue);
              value=0.0;
              try { value = Double.parseDouble(confList[9]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.home_rent, ivalue);
              value=0.0;
              try { value = Double.parseDouble(confList[10]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.premium, ivalue);
              
              value=0.0;
              try { value = Double.parseDouble(confList[11]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.first_name, ivalue);
              
              value=0.0;
              try { value = Double.parseDouble(confList[12]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.last_name, ivalue);
              
              value=0.0;
              try { value = Double.parseDouble(confList[13]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.email, ivalue);
              
              value=0.0;
              try { value = Double.parseDouble(confList[14]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.deductible, ivalue);
              
              value=0.0;
              try { value = Double.parseDouble(confList[15]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.birthday, ivalue);
              
              value=0.0;
              try { value = Double.parseDouble(confList[16]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.address_1, ivalue);
              
              value=0.0;
              try { value = Double.parseDouble(confList[17]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.city, ivalue);
              
              value=0.0;
              try { value = Double.parseDouble(confList[18]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.state, ivalue);
              
              value=0.0;
              try { value = Double.parseDouble(confList[19]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.zip, ivalue);
              
              value=0.0;
              try { value = Double.parseDouble(confList[20]);}
              catch( Exception x) {}
              ivalue = (int) value;
              confMap.put(ViaConstant.address_2, ivalue);
            }
            
            // Fields that we want to store in the userProfile and send to POST request (Initial quote request)
         	// Only following selected fields are retrieved from the response of Attestiv API and 
         	// sent as body request of POST request to quote generator
            String []vfieldNameToGet = 
              { "","fields", 
                  "address:", 
                  "coverage_a:", 
                  "coverage_b:", 
                  "coverage_c:", 
                  "coverage_d:", 
                  "coverage_e:",
                  "coverage_f:",
                  "effective_date:",
                  "policy:",
                  "premium:",
                  "first_name:",
                  "last_name:",
                  "email:",
                  "deductible:",
                  "dob:",//attestiv returns dob instead of "birthday"
                  "address_1:",
                  "city:",
                  "state:",
                  "postal_code:",//Attestiv API returns postal_code instead of "zip"
                  "address_2"
                  };
            ocrList = j2.getRecurJsonFields( statusString, false, vfieldNameToGet);
            
            // Store the details of User alongwith confidence score retrieved from Attestiv API
            HashMap<String, String> userProfile = new HashMap< String,String>();
            
            
            if( ocrList != null && ocrList.length>1)
            {
              String[] confList = ocrList[1].split("\t");
              System.out.println("ocrList");
              System.out.println(Arrays.toString(ocrList));
              // Attestiv API now returns the address fields separately
                        
              if( !confList[1].equals("NONE"))
                  userProfile.put(ViaConstant.coverage_a, 
                    confList[2]+";" + confMap.get(ViaConstant.coverage_a));
              if( !confList[2].equals("NONE"))
                userProfile.put(ViaConstant.coverage_a, 
                  confList[2]+";" + confMap.get(ViaConstant.coverage_a));
              if( ! confList[3].equals("NONE"))
                userProfile.put(ViaConstant.coverage_b, 
                  confList[3]+";" + confMap.get(ViaConstant.coverage_b));
              if( ! confList[4].equals("NONE"))
                userProfile.put(ViaConstant.coverage_c, 
                  confList[4]+";" + confMap.get(ViaConstant.coverage_c));
              if( ! confList[5].equals("NONE"))
                userProfile.put(ViaConstant.coverage_d, 
                  confList[5]+";" + confMap.get(ViaConstant.coverage_d));
              if( ! confList[6].equals("NONE"))
                userProfile.put(ViaConstant.coverage_e, 
                  confList[6]+";" + confMap.get(ViaConstant.coverage_e));
              if( ! confList[7].equals("NONE"))
                userProfile.put(ViaConstant.coverage_f, 
                  confList[7]+";" + confMap.get(ViaConstant.coverage_f));
              if( ! confList[8].equals("NONE"))
                userProfile.put(ViaConstant.effective_date, 
                  confList[8]+";" + confMap.get(ViaConstant.effective_date));
              //if( confList[9].equals("NONE"))
              //  confList[9] = "";
              //userProfile.put(ViaConstant.policy, confList[9]);
              if( ! confList[10].equals("NONE"))
                userProfile.put(ViaConstant.premium, 
                  confList[10]+";" + confMap.get(ViaConstant.premium));
              if( ! confList[11].equals("NONE"))
                  userProfile.put(ViaConstant.first_name, 
                    confList[11]+";" + confMap.get(ViaConstant.first_name));
              if( ! confList[12].equals("NONE"))
                  userProfile.put(ViaConstant.last_name, 
                    confList[12]+";" + confMap.get(ViaConstant.last_name));
              if( ! confList[13].equals("NONE"))
                  userProfile.put(ViaConstant.email, 
                    confList[13]+";" + confMap.get(ViaConstant.email));
              if( ! confList[14].equals("NONE"))
                  userProfile.put(ViaConstant.deductible, 
                    confList[14]+";" + confMap.get(ViaConstant.deductible));
              if( ! confList[15].equals("NONE"))
                  userProfile.put(ViaConstant.birthday, 
                    confList[15]+";" + confMap.get(ViaConstant.birthday));
              
              if( ! confList[16].equals("NONE"))
                  userProfile.put(ViaConstant.address_1, 
                    confList[16]+";" + confMap.get(ViaConstant.address_1));
              
              if( ! confList[17].equals("NONE"))
                  userProfile.put(ViaConstant.city, 
                    confList[17]+";" + confMap.get(ViaConstant.city));
              
              if( ! confList[18].equals("NONE"))
                  userProfile.put(ViaConstant.state, 
                    confList[18]+";" + confMap.get(ViaConstant.state));
              
              if( ! confList[19].equals("NONE"))
                  userProfile.put(ViaConstant.zip, 
                    confList[19]+";" + confMap.get(ViaConstant.zip));
              
              if( ! confList[20].equals("NONE"))
                  userProfile.put(ViaConstant.address_2, 
                    confList[20]+";" + confMap.get(ViaConstant.address_2));
              
              userProfile.put(ViaConstant.home_rent, 
                  "home"+"," + "100");
              /*userProfile.put(ViaConstant.birthday, 
                  "12-31-1990"+"," + "100");
              */
            }
            System.out.println("user Profile");
            System.out.println(userProfile);
            session.setAttribute("userProfile", userProfile);
            
            
        // Redirect to the JSP to render the results of "Comparison" between Current and Best Fit policy
        RequestDispatcher dispatcher
        = this.getServletContext().getRequestDispatcher("/upload2nv.jsp");
        dispatcher.forward(request, response);
        
	}	
	
	// method to find a string in StringBufffer of Redirected URL
	public static boolean contains(StringBuffer sb, String findString){
	    return sb.indexOf(findString) > -1;
	}
}
