package com.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.utils.Initialize;

/**
 * Servlet implementation class WelcomeServlet
 */
@WebServlet("/welcome")
public class WelcomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 public WelcomeServlet() {
         super();
     }
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		boolean debug=!true;
        String debugString="";
        boolean hasError = false;
        String errorString = "";

        Initialize curInit = new Initialize();
        String serverPath = curInit.checkInitDir(request);

          
         // Forward to /WEB-INF/views/upload.jsp
         // (Users can not access directly into JSP pages placed in WEB-INF)        
         RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/welcome.jsp");
          
         dispatcher.forward(request, response);
          
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
