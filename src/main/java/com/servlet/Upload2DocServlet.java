package com.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.utils.Initialize;

/**
 * Servlet implementation class Upload2DocServlet
 */
@WebServlet("/upload2doc")
public class Upload2DocServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Upload2DocServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				boolean debug=!true;
		        String debugString="";
		        boolean hasError = false;
		        String errorString = "";

		        Initialize curInit = new Initialize();
		        String serverPath = curInit.checkInitDir(request);

		         
		        // Lines 44 to 49 Added by Gagan Kaushal om 11th Aug 2020
		        // Store the current URL. It will help in distinguishing and printing an error, if required on upload2nv.jsp
                // if upload2compare redirects to upload2nv, then show error, otherwise not.
		        HttpSession session = request.getSession();
                StringBuffer requestUrl=request.getRequestURL();
       		 	session.setAttribute("previous_page",requestUrl);
		         // Forward to /WEB-INF/views/upload.jsp
		         // (Users can not access directly into JSP pages placed in WEB-INF) 
		         
		        
		         RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/upload2doc.jsp");
		          
		         dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
