package com.servlet;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.commons.io.FileUtils;
//import org.apache.commons.io.comparator.LastModifiedFileComparator;
import com.utils.ViaConstant;
import com.utils.Initialize;
import com.utils.ViaConstant;

@WebServlet(urlPatterns = { "/upload"})
public class FileUploadRequest extends HttpServlet {
	   private static final long serialVersionUID = 1L;

	   public FileUploadRequest() {
	       super();
	   }

	   @Override
	   protected void doGet(HttpServletRequest request, HttpServletResponse response)
	           throws ServletException, IOException 
	   {
		   
		   	//Connection conn=null;
		    boolean debug=!true;
		    String debugString="";
		   	boolean hasError = false;
		    String errorString = "";

		    Initialize curInit = new Initialize();
		    String serverPath = curInit.checkInitDir(request);

	        
	       // Forward to /WEB-INF/views/upload.jsp
	       // (Users can not access directly into JSP pages placed in WEB-INF)        
	       RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/upload.jsp");
	        
	       dispatcher.forward(request, response);
	        
	   }

	   @Override
	   protected void doPost(HttpServletRequest request, HttpServletResponse response)
	           throws ServletException, IOException {
	       doGet(request, response);
	   }

	}
