package com.servlet;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import com.utils.Command;
import com.utils.HttpClient;
import com.utils.Initialize;
import com.utils.ViaConstant;

/**
 * Servlet implementation class Upload2Servlet
 */
@WebServlet("/upload2")
public class Upload2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	// upload settings
	  private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
	  private static final int MAX_FILE_SIZE      = 1024 * 1024 * 1000; // 100MB
	  private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 1000; // 100MB
    
    public Upload2Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		boolean debug=!true;
        String debugString="";
        boolean hasError = false;
        String errorString = "";

        Initialize curInit = new Initialize();
        String serverPath = curInit.checkInitDir(request);

          
         // Forward to /WEB-INF/views/upload.jsp
         // (Users can not access directly into JSP pages placed in WEB-INF)        
         RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/upload2.jsp");
          
         dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String errorString ="";
	    String debugString ="";
	    String statusString="";
	    String translate="";
	    String translateCommand = "asyncrecognize";
	    String mode="";
	    String filePath = "";
	    
	    if (!ServletFileUpload.isMultipartContent(request)) {
	        // if not, we stop here
	        PrintWriter writer = response.getWriter();
	        System.out.println("Error: Form must has enctype=multipart/form-data.");
	        System.out.println("Error: Form must has enctype=multipart/form-data.");
	        return;
	     }
	    
	    // configures upload settings
	    DiskFileItemFactory factory = new DiskFileItemFactory();
	    // sets memory threshold - beyond which files are stored in disk
	    factory.setSizeThreshold(MEMORY_THRESHOLD);
	    // sets temporary location to store files
	    factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

	    ServletFileUpload upload = new ServletFileUpload(factory);

	    // sets maximum size of upload file
	    upload.setFileSizeMax(MAX_FILE_SIZE);

	    // sets maximum size of request (include file + form data)
	    upload.setSizeMax(MAX_REQUEST_SIZE);
	    

        Initialize curInit = new Initialize();
        //String uploadPath = curInit.checkInitDir(request);
        String appPath = request.getServletContext().getRealPath("");
        String uploadPath = appPath ;
        

        System.out.println("initial uploadPath=" + uploadPath);
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
          uploadDir.mkdir();
        }
        {
            boolean debug=false;
            String upfileName="";
            String folderName="";
            java.io.InputStream fileStream = null;
            String fileName = "";
            
            try 
            {
              // parses the request's content to extract file data
              @SuppressWarnings("unchecked")
              List<FileItem> formItems = upload.parseRequest(request);
              if (formItems != null && formItems.size() > 0) 
              {
                // iterates over form's fields
                FileItem curitem=null;
                for (FileItem item : formItems) 
                {
                  // processes only fields that are not form fields
                  if (!item.isFormField()) 
                  {
                    upfileName = new File(item.getName()).getName();
                    curitem = item;
                    fileStream = item.getInputStream();
                  }
                  else //if (item.isFormField()) 
                  {

                    String name = item.getFieldName();//text1
                    String nameValue = item.getString();
                    System.out.println("name=" + name + " nameValue=" + nameValue);
                    if( name.equals("folder"))
                    {
                      uploadPath += File.separator + nameValue;
                      folderName = nameValue;
                      debugString += " DEBUG: uploadPath=" + uploadPath;
                      System.out.println(debugString);
                      uploadDir = new File(uploadPath);
                      nameValue = ""; //reset the folder name
                    }
                  }
                }
         
                filePath = uploadPath + File.separator+ upfileName;
                
                if( filePath.length()>0)
                {
                  File storeFile = new File(filePath);
                  // saves the file on disk
                  curitem.write(storeFile);
                  System.out.println("Status: Upload " +upfileName + " to " + filePath + " successful!");

                  String imageFilepath = filePath; 
                  String imageFilename = upfileName;
                }

              }
            } catch (Exception ex) 
            {
              ex.printStackTrace();
              errorString = " ERROR: " + ex.getMessage();
              System.out.println(errorString);
            }

            {
                // redirects client to message page
                System.out.println(debugString);
                System.out.println(errorString);
                
                // Line 185 - 187 added by Gagan Kaushal
                // Store the 2 values in session: upfileName and filePath
                // Following attributes are being used in Upload2nvServlet to fetch the name value pairs
                // by sending document information to Attestiv.
                HttpSession session = request.getSession();
                session.setAttribute("upfileName", upfileName);
                session.setAttribute("filePath", filePath);
                // Store the current URL. It will help in distinguishing and printing an error, if required on upload2nv.jsp
                // if upload2compare redirects to upload2nv, then show error, otherwise not.
                StringBuffer requestUrl=request.getRequestURL();
       		 	session.setAttribute("previous_page",requestUrl);
                
                RequestDispatcher dispatcher
                = this.getServletContext().getRequestDispatcher("/upload2doc");
                
                
                request.setAttribute("fileurl", upfileName);

                dispatcher.forward(request, response);
              }
        }
	}

}
