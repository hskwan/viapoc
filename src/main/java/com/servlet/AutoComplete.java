package com.servlet;

//public class AutoComplete

//public class AutoC {
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.utils.Initialize;
import com.utils.ViaConstant;

//import com.sun.javafx.util.Utils;

//log4j.PropertyConfigurator



@WebServlet(urlPatterns = { "/autocomplete" })

public class AutoComplete extends HttpServlet {
  private static final long serialVersionUID = 1L;

  public AutoComplete() {
      super();
  }

  public void init() throws ServletException {


  }
  
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException 
  {

    String result="";
    String debugString = "";
    String statusString = "";
    String ac_city = "";
    //String qry = request.getParameter(Constant.qry);
    //qry = qry.replaceAll(" ", "+");
    Enumeration<String> parameterNames = request.getParameterNames();
    boolean pretty=false;
    boolean diag = false;
    boolean forceSave = false;
    while (parameterNames.hasMoreElements()) 
    {
      String paramName = parameterNames.nextElement();

      String paramValue = request.getParameter(paramName);
      if( paramName.equals( "accity"))
      {
        ac_city = paramValue;
        ac_city = ac_city.replace("%20", " ");
        ac_city = ac_city.replace('+', ' ');
      }
    } //end of while

    //read in the query file
    String uploadPath = "";
    Initialize curInit = new Initialize();
    String baseDataDirectory = curInit.checkInitDir(request);


    if( ViaConstant.viaZipCityStateList.size() <= 0 )
    {
      uploadPath = baseDataDirectory;
      uploadPath += File.separator + "public/ibot_city_state_uniq_length.txt";
      File downloadFile = new File(uploadPath );
      Scanner scan= new Scanner(downloadFile, "UTF-8"); //"/Users/hwan/Documents/adobe/stock/title_us.tsv")); //"/Users/hwan/Documents/adobe/stock/tag_index_1gram_score.tsv"));
      int count = 0;
      int countMax = 500000;
      String line=""; // no need to skip for this file scan.nextLine(); //skip header row
      line = scan.nextLine(); //skip header row

      for( ; count<countMax && scan.hasNextLine(); count++)
      {
        line = scan.nextLine(); //skip header row
        //String[] lineToken = line.split("\t");
        if( line != null && line.length() > 0 )
        {
           String query = line.toLowerCase();
          String [] lineList = query.split("\t");
          if( lineList.length >= 9)
          {
            ViaConstant.viaZipCityStateList.add( lineList[0] + ", "+ lineList[1] + ", " + lineList[2] );
            ViaConstant.viaZipQuoteMap.put(lineList[0], line);
          }
        }
      }
      scan.close();
      uploadPath = baseDataDirectory;
      uploadPath += File.separator + "public/apartment_location_redfin.tsv";
      
      debugString = "viaZipCityStateList size=" + count;
    }
    /*
    if( ViaConstant.viaAddressList.size() <= 0 )
    {
      uploadPath = baseDataDirectory;
      uploadPath += File.separator + "public/ibot_city_state_uniq_length.txt";

      String downloadFile = new File(uploadPath );
      Scanner scan= new Scanner(downloadFile, "UTF-8");  
      int count = 0;
      int countMax = 500000;
      String line=""; // no need to skip for this file scan.nextLine(); //skip header row
      line = scan.nextLine(); //skip header row

      for( ; count<countMax && scan.hasNextLine(); count++)
      {
        line = scan.nextLine(); //skip header row
        //String[] lineToken = line.split("\t");
        if( line != null && line.length() > 0 )
        {
          String query = line.toLowerCase();
          String [] lineList = query.split("\t");
          if( lineList.length >= 4)
          {
            String []addressParts = lineList[3].split(" ");
            String zipPart = addressParts[ addressParts.length-1 ];
            ViaConstant.viaZipCityStateList.add( lineList[3] + "\t" + zipPart);
          }
        }
      }
      scan.close();

    }
    */

    //71937   Cove    AR      40000   1000    300000  1426    91      408
    int matchCount =20;
    String sugResult = "";
    //for empty ac_city string , return the entire list
    if(  ac_city == null || ac_city.length()<=0)
    {
      // return top 20 items
      for( int i=0; matchCount > 0 && i<ViaConstant.viaZipCityStateList.size(); i++)
      {
        if( sugResult.length() > 0)
          sugResult += "\t";
        sugResult += ViaConstant.viaZipCityStateList.get(i);
        matchCount --;
      }
    }
    else if( ac_city.length()  <= 2  )
    {
      // for two char string,return matching pre-fix
      for( int i=0; matchCount > 0 && i<ViaConstant.viaZipCityStateList.size(); i++)
      {
        if( ViaConstant.viaZipCityStateList.get(i).startsWith( ac_city ))
        {
          if( sugResult.length() > 0)
            sugResult += "\t";
          sugResult += ViaConstant.viaZipCityStateList.get(i);
          matchCount --;
        }
      }
    }
    else if( ac_city.length()  == 3 )
    {
      String stateNameToMatch = "";
      if( ac_city.endsWith( " "))
        stateNameToMatch = " " + ac_city.substring(0, 2);
      // for three char string, retutn matching pre-fix as well as matching state name
      for( int i=0; matchCount > 0 && i<ViaConstant.viaZipCityStateList.size(); i++)
      {
        if( ViaConstant.viaZipCityStateList.get(i).startsWith( ac_city ) ||
            ( stateNameToMatch.length()> 0 && 
                ViaConstant.viaZipCityStateList.get(i).endsWith( stateNameToMatch ))
            )
        {
          if( sugResult.length() > 0)
            sugResult += "\t";
          sugResult += ViaConstant.viaZipCityStateList.get(i);
          matchCount--;
        }
      }

    }

    else //more the 3 chars
    {
      String stateNameToMatch = "";
      String ac_city_substring = "";
      if( ac_city.charAt( 2 ) == ' ')
      {
        stateNameToMatch = " " + ac_city.substring(0, 2);
        ac_city_substring = ac_city.substring( 3 );
      }
      else if( ac_city.charAt( ac_city.length()-3 ) == ' ')
      {
        stateNameToMatch = ac_city.substring(ac_city.length()-3 );
        ac_city_substring = ac_city.substring(0, ac_city.length()-3 );

      }
      // for 4 or more char string, return matching in-fix as well as matching state name
      for( int i=0; matchCount > 0 && i<ViaConstant.viaZipCityStateList.size(); i++)
      {
        if( ViaConstant.viaZipCityStateList.get(i).contains( ac_city ) ||
            ( 
                ac_city_substring.length() > 3 &&
                ViaConstant.viaZipCityStateList.get(i).contains( ac_city_substring ) &&
                ViaConstant.viaZipCityStateList.get(i).endsWith( stateNameToMatch )
                ) ||
            (
                ac_city_substring.length() > 0 && ac_city_substring.length() <= 3 &&
                    ViaConstant.viaZipCityStateList.get(i).startsWith( ac_city_substring ) &&
                    ViaConstant.viaZipCityStateList.get(i).endsWith( stateNameToMatch )

                )
            )
        {
          if( sugResult.length() > 0)
            sugResult += "\t";
          sugResult += ViaConstant.viaZipCityStateList.get(i);
          matchCount --;
        }
      }

    }
    /*
     * Access-Control-Allow-Origin: http://api.bob.com
     * Access-Control-Allow-Credentials: true
     *Access-Control-Expose-Headers: FooBar
     */

    response.addHeader("Access-Control-Allow-Origin", "*");
    response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
    response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
    response.addHeader("Access-Control-Max-Age", "1728000");

    PrintWriter HttpOut = response.getWriter();
    HttpOut.println( sugResult );
    HttpOut.close();
  }
  


  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
  doGet(request, response);
  }
  
  

}
