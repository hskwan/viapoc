package com.rsocket;

//public class Rclient
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utils.ProfileProcessor;
import com.utils.ViaConstant;
import io.rsocket.Payload;
import io.rsocket.RSocket;
import io.rsocket.core.RSocketConnector;
import io.rsocket.transport.netty.client.TcpClientTransport;
import io.rsocket.util.DefaultPayload;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public final class Rclient {

    private static final Logger logger = LoggerFactory.getLogger(HelloWorldClient.class);

    public static void main(String[] args) throws JsonProcessingException 
    {
      HelloWorldClient hc = new HelloWorldClient();
      hc.doMsg();
    }
    public HashMap<String, String> doMsg(HashMap<String, String> analyzedProfile)
    {
      RSocket  socket =
          RSocketConnector.connectWith(TcpClientTransport.create("localhost", 7000)).block();
      ProfileProcessor pc = new ProfileProcessor();

      //Calcuating effectiveDate
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DAY_OF_YEAR, 1);
      SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd");
      String currentDate = DateFor.format(cal.getTime());

      //Building a json object (Can be done any way you like as long as the result is a JSON string.)
      JSONObject inquiryRequest = new JSONObject();
      inquiryRequest.put("address.1a345678.city", analyzedProfile.get(ViaConstant.city));
      inquiryRequest.put("address.1a345678.line1",  analyzedProfile.get(ViaConstant.address_1));
      inquiryRequest.put("address.1a345678.state",  analyzedProfile.get(ViaConstant.state));
      inquiryRequest.put("address.1a345678.zipcode",  analyzedProfile.get(ViaConstant.zip));
      inquiryRequest.put("person.1ab45678.dob",  analyzedProfile.get(ViaConstant.birthday));
      inquiryRequest.put("person.1ab45678.email",  analyzedProfile.get(ViaConstant.email));
      inquiryRequest.put("person.1ab45678.firstName",  analyzedProfile.get(ViaConstant.first_name));
      inquiryRequest.put("person.1ab45678.lastName",  analyzedProfile.get(ViaConstant.last_name));
      inquiryRequest.put("person.1ab45678.phone",  analyzedProfile.get(ViaConstant.phone));
      inquiryRequest.put("policy.home.effectiveDate", analyzedProfile.get(ViaConstant.effective_date));
      inquiryRequest.put("primaryInsured.person.creditDisclosureFlag", "true");
      //inquiryRequest.put("product.lob",  analyzedProfile.get(ViaConstant.home_rent));
      inquiryRequest.put("product.lob",  analyzedProfile.get(ViaConstant.home_rent));


      //Connect to the quote generator and send the payload (JSON)
      String quoteResponse = socket.requestResponse(DefaultPayload.create(inquiryRequest.toString()))
          .map(Payload::getDataUtf8)
          .onErrorReturn("error")
          .block();

      QuoteMessage newQuote = null;
      try {
      //Map the resulting string into an object for easy access
      newQuote = new ObjectMapper().readValue(quoteResponse, QuoteMessage.class);
      }catch(Exception x)
      {
        x.printStackTrace();
      }
      //Demo access to the results (premium)
      //System.out.println("Premium: " + newQuote.premium);

      //Close socket, bye-bye
      socket.dispose();
      if( newQuote!=null)
      {
        analyzedProfile.put( ViaConstant.status, "OK");
        DecimalFormat df2 = new DecimalFormat("#.##");
        String newPremium = df2.format(newQuote.premium);
        if( analyzedProfile.get(ViaConstant.home_rent).equals("renters"))
        {
          analyzedProfile.put( ViaConstant.premium, newPremium + "/year");
        }
        else
        {
          analyzedProfile.put( ViaConstant.premium, newPremium + "/year");
        }
        
      }
      else
      {
        analyzedProfile.put( ViaConstant.status, "ERROR");
        analyzedProfile.put( ViaConstant.premium, "");
      }
       analyzedProfile.put( ViaConstant.company, "Homesite-G");
       return analyzedProfile;

    }
}
