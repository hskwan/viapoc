package com.rsocket;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.*;

@Getter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Setter
public class QuoteMessage {
  //result={"code":400,"errors":{"messages":[{"reason":"Address lock verification failed","message":"Address lock verification failed"}]},"message":"Bad Request"}

  public String code;
  //Added 2 new fields in quote-generator-0.0.7 on 08/04
  public String inquiryId;
  public String offerSetId;
  public String[] coverage_a_option;
  public String[] coverage_b_option;
  public String[] coverage_c_option;
  public String[] coverage_d_option;
  public String[] coverage_e_option;
  public String[] coverage_f_option;
  public JsonNode errors;
  public JsonNode messages;
  public String reason;
  public String message;
    public String first_name;
    public String last_name;
    public String middle_initial;
    public String birthday;
    public String email;
    public String address_1;
    public String address_2;
    public String city;
    public String state;
    public String zipCode;
    public String home_rent;
    public Integer units;
    public Integer people;
    public String company;
    public String deductible;
    public Double coverage_a;
    public Double coverage_b;
    public Double coverage_c;
    public Double coverage_d;
    public Double coverage_e;
    public Double coverage_f;
    public String effective_date;
    public Double premium;
}
