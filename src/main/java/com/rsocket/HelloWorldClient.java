package com.rsocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.rsocket.Payload;
import io.rsocket.RSocket;
import io.rsocket.core.RSocketConnector;
import io.rsocket.transport.netty.client.TcpClientTransport;
import io.rsocket.util.DefaultPayload;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class HelloWorldClient {

    private static final Logger logger = LoggerFactory.getLogger(HelloWorldClient.class);

    public static void main(String[] args) throws JsonProcessingException 
    {
      HelloWorldClient hc = new HelloWorldClient();
      hc.doMsg();
    }
    public Double doMsg()
    {
      RSocket  socket =
          RSocketConnector.connectWith(TcpClientTransport.create("localhost", 7000)).block();

      //Calcuating effectiveDate
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DAY_OF_YEAR, 1);
      SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd");
      String currentDate = DateFor.format(cal.getTime());

      //Building a json object (Can be done any way you like as long as the result is a JSON string.)
      JSONObject inquiryRequest = new JSONObject();
      inquiryRequest.put("address.1a345678.city", "Chicago");
      inquiryRequest.put("address.1a345678.line1", "3655 North Artesian Ave");
      inquiryRequest.put("address.1a345678.state", "IL");
      inquiryRequest.put("address.1a345678.zipcode", "60618");
      inquiryRequest.put("person.1ab45678.dob", "1991-02-02");
      inquiryRequest.put("person.1ab45678.email", "xyz.abcde@test.com");
      inquiryRequest.put("person.1ab45678.firstName", "xyz");
      inquiryRequest.put("person.1ab45678.lastName", "lastnameabcd");
      inquiryRequest.put("person.1ab45678.phone", "524-341-8225");
      inquiryRequest.put("policy.home.effectiveDate", currentDate);
      inquiryRequest.put("primaryInsured.person.creditDisclosureFlag", "true");
      //inquiryRequest.put("product.lob", "home");
      inquiryRequest.put("product.lob", "renters");


      //Connect to the quote generator and send the payload (JSON)
      String quoteResponse = socket.requestResponse(DefaultPayload.create(inquiryRequest.toString()))
          .map(Payload::getDataUtf8)
          .onErrorReturn("error")
          .block();

      QuoteMessage newQuote = null;
      try {
      //Map the resulting string into an object for easy access
      newQuote = new ObjectMapper().readValue(quoteResponse, QuoteMessage.class);
      }catch(Exception x)
      {
        x.printStackTrace();
      }
      //Demo access to the results (premium)
      //System.out.println("Premium: " + newQuote.premium);

      //Close socket, bye-bye
      socket.dispose();
      if( newQuote!=null)
        return newQuote.premium;
      else
        return -1.0;

    }
}
