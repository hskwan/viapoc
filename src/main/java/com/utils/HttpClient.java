package com.utils;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
//import org.apache.logging.log4j.core.Logger;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.File;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
//import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import org.apache.http.ssl.SSLContextBuilder;
/*
import org.apache.logging.log4j.core.Logger;
import utils.Exception;
import utils.File;
import utils.String;
import utils.StringBuffer;
*/
import javax.net.ssl.*;
import java.io.IOException;
import org.apache.http.conn.ssl.*;


//import MyUtils.;

public class HttpClient {

	public static void main(String[] args) {


	}
	public String doPost( String url, String body, String key,
			boolean debug)
	{
	  String result="";
	  try
	  {
	    if( url==null )
	      url = "https://westus.api.cognitive.microsoft.com/text/analytics/v2.0/keyPhrases";

	    CloseableHttpClient client = HttpClientBuilder.create().build();
	    HttpPost request = new HttpPost(url);
	    //HttpClient httpclient = new DefaultHttpClient();
	    //HttpGet request = new HttpGet(theUrl);
	    ///request.addHeader("api-key", utils.Constant.gpsearch_key);
	    //HttpResponse response = httpclient.execute(request);
	    // add request header
	    request.addHeader("User-Agent", ViaConstant.userAgent);
	    request.addHeader("Content-Type", "application/json");
	    if( key != null && key.length()>0)
	      request.addHeader("Ocp-Apim-Subscription-Key", key);	// MyUtils.GPTextAnalytics_key1);
	    // Request body
	    StringEntity reqEntity=null;
	    try{
	      reqEntity = new StringEntity(body);
	    }catch(Exception x){}
	    request.setEntity(reqEntity);

	    //HttpResponse response = httpclient.execute(request);
	    HttpResponse response = null;

	    try
	    {
	      response = client.execute(request);
	    }
	    catch( Exception x )
	    {
	      x.printStackTrace();
	      result = "client.execute exception:" + x.getMessage(); 
	    }
	    if( response != null )
	    {
	      System.out.println("Response Code : "
	          + response.getStatusLine().getStatusCode());
	      StringBuffer resultb = new StringBuffer();
	      try
	      {	
	        BufferedReader rd = new BufferedReader(
	            new InputStreamReader(response.getEntity().getContent()));


	        String line = "";

	        while ((line = rd.readLine()) != null) {
	          resultb.append(line);
	        }
	      }
	      catch( Exception x)
	      {

	      }
	      result = resultb.toString();
	    }
	  }
	  catch(Exception x ) {}
	  return result;
	}
	public String doKeyPhrasePost( String url, String body, 
			boolean debug)
	{
		String result="";
		try
		{
		if( url==null )
			url = "https://greywebapp.azurewebsites.net/gp_test/getkeyphrase";
		if(debug)System.out.println("doPost:url=" + url+"\n");
		if(debug)System.out.println("body=" + body+"\n");

		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpPost request = new HttpPost(url);
		//request.addHeader("User-Agent", MyUtils.userAgent);
		request.addHeader("Content-Type", "text/html");
		// Request body
		StringEntity reqEntity=null;
		try{
			reqEntity = new StringEntity(body);
		}catch(Exception x){}
        request.setEntity(reqEntity);

        //HttpResponse response = httpclient.execute(request);
		HttpResponse response = null;
		
		try
		{
			response = client.execute(request);
		}
		catch( Exception x )
		{
    		x.printStackTrace();
   			result = "client.execute exception:" + x.getMessage(); 
		}
		if( response != null )
		{
			System.out.println("Response Code : "
		                + response.getStatusLine().getStatusCode());
			StringBuffer resultb = new StringBuffer();
			try
			{	
				BufferedReader rd = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
		
				
				String line = "";

				while ((line = rd.readLine()) != null) {
					resultb.append(line);
				}
			}
			catch( Exception x)
			{
			
			}
			result = resultb.toString();
			if(debug)System.out.println("result=" + result+"\n");
		}
		}
		catch(Exception x ) {}
		return result;
	}

	public String doPostContentType( String url, String body, String contentType,
			boolean debug)
	{
		String result="";
		try
		{
		if( url==null )
		{
			result = "doPostContentType error : missing url" ;
			return result;
		}
		if(debug)System.out.println("doPost:url=" + url+"\n");
		if(debug)System.out.println("body=" + body+"\n");

		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpPost request = new HttpPost(url);
		
		
		//request.addHeader("User-Agent", MyUtils.userAgent);
		String curContent = contentType;
		if( curContent == null || curContent.length()<=0)
			curContent = "text/html";
		request.addHeader("Content-Type", curContent);
		// Request body
		StringEntity reqEntity=null;
		try{
			reqEntity = new StringEntity(body);
		}catch(Exception x){}
        request.setEntity(reqEntity);

        //HttpResponse response = httpclient.execute(request);
		HttpResponse response = null;
		
		try
		{
			response = client.execute(request);
		}
		catch( Exception x )
		{
			x.printStackTrace();
    		result = "client.execute exception:" + x.getMessage(); 
		}
		if( response != null )
		{
			System.out.println("Response Code : "
		                + response.getStatusLine().getStatusCode());
			StringBuffer resultb = new StringBuffer();
			try
			{	
				BufferedReader rd = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
		
				
				String line = "";

				while ((line = rd.readLine()) != null) {
					resultb.append(line);
				}
			}
			catch( Exception x)
			{
			
			}
			result = resultb.toString();
			if(debug)System.out.println("result=" + result+"\n");
		}
		}
		catch(Exception x ) {}
		return result;
	}
	
	public String doPatchContentType( String url, String body, String contentType,
			boolean debug)
	{
		String result="";
		try
		{
		if( url==null )
		{
			result = "doPatchContentType error : missing url" ;
			return result;
		}
		if(debug)System.out.println("doPatch:url=" + url+"\n");
		if(debug)System.out.println("body=" + body+"\n");
		
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpPatch request = new HttpPatch(url);
		String curContent = contentType;
		if( curContent == null || curContent.length()<=0)
			curContent = "text/html";
		request.addHeader("Content-Type", curContent);
		// Request body
		StringEntity reqEntity=null;
		try{
			reqEntity = new StringEntity(body);
		}catch(Exception x){}
        request.setEntity(reqEntity);

        //HttpResponse response = httpclient.execute(request);
		HttpResponse response = null;
		
		try
		{
			response = client.execute(request);
		}
		catch( Exception x )
		{
			x.printStackTrace();
    		result = "client.execute exception:" + x.getMessage(); 
		}
		if( response != null )
		{
			System.out.println("Response Code : "
		                + response.getStatusLine().getStatusCode());
			StringBuffer resultb = new StringBuffer();
			try
			{	
				BufferedReader rd = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
		
				
				String line = "";

				while ((line = rd.readLine()) != null) {
					resultb.append(line);
				}
			}
			catch( Exception x)
			{
			
			}
			result = resultb.toString();
			if(debug)System.out.println("result=" + result+"\n");
		}
		}
		catch(Exception x ) {}
		return result;
	}
	
	
	public String doHttpGet( String url)
  {
    String result="";
  
    
    CloseableHttpClient client = HttpClientBuilder
                .create()
                .build();
    System.out.println("httpclient:url=" + url);
    ///HttpPost request = new HttpPost(url);
    //HttpClient httpclient = new DefaultHttpClient();
    HttpGet request = new HttpGet(url);
    request.addHeader("User-Agent", ViaConstant.userAgent);
    HttpResponse response = null;
    
    try
    {
      response = client.execute(request);
    }
    catch( Exception x )
    {
      result = "doGet client.execute exception:" + x.getMessage(); 
      x.printStackTrace();
      System.out.println(result);
      return null;
    }
    if( response != null )
    {
      System.out.println("Response Code : "
                    + response.getStatusLine().getStatusCode());
      StringBuffer resultb = new StringBuffer();
      try
      { 
        BufferedReader rd = new BufferedReader(
            new InputStreamReader(response.getEntity().getContent()));
    
        
        String line = "";

        while ((line = rd.readLine()) != null) {
          resultb.append(line);
        }
      }
      catch( Exception x)
      {
            x.printStackTrace();
            System.out.println("doGet BufferedReader Exception: " + x.getMessage());
        return null;
      }
      result = resultb.toString()  ;

    }
    return result;
  }



	
	
	public String doHttpsGet( String url)
	{
		String result="";
	
		if( url==null )
			url = "https://westus.api.cognitive.microsoft.com/text/analytics/v2.0/keyPhrases";
		SSLConnectionSocketFactory connectionFactory = null;
		try
		{
	        // use the TrustSelfSignedStrategy to allow Self Signed Certificates
	        SSLContext sslContext = SSLContextBuilder
	                .create()
	                .loadTrustMaterial(new TrustSelfSignedStrategy())
	                .build();
	
	        // we can optionally disable hostname verification. 
	        // if you don't want to further weaken the security, you don't have to include this.
	        HostnameVerifier allowAllHosts = new NoopHostnameVerifier();
	        
	        // create an SSL Socket Factory to use the SSLContext with the trust self signed certificate strategy
	        // and allow all hosts verifier.
	        connectionFactory = new SSLConnectionSocketFactory(sslContext, allowAllHosts);
		}
		catch( Exception x)
		{
			x.printStackTrace();
			System.out.println("doGet Client exception build ssl factory:" + x.getMessage());
			return null;
		}
		
		CloseableHttpClient client = HttpClientBuilder
				.create()
                .setSSLSocketFactory(connectionFactory)
                .build();
		System.out.println("httpclient:url=" + url);
		///HttpPost request = new HttpPost(url);
		//HttpClient httpclient = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);
		///request.addHeader("api-key", utils.Constant.gpsearch_key);
		//HttpResponse response = httpclient.execute(request);
		// add request header
		request.addHeader("User-Agent", ViaConstant.userAgent);
		//request.addHeader("Content-Type", "application/json");
		String apiKey = "";
		HttpResponse response = null;
		
		try
		{
			response = client.execute(request);
		}
		catch( Exception x )
		{
			result = "doGet client.execute exception:" + x.getMessage(); 
			x.printStackTrace();
			System.out.println(result);
			return null;
		}
		if( response != null )
		{
			/*
			String 	Access_Control_Allow_Origin="";
			try{
				Access_Control_Allow_Origin=
					response.getFirstHeader(MyUtils.Access_Control_Allow_Origin).getValue();
			}catch( Exception x){}
			*/
			//if( Access_Control_Allow_Origin == null)
			//	Access_Control_Allow_Origin = "none";
			//System.out.println("Response Code : "
		    //            + response.getStatusLine().getStatusCode());
		  System.out.println("Response Code : "
		                + response.getStatusLine().getStatusCode());
			StringBuffer resultb = new StringBuffer();
			try
			{	
				BufferedReader rd = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
		
				
				String line = "";

				while ((line = rd.readLine()) != null) {
					resultb.append(line);
				}
			}
			catch( Exception x)
			{
        		x.printStackTrace();
        		System.out.println("doGet BufferedReader Exception: " + x.getMessage());
				return null;
			}
			/*
			if( Access_Control_Allow_Origin!= null && Access_Control_Allow_Origin.length()>0)
				result = "Access_Control_Allow_Origin =" + Access_Control_Allow_Origin + resultb.toString()  ;
			else
				*/
				result = resultb.toString()  ;

		}
		return result;
	}
	
	public String doPostImageFile2(String url, String imageFilepath, String textFileName)
	{
	  String resultString="";
    File file = new File(imageFilepath);
    FileBody fileBody = new FileBody(file, ContentType.DEFAULT_BINARY);

	  CloseableHttpClient httpClient = HttpClientBuilder
	      .create()
	      .build();
	  HttpEntity entity = MultipartEntityBuilder
	      .create()
	      .addTextBody("workflow", "\"coverage_workflow\"")
        .addBinaryBody("image", file, ContentType.MULTIPART_FORM_DATA, textFileName)
	      .build();

	  HttpPost httpPost = new HttpPost(url);
	  httpPost.setEntity(entity);
	  try
	  {
	    HttpResponse response = httpClient.execute(httpPost);
	    HttpEntity result = response.getEntity();
	    //System.out.println( response.toString());

	    if( response != null )
	    {
	      System.out.println("Response Code : "
	          + response.getStatusLine().getStatusCode());
	      StringBuffer resultb = new StringBuffer();
	      BufferedReader rd = new BufferedReader(
	          new InputStreamReader(response.getEntity().getContent()));


	      String line = "";

	      while ((line = rd.readLine()) != null) {
	        resultb.append(line);
	      }
	      resultString = resultb.toString();

	    }
	  }catch( Exception x)
	  {
	    x.printStackTrace();
	  }
	  return resultString;
	}
	
	public String doPostImageFile(String url, String textFileName)
	{

	  SSLConnectionSocketFactory connectionFactory = null;
	  try
	  {
	    // use the TrustSelfSignedStrategy to allow Self Signed Certificates
	    SSLContext sslContext = SSLContextBuilder
	        .create()
	        .loadTrustMaterial(new TrustSelfSignedStrategy())
	        .build();
	    HostnameVerifier allowAllHosts = new NoopHostnameVerifier();
	    connectionFactory = new SSLConnectionSocketFactory(sslContext, allowAllHosts);
	  }
	  catch( Exception x)
	  {
	    x.printStackTrace();
	    System.out.println("doGet Client exception build ssl factory:" + x.getMessage());
	    return null;
	  }

	  CloseableHttpClient httpClient = HttpClientBuilder
	      .create()
	      .setSSLSocketFactory(connectionFactory)
	      .build();


	  File file = new File(textFileName);
	  HttpPost post = new HttpPost(url);
	  FileBody fileBody = new FileBody(file, ContentType.DEFAULT_BINARY);
	  StringBody stringCoverageWorkflow = new StringBody("\"coverage_workflow\"", ContentType.MULTIPART_FORM_DATA);
	  MultipartEntityBuilder builder = MultipartEntityBuilder.create();
	  builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
	  //builder.addPart("image", fileBody);
	  builder.addBinaryBody("image", file, ContentType.DEFAULT_BINARY, "small.jpg");
	  //builder.addPart("workflow", stringCoverageWorkflow);
	  //builder.addTextBody("workflow", "\"coverage_workflow\"");
	  HttpEntity entity = builder.build();
	  post.setEntity(entity);
	  post.addHeader("Content-Type", "multipart/form-data"); //; boundary=--------------------------926408482945888734455752");
	  HttpResponse response = null;
	  String result = "";
	  System.out.println("url=" + url + " textFileName=" + textFileName );
	  try
	  {
	    response = httpClient.execute(post);
	  }
	  catch( Exception x)
	  {
	    System.out.println("httpClient.execute(post) exception:" + x.getMessage());
	    x.printStackTrace();
	  }
	  if( response != null )
	  {
	    System.out.println("Response Code : "
	        + response.getStatusLine().getStatusCode());
	    StringBuffer resultb = new StringBuffer();
	    try
	    { 
	      BufferedReader rd = new BufferedReader(
	          new InputStreamReader(response.getEntity().getContent()));


	      String line = "";

	      while ((line = rd.readLine()) != null) {
	        resultb.append(line);
	      }
	    }
	    catch( Exception x)
	    {
	      System.out.println("httpclient doPostImageFile exception:"  + x.getMessage());
	      x.printStackTrace();
	    }
	    result = resultb.toString();
	  }
	  return result;
	}

}
