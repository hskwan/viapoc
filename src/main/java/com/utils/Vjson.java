package com.utils;

//public class Vjson

//import java.io.OutputStreamWriter;
//import org.apache.logging.log4j.core.Logger;
//import java.util.ArrayList;
//import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

//import jdk.nashorn.internal.parser.Token;

public class Vjson {

public void parse(JSONObject json , Map<String,String> outMap, String keyname, boolean debug)
{
try
{

if(json == null || json.keySet() == null || json.keySet().iterator() == null )
  return;
String keyChain="";  //this is a unique json keyname by chain all the json name in each layer such as 
// categories:code:listing:rental_id
for(Iterator iterator = json.keySet().iterator(); json.keySet()!=null && iterator!= null && iterator.hasNext();) 
{
  keyChain = keyname;
    String key = (String) iterator.next();
    if( debug) System.out.println(json.get(key));
    //if(debug) oswOutLog.write("debug:parse: key="+ key +"\n");
      String val = null;
      try{
           Object value = json.get(key);
           if( keyChain == null || keyChain.length()<=0)
             keyChain = key;
           else
             keyChain += ":" + key;
           val = null;
     if( value instanceof String)
      val =  (String) value;
     else if( value instanceof Integer)
       val = "" + value;
     else if( value instanceof Long)
       val = "" + value;
     else if( value instanceof Double)
       val = "" + value;
     else if( value instanceof JSONArray)
     {
       JSONArray value_jsonarray = (JSONArray) value;
       for (int i = 0; i < value_jsonarray.size(); i++) 
       {
         val = null;
         value = value_jsonarray.get(i);
         if( value instanceof String)
            val =  (String) value;
         else if( value instanceof Integer)
             val = "" + value;
         else if( value instanceof Long)
             val = "" + value;
         else if( value instanceof Double)
           val = "" + value;
         else
         {
           JSONObject value_json = (JSONObject) value;
           if( value_json != null )
             parse(value_json, outMap, keyChain, debug);
         }
           if(val != null)
           {
            String curval = outMap.get( keyChain );
            if( curval == null )
              curval = "";
            if( curval.length() > 0 )
              curval += ",";
            curval += val.replace("\"",  "");
                outMap.put(keyChain, curval);
            //System.out.println( "add to outMap:" + keyChain + "=" + curval);
                val = null;
           }
       }
     }
     else
     {
       JSONObject value_json = (JSONObject) value;
       parse(value_json, outMap, keyChain, debug);
     }
       if(val != null){
          String curval = outMap.get( keyChain );
          if( curval == null )
            curval = "";
          if( curval.length() > 0 )
            curval += ",";
          curval += val.replace("\"",  "");
              outMap.put(keyChain, curval);
          //if( debug) { System.out.println( "add to outMap:" + keyChain + "=" + curval); }

       }
      }catch(Exception e)
      {
    e.printStackTrace();
          val = json.get(key).toString().replace("\"",  "");;
      }
      /*
      if(val != null){
          //key = key.toLowerCase(); //make it backward compatible
          outMap.put(key,val.replace("\"",  ""));
          //if(debug) oswOutLog.write("debug: parse: key=" + key + " val=" + val.toString() + "\n");
      }
**/
  

}
}catch(Exception x)
{
  x.printStackTrace();
  System.out.println(x);
}
  return ;  //out;
}

/*
* Get recursive json fields based on the vfieldNamesToGet format, starting from the rootElement underneath the rootElement
*      vfieldNameToGet_x =
* fields is to replace vfieldNameToGet and supplied by the caller as follows:
* The results are always returned in a tsv file as a table
* example:
* "fields=
* aggregations (root  element)
* |E1;  (the node name after the root element)
* listings:buckets::key,doc_count,top_score:value (the subsequent node to parse and to extract value)
* OR
* fields= 
* aggregations
* |E1
* |E1:doc_count:
* |E1:listings:doc_count_error_upper_bound:
*  
*  aggregations - the first one is always the root element. Empty means starting from the top
*  |E1 - '|' means starting the the root element, and look for a node key name E1
*   if the string DOESNOT ends with :, save the key (E1) as a column in the result
*   if the string ends with :, save the value of the key (E1) as a column in the result
*   listings:buckets:: - look for two consecutive keys listing and buckets and parse the JSONArray that follows:
*   key,doc_count,top_score:value - are the elements in the JSIONArray to be extracted
*   top_score:value means that the element is two levels deep in the JSONArray
*   |E1:doc_count: - starting from the root element, looks for two levels and extract the value of doc_count and 
*   
*   fields=aggregations;|E1;listings:buckets::key,doc_count,top_score:value should return a tsv as follows:
*   |E1  |E1:doc_count:  |E1:listings:doc_count_error_upper_bound:
*    E1  1064          0
*
*   fields=aggregations;|E1;listings:buckets::key,doc_count,top_score:value should return a tsv as follows:
*   |E1  listings:buckets::key doc_count top_score:value
*    E1  p257676                 19          1221.718994140625
*    E1  p1734636              42          1220.2391357421875
 *    E1  p72714                  16          1214.8699951171875
 *    E1  p911237                 11          1214.0438232421875
 *    
*          The vertical bar '|' means that the element is to be extract from the rootElement
*          If there is no vertical bar, it will be relative to the previous element
*      "|A1", // if json path DOESNOT ends with :, save the name (A1) as a column in the result
*      "doc_count:", // if json path ends with :, save the value of doc_count as a column in the result
*      "listings:buckets::key,doc_count,top_score :", //save the json array elements (comma separated) key's value a a column in the result
 *
*/
public  String[] getRecurJsonFields(String curJLine,  boolean debug, String []vfieldNameToGet)
{
Vector<String>  fields= new Vector<String>();
String [] fieldValues = null;
if( vfieldNameToGet.length <= 1 ) 
{ 
  fieldValues = new String[1];
  fieldValues[0]="getRecurJsonFields error= vfieldNameToGet has no elements to get ";
  return fieldValues;
}
String debugString = "";

//int nextFieldName = 0;
String curField="";
int irecord=1;

try
{
  //if(debug) {oswOutLog.write("debug: curJLine=" + curJLine + "\n"); oswOutLog.flush();}
  JSONParser jsonParser = new JSONParser();
  JSONObject jsonObject = (JSONObject)jsonParser.parse(curJLine);
  JSONObject jsonObjectRoot= null;
  // use the first element in vfieldNameToGet as the root
  if( vfieldNameToGet[0] != null && vfieldNameToGet[0].trim().length() > 0)
    jsonObjectRoot = (JSONObject) jsonObject.get( vfieldNameToGet[0].trim() );
  else
    jsonObjectRoot = jsonObject;
  /*
  //iterate thru all the keys in the rootObject
  Iterator<String> iterator = jsonObjectRoot.keySet().iterator();
  for(; iterator.hasNext(); ) 
  {
    String key = (String) iterator.next();
    //System.out.println(jsonObject.get(key));
    JSONObject curJsonObjectFromRoot = (JSONObject) jsonObjectRoot.get(key);

   */
    boolean hasElement = false;
    if( jsonObjectRoot != null && ! jsonObjectRoot.isEmpty() )
      hasElement = true;
    // set up the json object to use for each iteration of the vfieldNameToGet loop
    JSONObject jsonObjectForLoop = jsonObjectRoot; //default is to start from the root element
    JSONObject parentObjectForLoop = jsonObjectRoot; // sometimes it will be relative to the object produce3d from the previous element
    //create the header row
    curField = "";
    for( int k=1; k<vfieldNameToGet.length; k++)
    {
      if( curField.length() >0)
        curField += "\t";
      curField += vfieldNameToGet[k].replace( ',', '\t');
    }
    fields.add( curField );
    curField = "";
    
    // loop thru all element to extract to build the fields result
    if( hasElement == true ) 
    {
      // build one row of the output for the fields vector
      curField = "";
      String curvfieldName = "";
      for( int k=1; k<vfieldNameToGet.length; k++)
      {
        curvfieldName = vfieldNameToGet[k].trim();
        if( curvfieldName.startsWith( "|"))
        {
          jsonObjectForLoop = jsonObjectRoot;
          curvfieldName = curvfieldName.substring( 1 ); //remove the leading |
        }
        else
          jsonObjectForLoop = parentObjectForLoop; // if no vertical bar, use the object from the previous element
        if( curvfieldName.contains( "::") == true )  
        {
          // processing array elements
          String [] jsonArrayList = curvfieldName.split( "::" );
          //move to the root of the array
          //String[] subFieldNameList =  jsonArrayList[0].split( ":");
          JSONObject curJObject = null;
          Object curOObject = null;
          JSONArray foundArray = getJsonTreeArray( jsonArrayList[0], jsonObjectForLoop );
          if ( foundArray == null )
          {
            fields.add( "ERROR: missing json array=" + curvfieldName + " curJLine=" + curJLine);
            continue;
          }
          // convert the json object into an json array
          JSONArray indexObjectArray= foundArray;
          java.util.Iterator index = indexObjectArray.iterator();
          // take each value from the json array separately
          try
          {
            // iterate thru the json array and output one line for each iteration
            while (index != null && index.hasNext()) 
            {
              Map<String,String> outMap = new HashMap<String, String>();
              String curArrayField = curField; // each line start with what has been accumulated in curField
              JSONObject innerObj = null;
              Object curIndexNext = index.next();
              String innerString = "";
              try {
                innerObj = (JSONObject) curIndexNext;
              }
              catch( Exception x) {}
              if( innerObj == null )
              {
                innerString = (String) curIndexNext;
                String curValue ="";
                curValue = innerString;
                if( curValue == null )
                  curValue = "NONE";
                curValue = curValue.replace('\t', ' ');
                curValue = curValue.replace('\n', ' ');
                curValue = curValue.replace('\r', ' ');
                if( curArrayField.length() > 0 )  
                  curArrayField += "\t";
                curArrayField += curValue;

              }
              else
              {

                parse(innerObj, outMap, "", debug);
                if( debug) System.out.println( " debug: Json:getAllJsonFields: outMap.size=" + outMap.size());
                // vjsonArrayList will have a list of array elements to extract to form the table
                String[] vjsonArrayList = jsonArrayList[1].split( "," );
                if( vjsonArrayList != null && vjsonArrayList.length>0)
                {
                  for( int i=0; i<vjsonArrayList.length ; i++) //scroll thru all field names to get
                  {
                    String curFieldName = vjsonArrayList[i];
                    if(debug) { System.out.println( "debug: curFieldName=" + curFieldName ); }
                    //if( oswOutLog != null ) oswOutLog.flush();}
                    String curValue ="";
                    String curId32 = "";
                    curValue = outMap.get(curFieldName);
                    if( curValue == null )
                      curValue = "NONE";
                    curValue = curValue.replace('\t', ' ');
                    curValue = curValue.replace('\n', ' ');
                    curValue = curValue.replace('\r', ' ');
                    if( curArrayField.length() > 0 )  
                      curArrayField += "\t";
                    curArrayField += curValue;
                  }  //end of for vjsonArrayList loop
                  if( debug) System.out.println("getAllJsonFields curField=" + curField);
                }
              }
              fields.add( curArrayField );
              outMap.clear();
              irecord++;  
            }//end of while on json array element loop
          }
          catch( Exception x)
          {
            x.printStackTrace();
            //debugString += "Json exception in indexObjectArray.iterator(); loop";
          }

          
        } // end of [] case

        else if( ! curvfieldName.endsWith( ":") )
        {
          // this is to get the KEY, not value
          String foundFieldName = "NONE";
          // find the node name and save it into the fields table
          JSONObject foundObject = getJsonTreeObject( curvfieldName, jsonObjectForLoop );
          if( foundObject != null )
            foundFieldName = curvfieldName;
          if( curField.length() > 0 )
            curField += "\t";
          curField += foundFieldName;
          // make this foundObject the new parent for the next element in this loop
          parentObjectForLoop = foundObject; 

          
        }
        else if( curvfieldName.endsWith( ":") == true )
        {
          // find the node value and save it into the fields table
          // next remove the trailing :
          curvfieldName = curvfieldName.substring(0, curvfieldName.length()-1);
          String foundFieldValue = "NONE";
          String foundObjectValue = getJsonTreeValue( curvfieldName, jsonObjectForLoop );
          if( foundObjectValue != null )
            foundFieldValue = foundObjectValue;
          else
            foundFieldValue = "ERROR: foundObjectValue is null";
          if( curField.length() > 0 )
                curField += "\t";
          curField += foundFieldValue;
            
        }

      }// end of vFieldsToGet loop
      
      if( curField.trim().length()>0)
        fields.add(curField);
      curField = "";

    } // end of while hasElement
  // no more iteration loop } // end of iteration on root elements
}
catch( Exception x)
{
  x.printStackTrace();
  
    if( debug)
    { 
      {
        fieldValues = new String[1];
        fieldValues[0]="getAllJsonFields exception=" + x.getMessage() + debugString;
      }
    }
    return fieldValues;
    
}
finally
{
//    if( curField.trim().length()>0)
//      fields.add( curField.replace('\t', ' ')); // add the last partial field to list
    if( fields.size() > 0 )
    {
      fieldValues = new String[fields.size()];
      for( int i=0; i<fields.size();i++)
      {
        fieldValues[i] = fields.elementAt(i);
        //fieldValues = (String [])fields.toArray();
        
      }

    }
    else
    {
      fieldValues = new String[1];
      fieldValues[0]="getAllJsonFields error= cannot find any field" + debugString;

    }
}

return fieldValues;

}
/*
* Search the given jsonObject and return the object of the given key (curvfieldName)
*/
JSONObject getJsonTreeObject( String curvfieldName, JSONObject jsonObjectForLoop)
{
String[] subFieldNameList =  curvfieldName.split( ":");
Object newJObject = null;
JSONObject curObject = jsonObjectForLoop;
String fname = "";
try
{
  for( int p=0; curObject != null && p<subFieldNameList.length; p++)
  {
    fname = subFieldNameList[p].trim();
    newJObject = curObject.get( fname );
    try
    {
      //if( (p+1) < subFieldNameList.length) // re-assign it to an object only if the loop has one more round
        curObject = (JSONObject) newJObject;
    }catch( Exception x)
    {
      x.printStackTrace();
      return null;
    }
    
  }
}
catch( Exception x )
{
  System.out.println( " get fname=" +fname);
  System.out.println( "ERROR: sub-field navigation exception: jsonObjectForLoop=" + jsonObjectForLoop );
  System.out.println( "newJObject="+ newJObject.toString());
  return null;
}
return (JSONObject) newJObject;

}


/*
* Search the given jsonObject and return the value of the given key (curvfieldName)
*/
String getJsonTreeValue( String curvfieldName, JSONObject jsonObjectForLoop)
{
String[] subFieldNameList =  curvfieldName.split( ":");
Object newJObject = null;
String returnValue=null;
JSONObject curObject = jsonObjectForLoop;
String fname = "";
try
{
  for( int p=0; curObject != null && p<subFieldNameList.length; p++)
  {
    fname = subFieldNameList[p].trim();
    newJObject = curObject.get( fname );
    try
    {
      if( (p+1) < subFieldNameList.length) // re-assign it to an object only if the loop has one more round
        curObject = (JSONObject) newJObject;
    }catch( Exception x)
    {
      x.printStackTrace();
      return null;
    }
    
  }
}
catch( Exception x )
{
  System.out.println( " get fname=" +fname);
  System.out.println( "ERROR: sub-field navigation exception: jsonObjectForLoop=" + jsonObjectForLoop );
  System.out.println( "newJObject="+ newJObject.toString());
  return null;
}
if( newJObject == null)
  return "NONE";
else
  return newJObject.toString();

}

/*
* Search the given jsonObject and return the array of the given key (curvfieldName)
*/
JSONArray getJsonTreeArray( String curvfieldName, JSONObject jsonObjectForLoop)
{
String[] subFieldNameList =  curvfieldName.split( ":");
Object newJObject = null;
String returnValue=null;
JSONObject curObject = jsonObjectForLoop;
String fname = "";
try
{
  for( int p=0; curObject != null && p<subFieldNameList.length; p++)
  {
    fname = subFieldNameList[p].trim();
    newJObject = curObject.get( fname );
    try
    {
      if( (p+1) < subFieldNameList.length) // re-assign it to an object only if the loop has one more round
        curObject = (JSONObject) newJObject;
    }catch( Exception x)
    {
      x.printStackTrace();
      return null;
    }
    
  }
  JSONArray jsonArrayResult = (JSONArray)newJObject;
  return (JSONArray)newJObject;
}
catch( Exception x )
{
  System.out.println( " get fname=" +fname);
  System.out.println( "ERROR: sub-field navigation exception: jsonObjectForLoop=" + jsonObjectForLoop );
  System.out.println( "newJObject="+ newJObject.toString());
  return null;
}

}



/*
* Search the given jsonObject and return the object of the given key (curvfieldName)
*/
Object getTreeObject( String curvfieldName, JSONObject jsonObjectForLoop)
{
String[] subFieldNameList =  curvfieldName.split( ":");
Object newJObject = null;
JSONObject curObject = jsonObjectForLoop;
//JSONArray curArrayObject = null;
String fname = "";
try
{
  for( int p=0; curObject != null && p<subFieldNameList.length; p++)
  {
    fname = subFieldNameList[p].trim();
    newJObject = curObject.get( fname );
    
    try
    {
      //if( (p+1) < subFieldNameList.length) // re-assign it to an object only if the loop has one more round
      
        curObject = (JSONObject)newJObject;
    }catch( Exception x)
    {
      x.printStackTrace();
      return null;
    }
    
    
  }
}
catch( Exception x )
{
  System.out.println( " get fname=" +fname);
  System.out.println( "ERROR: sub-field navigation exception: jsonObjectForLoop=" + jsonObjectForLoop );
  System.out.println( "newJObject="+ newJObject.toString());
  return null;
}
return newJObject;

}

public  String[] getAllJsonFields(String curJLine, String rootElement, boolean debug, String []vfieldNameToGet)
{
//curJLine="{\"took\":78,\"hits\":{\"total\":5680,\"hits\":[{\"_index\":\"20160819_standard_revision11\"},{\"_index\":\"20160820_standard_revision11\"}]}}";
//curJLine="{\"took\":78,\"hits\":{\"total\":5680}}";

Vector<String>  fields= new Vector<String>();
String [] fieldValues = null;
String debugString = "";

//int nextFieldName = 0;
String curField="";
int irecord=1;

try
{
  //if(debug) {oswOutLog.write("debug: curJLine=" + curJLine + "\n"); oswOutLog.flush();}
  //java.io.FileReader reader = new java.io.FileReader("/Users/hwan/Documents/adobe/query_log/test_json.txt");
  JSONParser jsonParser = new JSONParser();
  JSONObject jsonObject = (JSONObject)jsonParser.parse(curJLine);
  //if(debug) debugString += "getAllJsonFields jsonObject=" + jsonObject.toString() +"\n";// oswOutLog.write("getAllJsonFields jsonObject=" + jsonObject.toString());
  //JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
  Map<String,String> outhit = new HashMap<String, String>();
  

    //parse(jsonObject,out, debug, oswOutLog);
  //oswOutLog.flush();
  ///JSONObject hitsObject = (JSONObject)jsonObject.get("value");     
  ///parse(hitsObject, outhit,debug, oswOutLog);
  
  ///JSONArray indexObjectArray=  (JSONArray) hitsObject.get("value");
  JSONArray indexObjectArray=  (JSONArray) jsonObject.get(rootElement);
  java.util.Iterator index = indexObjectArray.iterator();
  // take each value from the json array separately
  try
  {
    while (index != null && index.hasNext()) 
    {
      JSONObject innerObj = (JSONObject) index.next();
      ///JSONObject innerObj =jsonObject;
      //if(debug) debugString += "debug: innerObj=" + innerObj.toString() + "\n";
      Map<String,String> outMap = new HashMap<String, String>();
      parse(innerObj, outMap, "", true);
      if( debug) System.out.println( " debug: Json:getAllJsonFields: outMap.size=" + outMap.size());
      //if( oswOutLog != null ) oswOutLog.flush();
      
      //if(debug) debugString += "debug: vfieldNameToGet.length=" + vfieldNameToGet.length + "\n";
      //if( oswOutLog != null ) oswOutLog.flush();
      if( vfieldNameToGet != null && vfieldNameToGet.length>0)
      {
        for( int i=0; i<vfieldNameToGet.length; i++) //scroll thru all field names to get
        {
          String curFieldName = vfieldNameToGet[i];
          if(debug) { System.out.println( "debug: curFieldName=" + curFieldName ); }
          //if( oswOutLog != null ) oswOutLog.flush();}
          String curValue ="";
          String curId32 = "";
          curValue = outMap.get(curFieldName);
          if(curValue == null)
          {
            curValue = outhit.get(curFieldName); //check hit object as well
            if(curValue == null)
              curValue = " ";
          }
          curValue = curValue.replace('\t', ' ');
          curValue = curValue.replace('\n', ' ');
          curValue = curValue.replace('\r', ' ');
          if( i>0)  //curField.length()>0)
            curField += "\t";
          curField += curValue;
        }  //end of for vFieldName loop
        if( debug) System.out.println("getAllJsonFields curField=" + curField);
        if( curField.trim().length()>0)
          fields.add(curField);
      }
      else
      {
        try
        {
            Iterator<Entry<String, String>> it = outMap.entrySet().iterator();
            while (it.hasNext()) 
            {
                @SuppressWarnings("rawtypes")
            Map.Entry pair = (Map.Entry)it.next();
                String name= (String)pair.getKey();
                String value= (String)pair.getValue();
            if( value.startsWith("[") && value.endsWith("]"))
              value = value.substring(1, value.length()-1);
            
                //System.out.println(pair.getKey() + " = " + pair.getValue());
            if( debug) System.out.println("getAllJsonFields curField=" + curField + "\n");
            if( value.length()>0)
              fields.add(name + " = " + value);
                it.remove(); // avoids a ConcurrentModificationException
            }
        }
        catch(Exception x )
        {
          x.printStackTrace();
          //debugString += "Json exception in outMap iteration loop";
        }
        
      }
        
      curField = "";
      outMap.clear();
      irecord++;  
    }//end of while
  }
  catch( Exception x)
  {
    x.printStackTrace();
    //debugString += "Json exception in indexObjectArray.iterator(); loop";
  }

}
catch( Exception x)
{
  x.printStackTrace();
  
    if( debug)
    { 
      {
        fieldValues = new String[1];
        fieldValues[0]="getAllJsonFields exception=" + x.getMessage() + debugString;
      }
    }
    return fieldValues;
    
}
finally
{
    if( curField.trim().length()>0)
      fields.add( curField.replace('\t', ' ')); // add the last partial field to list
    if( fields.size() > 0 )
    {
      fieldValues = new String[fields.size()];
      for( int i=0; i<fields.size();i++)
      {
        fieldValues[i] = fields.elementAt(i);
        //fieldValues = (String [])fields.toArray();
        
      }

    }
    else
    {
      fieldValues = new String[1];
      fieldValues[0]="getAllJsonFields error= cannot find any field" + debugString;

    }
}

return fieldValues;

}
public String getOneFieldValue( String curJLine, String name )
{
//curJLine = "{\"elapseTime\":\"78 milliseconds.\"}";
String value = "";  //"getOneFieldValue: " + curJLine;
try
{
  //if(debug) {oswOutLog.write("debug: curJLine=" + curJLine + "\n"); oswOutLog.flush();}
  //java.io.FileReader reader = new java.io.FileReader("/Users/hwan/Documents/adobe/query_log/test_json.txt");
  JSONParser jsonParser = new JSONParser();
  JSONObject jsonObject = (JSONObject)jsonParser.parse(curJLine);
  Map<String,String> jsonmap = new HashMap<String, String>();
  parse(jsonObject,jsonmap, "", true);
  //value = "getOneFieldValue:" + jsonmap.toString();
  Object value_of_name = jsonObject.get( name );
  if( value_of_name == null )
    value = "";
  else
  {
    try
    {
      if( value_of_name instanceof String)
        value =  (String) value_of_name;
      else if( value_of_name instanceof JSONArray)
        value = value_of_name.toString();
      else if( value_of_name instanceof Integer)
        value = "" + value_of_name;
      else
        value = value_of_name.toString();
    }catch( Exception x)
    {
      x.printStackTrace();
    }
  }
}
catch ( Exception x)
{
  x.printStackTrace();
  value += "Exception:" + x.getMessage();
}
return value;
}

public  String getOneFieldValueFromRoot
    (String curJLine, String rootElement, boolean debug, 
  String fieldname)
{

String debugString = "";

//int nextFieldName = 0;
String curValue ="NOT_FOUND_YET";

try
{
  JSONParser jsonParser = new JSONParser();
  JSONObject jsonObject = (JSONObject)jsonParser.parse(curJLine);
  if(debug) debugString += "getAllJsonFields jsonObject=" + jsonObject.toString() +"\n";// oswOutLog.write("getAllJsonFields jsonObject=" + jsonObject.toString());
  //JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
  
  JSONArray indexObjectArray=  (JSONArray) jsonObject.get(rootElement);
  java.util.Iterator index = indexObjectArray.iterator();
  // take each value from the json array separately
  try
  {
    while (index != null && index.hasNext()) 
    {
      JSONObject innerObj = (JSONObject) index.next();
      //if(debug) debugString += "debug: innerObj=" + innerObj.toString() + "\n";
      Map<String,String> outMap = new HashMap<String, String>();
      parse(innerObj, outMap, "",false);
      if( debug) 
        debugString += " debug: Json:getAllJsonFields: outMap.size=" + outMap.size();
      
      //process user requested fields in vfieldNameToGet first 
      if( fieldname != null && fieldname.length()>0)
      {
        {
          String curFieldName = fieldname.trim();
          if(debug) debugString += "debug: curFieldName=" + curFieldName + "<br>"; 
          curValue = outMap.get(curFieldName);
          if( curValue==null || curValue.length()<=0)
            curValue = "NONE";
          {
            curValue = curValue.replace('\t', ' ');
            curValue = curValue.replace('\n', ' ');
            curValue = curValue.replace('\r', ' ');
            curValue = curValue.replace("\"", "");
            curValue = curValue.replace(", ", ",");
            curValue = curValue.replace(" ,", ",");

            if( curValue.startsWith("[") &&
                curValue.endsWith("]") )
            {
              curValue = curValue.substring(1, curValue.length()-1);
            }
            curValue = curValue.trim();

          }
        }  //end of for vFieldName loop
      }
      outMap.clear();
    }//end of while
  }
  catch( Exception x)
  {
    x.printStackTrace();
    debugString += "Json exception in indexObjectArray.iterator(); loop";
  }

}
catch( Exception x)
{
  x.printStackTrace();
  return curValue;
    
}
  return curValue;


}




public int countMatchingToken( String query, String value)
{
String[] queryTokens = query.replace('+', ' ').toLowerCase().split(" ");  //"\\s+");
String curValue = value.toLowerCase();
//curValue = value.replace('+', ' ').replace('"', ' ').replace(',', ' ').replace('[', ' ').replace(']', ' ').replace(':',' ');  //"\\s+");
String[] curValueItem = curValue.split(" ");
int count = 0;
for( int i=0; queryTokens!=null && queryTokens.length>0 && i<queryTokens.length; i++)
{
  if( curValue.contains(queryTokens[i]))
    ++count;

  /*
  for(int j=0; j<curValueItem.length; j++)
  {
    //if( queryTokens[i].equals(curValueItem[j]))
  }
  */
}
return count;
}
}

