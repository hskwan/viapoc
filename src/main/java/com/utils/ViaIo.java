package com.utils;


import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
//import com.utils.*;
//import com.utils.ViaConstant;

public class ViaIo {


    public static Scanner readWebContentFile(ServletContext ctx, String fileName) {
        try {

            String helpPath = ctx.getRealPath("")
                    + File.separator
                    + "data"
                    + File.separator
                    + fileName;

            return new Scanner(new File(helpPath), "UTF-8");

        } catch (Exception e) {
            System.out.println("IoUtil.readWebContentFile was unable to open file "+fileName);
            return null;
        }
    }

    public static void writeToHttpOut(Scanner scanner, HttpServletResponse response) {
        try {

            String curLine = "";
            PrintWriter HttpOut = response.getWriter();
            while (scanner.hasNext()) {
                curLine = scanner.nextLine();
                HttpOut.println(curLine);
            }
            HttpOut.close();

        } catch (Exception e) {
          System.out.println("IoUtil.writeToHttpOut");
        }
    }

    public static void writeToHttpOut(String output, PrintWriter HttpOut) {
        try {

            //PrintWriter HttpOut = response.getWriter();
            HttpOut.println(output);
            //HttpOut.close();

        } catch (Exception e) {
          System.out.println("IoUtil.writeToHttpOut");
        }
    }

}
