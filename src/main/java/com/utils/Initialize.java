package com.utils;


import java.io.File;

import javax.servlet.*;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class Initialize {

  public static void main(String[] args) 
  {
    // TODO Auto-generated method stub

  }
  /**
   * check and initialize all data directories as needed
   * and return the dataDirectoryBase
   */
  public String  checkInitDir(ServletRequest request)
  {
    String dataDirectoryBase="";
    String serverDataPath = "";
    
    serverDataPath = ViaConstant.dataDirectoryAWS;
    File findFile = new File( serverDataPath );
    if (!findFile.exists())
    {
      serverDataPath = ViaConstant.dataDirectoryLaptop;
      findFile = new File( serverDataPath );
      if (!findFile.exists())
      {
        String serverPath = request.getServletContext().getRealPath("");
        System.out.println("initialize: serverpath=" + serverPath);
        InetAddress ip;
        String hostname;

        try
        {
        ip = InetAddress.getLocalHost();
        hostname = ip.getHostName();
        System.out.println("Your current IP address : " + ip);
        System.out.println("Your current Hostname : " + hostname);
        }
        catch( Exception x)
        {
          x.printStackTrace();
        }

        if( serverPath.endsWith("/"))
          serverDataPath= serverPath +
          ViaConstant.dataDirectory;
        else
          serverDataPath= serverPath
          + File.separator + 
          ViaConstant.dataDirectory;
      }
    }
    
    // check and init data directory
    findFile = new File( serverDataPath );
    if (!findFile.exists())
    {
      System.out.println( " data directory not found at: " + serverDataPath + ". Will create a new one.");
      try
      {
        if(findFile.mkdir())
        {
          System.out.println(serverDataPath+" directory Created");

        }
      }
      catch( Exception x )
      {
        System.out.println( "ERROR:  data directory cannot be created at: " + serverDataPath);
        return null;

      }
    }
    dataDirectoryBase = serverDataPath;
    ViaConstant.dataDirectoryBase =serverDataPath;
    return dataDirectoryBase;
  }

  public String  checkInitUserDir(ServletRequest request, String userName)
  {
    String dataDirectoryBase="";
    String serverDataPath = "";
    
    dataDirectoryBase = checkInitDir( request);
    String userPath = dataDirectoryBase + File.separator + userName;
    File findFile = new File( userPath );
    if (!findFile.exists())
    {
      System.out.println( " user directory not found at: " + userPath + ". Will create a new one.");
      try
      {
        if(findFile.mkdir())
        {
          System.out.println(userPath+" directory Created");

        }
      }
      catch( Exception x )
      {
        System.out.println( "ERROR:  user directory cannot be created at: " + userPath);
        return null;

      }
    }

    return userPath;
  }

  public boolean isdigit( String acckey )
  {
    if( acckey.matches("[0-9]+") )  //&& acckey.length() > 2 )
      return true;
    else
      return false;
  }
  /* an static function to return the first n characters
   * of an string s + a sufix. if string is too short, return the entire string
   */
  public static String subString(String s, int count, String sufix )
  {
    if( s.length()<count)
      return s;
    else
      return s.substring(0, count) + sufix;
  }
}
