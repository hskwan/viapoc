package com.utils;

import java.util.ArrayList;
import java.util.HashMap;

public class ViaConstant
{
  //http://ec2-54-227-84-141.compute-1.amazonaws.com:8080/am/answer?q=what+is+moon
  public static String amurl = "http://ec2-54-227-84-141.compute-1.amazonaws.com:8080/am/answer?";
  //arrayList of 02421, waltham, ma
  public static float confidenceThreshold = (float)50.00;
  public static String dataDirectoryBase = "";
  public static String agentG = "agentG"; // agent G's icon image
  public static String option = "option"; // show options to user
  public static String zipMarker = " ......"; // a list by adding all lists together for autocomplete
  public static String qnaMarker = " ....."; // a list by adding all lists together for autocomplete
  public static String addressMarker = " ...."; // a list by adding all lists together for autocomplete
  public static String birthdayMarker = " ..."; // a list by adding all lists together for autocomplete
  public static String effectivedayMarker = " .."; // a list by adding all lists together for autocomplete
  public static String otherMarker = " ."; // a list by adding all lists together for autocomplete
  public static ArrayList<String> combineList = new ArrayList<String>(); // a list by adding all lists together for autocomplete
  public static ArrayList<String> viaZipCityStateList = new ArrayList<String>();
  public static ArrayList<String> birthdayList = new ArrayList<String>();
  public static ArrayList<String> otherContextList = new ArrayList<String>();
  public static ArrayList<String> effectivedayList = new ArrayList<String>();
  public static ArrayList<String> uniqueStreetNameList = new ArrayList<String>();
  public static HashMap<String, ArrayList<String>>  streetsMap = new HashMap<String, ArrayList<String>>();
  //public static HashMap<String, String> viaAddressMap = new HashMap<String, String>();
  
  
  public static final String userContext = "userContext";
  public static final String him = "him";
  public static final String userConversation = "userConversation";
  // haspMap to look up quote by zipcode
  public static HashMap<String, String> viaZipQuoteMap = new HashMap<String, String>();
  public static HashMap<String, String> qnaMap = new HashMap<String, String>();
  public static ArrayList<String> qnaList = new ArrayList<String>();
  // HashMap to look up possible address by zip code
  public static HashMap<String, String> viaZipAddressMap = new HashMap<String, String>();
  public static final String errorString = "errorString";
  public static final String debugString= "debugString";
  public static final String dataDirectoryAWS = "/home/ubuntu/tomcat/apache-tomcat-8.5.56/data";
  public static final String dataDirectoryLaptop = "/Users/skwan/tomcat_8.5.37/data";
  public static final String public_folder = "public";
  public static final String data_folder = "data";
  public static final String autocompletequery = "autocompletequery";

  public static final String statusString = "statusString";
  public static final String warningString = "warningString";
  public static final String userAgent = "Mozilla/5.0";
  public static final String dataDirectory = "../../data";
  public static final String dataUserFile = "user.tsv";
  public static final String version = "v0.9";

  public static final String command ="command"; //get_quote
  public static final String debug ="debug"; // debug mode
  public static final String status ="status";
  public static final String phone ="phone";
  public static final String first_name ="first_name";
  public static final String last_name = "last_name"; //,score - required
  public static final String middle_initial = "middle_initial"; //,score
  public static final String birthday = "birthday"; //,score - required
  public static final String email = "emai";  //,score - required
  public static final String address_1 = "address_1";  //;  //,score - required
  public static final String address_2 = "address_2";  //,score
  public static final String city = "city"; //,score - required
  public static final String state = "state";  //,score - required
  public static final String zip = "zip";  //,score - required
  public static final String home_rent = "home_rent";  //,score - required
  public static final String units = "units";  //,score
  public static final String people = "people"; //,score
  public static final String premium = "premium";  //,score
  public static final String company = "company";  //,score
  public static final String deductible = "deductible"; //,score
  public static final String personal = "personal"; //,score
  public static final String liability = "liability"; //,score
  public static final String yousay = "yousay"; //,score
  public static final String offerSetId = "offerSetId"; //,offerSetId
  public static final String inquiryId = "inquiryId"; //,inquiryId
  
  
  public static final String coverage_a ="coverage_a"; //,score
  public static final String coverage_b = "coverage_b"; //,score
  public static final String coverage_c = "coverage_c"; //,score
  public static final String coverage_d = "coverage_d"; //,score
  public static final String coverage_e = "coverage_e"; //,score
  public static final String coverage_f = "coverage_f"; //,score
  
  //these are just name of the "key" in json object, that's why string and not array
  public static final String coverage_a_option = "coverage_a_option"; //,score
  public static final String coverage_b_option = "coverage_b_option"; //,score
  public static final String coverage_c_option = "coverage_c_option"; //,score
  public static final String coverage_d_option = "coverage_d_option"; //,score
  public static final String coverage_e_option = "coverage_e_option"; //,score
  public static final String coverage_f_option = "coverage_f_option"; //,score
  
  public static final String effective_date = "effective_date";  //,score
  public static final String confidence = "confidence";  //,confidence


   
  
  
  
}

