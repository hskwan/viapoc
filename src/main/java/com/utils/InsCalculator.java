package com.utils;

import com.bean.UserContext;

public class InsCalculator
{
  /*
   * calculate the insurance based on the user context collected
   *   
    firstName="";
    lastName="";
    email="";
    birthday="";
    zip="";
    city="";
    state="";
    address1="";
    address2="";
    personal=0;
    deductible=0;
    liability=0;
    quote= "";
    firstNameP=false;
    lastNameP=false;
250 deductible
500 deductible
1000 deductible
1500 deductible
2500 deductible

10000 personal property
20000 personal property
50000 personal property
150000 personal property
250000 personal property

100000 liability
200000 liability
500000 liability
750000 liability
1000000 liability

   */
  public String getInsurance( UserContext cx)
  {
    //zipcode city    state   Property_Coverage       Deductible      Liability       high    low     average
    // 71937   Cove    AR      40000   1000    300000  1426    91      408
    String quote="";
    
    String quoteLine = ViaConstant.viaZipQuoteMap.get(cx.getZip());
    if( quoteLine == null )
      quote = "20";
    else
    {
      String []quoteparts = quoteLine.split("\t");
      int high = Integer.parseInt(quoteparts[6]);
      int low = Integer.parseInt(quoteparts[7]);
      int average = Integer.parseInt(quoteparts[8]);
      int increment = (high-low)/19;
      int []steps = new int[20];
      
      for( int i=0; i<steps.length; i++ )
      {
        steps[i] = low + i * increment;
      }
      int cstep = 0;
      if( cx.getZip() != null && cx.getZip().length()>0 )
        cstep ++;
      if( cx.getFirstName() != null && cx.getFirstName().length()>0 )
        cstep ++;
      if( cx.getLastName() != null && cx.getLastName().length()>0 )
        cstep ++;
      if( cx.getBirthday() != null && cx.getBirthday().length()>0 )
        cstep ++;
      if( cx.getEmail() != null && cx.getEmail().length()>0 )
        cstep ++;
      if( cx.getAddress1() != null && cx.getAddress1().length()>0 )
        cstep ++;
      if( cx.getAddress2() != null && cx.getAddress2().length()>0 )
        cstep ++;
      if( cx.getPhone() != null && cx.getPhone().length()>0 )
        cstep ++;
      int curD = cx.getDeductible();
      {
        if( curD <= 250 )
          cstep = cstep + 2;
        else if( curD == 1000 )
          cstep = cstep + 1;
        else if( curD == 2500 )
          cstep = cstep + 0;
      }
      int curP = cx.getPersonal();
      {
        if( curP <= 10000 )
          cstep = cstep + 2;
        else if( curP == 50000 )
          cstep = cstep + 1;
        else if( curP == 250000 )
          cstep = cstep + 0;
      }
      int curL = cx.getLiability();
      {
        if( curL <= 100000 )
          cstep = cstep + 2;
        else if( curL == 500000 )
          cstep = cstep + 1;
        else if( curL == 1000000 )
          cstep = cstep + 0;
      }
      int discount= 9-cstep;
      if( discount <= 0)
        discount = 0;
      quote = "" + steps[discount]/12;

       
      
    }
    
    return quote;
  }
}
