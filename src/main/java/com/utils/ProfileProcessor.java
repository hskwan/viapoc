package com.utils;

import java.util.HashMap;
import java.util.Map;

public class ProfileProcessor
{

  /*
   * for the given hashMap, retrieve the value of the key, 
   * check the score, and replace it with replacement if it is lower than the threshold
   * return the final value
   */
  public String getParamneter( HashMap<String, String> parameters, String key, int threshold, String replacement)
  {
    String finalValue="";
    String valueScore = parameters.get(key);
    if( valueScore == null || valueScore.length()<=0)
      finalValue = replacement;
    else if( threshold == 0 )
    {
      finalValue = valueScore; // if no threshold, just copy the value and score and return;
    }
    else
    {
      Integer score = 0;
      String[] valueScoreList = valueScore.split(",");
      if( valueScoreList.length>0 )
        valueScoreList[0] = valueScoreList[0].trim();
      if( valueScoreList.length>1 )
      {
        valueScoreList[1] = valueScoreList[1].trim();
        try
        { 
          score = Integer.parseInt(valueScoreList[1]);
        }
        catch( Exception x)
        {}
      }
      if( score < threshold)
        valueScoreList[0] = replacement; 
      finalValue = valueScoreList[0];
    }
    return finalValue;
  }

  /*
   * for the given hashMap, retrieve the score of the key, 
   */
  public Integer getParamneterScore( HashMap<String, String> parameters, String key)
  {
    int finalScore=0;
    String valueScore = parameters.get(key);
    if( valueScore == null || valueScore.length()<=0)
      return finalScore;
    else
    {
      Integer score = 0;
      String[] valueScoreList = valueScore.split(",");
      if( valueScoreList.length>1 )
      {
        valueScoreList[1] = valueScoreList[1].trim();
        try
        { 
          score = Integer.parseInt(valueScoreList[1]);
        }
        catch( Exception x)
        {}
      }
      finalScore = score;
    }
    return finalScore;
  }
  
  /*
   * for the given hashMap, retrieve the value of the key, 
   */
  public String getParamneterValue( HashMap<String, String> parameters, String key)
  {
    String finalValue="";
    String valueScore = parameters.get(key);
    if( valueScore == null || valueScore.length()<=0)
      return finalValue;
    else
    {
      Integer score = 0;
      String[] valueScoreList = valueScore.split(",");
      if( valueScoreList.length>0 )
      {
        finalValue = valueScoreList[0];
      }
    }
    return finalValue;
  }
}
