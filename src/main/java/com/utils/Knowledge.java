package com.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Knowledge
{

  /*
  public void initZipCityStateList( String baseDataDirectory)
  {
    try
    {
      if( ViaConstant.viaZipCityStateList.size() <= 0 )
      {
        String uploadPath = baseDataDirectory;
        //uploadPath += File.separator + "public/ibot_city_state_uniq_length.txt";
        uploadPath += File.separator + "public/street_names.tsv";
        File downloadFile = new File(uploadPath );
        Scanner scan= new Scanner(downloadFile, "UTF-8"); 
        int count = 0;
        int countMax = 50000;
        String line=""; // no need to skip for this file scan.nextLine(); //skip header row
        line = scan.nextLine(); //skip header row

        for( ; count<countMax && scan.hasNextLine(); count++)
        {
          line = scan.nextLine(); //skip header row
          //String[] lineToken = line.split("\t");
          if( line != null && line.length() > 0 )
          {
            String query = line.toLowerCase();
            String [] lineList = query.split("\t");
            if( lineList.length >= 4)
            {
              String zip = lineList[0];
              if( zip.length() <= 4 )
                zip = "0" + zip;
              ViaConstant.viaZipCityStateList.add( zip + " "+ lineList[1] + " " + lineList[2] + ViaConstant.zipMarker );
              //ViaConstant.viaZipQuoteMap.put(lineList[0], line);
              ViaConstant.viaZipAddressMap.put(zip, lineList[3]);
            }
          }
        }
        scan.close();
        System.out.println( "initZipCityStateList done");
      }
    }
    catch( Exception x) {
      x.printStackTrace();
    }
  }
  */
  
  public void initZipQuoteMap( String baseDataDirectory)
  {
    try
    {
      if( ViaConstant.viaZipQuoteMap.size() <= 0 )
      {
        String uploadPath = baseDataDirectory;
        uploadPath += File.separator + "public/ibot_city_state_uniq_length.txt";
        //uploadPath += File.separator + "public/insurance_calculator.tsv";
        File downloadFile = new File(uploadPath );
        Scanner scan= new Scanner(downloadFile, "UTF-8"); 
        int count = 0;
        int countMax = 50000;
        String line=""; // no need to skip for this file scan.nextLine(); //skip header row
        line = scan.nextLine(); //skip header row

        for( ; count<countMax && scan.hasNextLine(); count++)
        {
          line = scan.nextLine(); //skip header row
          //String[] lineToken = line.split("\t");
          if( line != null && line.length() > 0 )
          {
            String query = line.toLowerCase();
            String [] lineList = query.split("\t");
            if( lineList.length >= 9)
            {
              ViaConstant.viaZipQuoteMap.put(lineList[0], line.toLowerCase());
              ViaConstant.viaZipCityStateList.add( lineList[0] + " "+ lineList[1] + " " + lineList[2]  + ViaConstant.zipMarker  );
            }
          }
        }
        scan.close();
        System.out.println( "initZipQuoteMap done");

      }
    }
    catch( Exception x) {
      x.printStackTrace();
    }
  }

  public void initQnaMap( String baseDataDirectory)
  {
    try
    {
      if( ViaConstant.qnaMap.size() <= 0 )
      {
        String uploadPath = baseDataDirectory;
        uploadPath += File.separator + "public/qna.tsv";
        //uploadPath += File.separator + "public/insurance_calculator.tsv";
        File downloadFile = new File(uploadPath );
        Scanner scan= new Scanner(downloadFile, "UTF-8"); 
        int count = 0;
        int countMax = 50000;
        String line=""; // no need to skip for this file scan.nextLine(); //skip header row
        line = scan.nextLine(); //skip header row

        for( ; count<countMax && scan.hasNextLine(); count++)
        {
          line = scan.nextLine(); //skip header row
          //String[] lineToken = line.split("\t");
          if( line != null && line.length() > 0 )
          {
            String query = line.toLowerCase();
            String [] lineList = query.split("\t");
            if( lineList.length >= 2)
            {
              ViaConstant.qnaList.add(lineList[0] + ViaConstant.qnaMarker);
              ViaConstant.qnaMap.put(lineList[0], lineList[1]);
            }
          }
        }
        scan.close();
        System.out.println( "initZipQuoteMap done");

        uploadPath = baseDataDirectory;
        uploadPath += File.separator + "public/qna_google.tsv";
        //uploadPath += File.separator + "public/insurance_calculator.tsv";
        downloadFile = new File(uploadPath );
        scan= new Scanner(downloadFile, "UTF-8"); 
        count = 0;
        countMax = 50000;
        line=""; // no need to skip for this file scan.nextLine(); //skip header row
        line = scan.nextLine(); //skip header row

        for( ; count<countMax && scan.hasNextLine(); count++)
        {
          line = scan.nextLine(); //skip header row
          //String[] lineToken = line.split("\t");
          if( line != null && line.length() > 0 )
          {
            String query = line.toLowerCase();
            String [] lineList = query.split("\t");
            if( lineList.length >= 4)
            {
              ViaConstant.qnaList.add(lineList[2] + ViaConstant.qnaMarker);
              ViaConstant.qnaMap.put(lineList[2], lineList[3]);
            }
          }
        }
        scan.close();
        System.out.println( "initZipQuoteMap with qna_google done");

        
        
      }
    }
    catch( Exception x) {
      x.printStackTrace();
    }
  }
  

  
  public void initBirthdayList( String baseDataDirectory)
  {
    try
    {
      if( ViaConstant.birthdayList.size() <= 0 )
      {
        String uploadPath = baseDataDirectory;
        uploadPath += File.separator + "public/birthday.txt";
        File downloadFile = new File(uploadPath );
        Scanner scan= new Scanner(downloadFile, "UTF-8"); 
        int count = 0;
        int countMax = 50000;
        String line=""; // no need to skip for this file scan.nextLine(); //skip header row
        //line = scan.nextLine(); //skip header row

        for( ; count<countMax && scan.hasNextLine(); count++)
        {
          line = scan.nextLine(); //skip header row
          //String[] lineToken = line.split("\t");
          if( line != null && line.length() > 0 )
          {
            ViaConstant.birthdayList.add( line + " birthday" + ViaConstant.birthdayMarker);
            
          }
        }
        scan.close();
        System.out.println( "birthday list done");

      }
    }
    catch( Exception x) {
      x.printStackTrace();
    }
  }
  
  public void initOtherContextList( String baseDataDirectory)
  {
    try
    {
      if( ViaConstant.otherContextList.size() <= 0 )
      {
        String uploadPath = baseDataDirectory;
        uploadPath += File.separator + "public/other_context.txt";
        File downloadFile = new File(uploadPath );
        Scanner scan= new Scanner(downloadFile, "UTF-8"); 
        int count = 0;
        int countMax = 50000;
        String line=""; // no need to skip for this file scan.nextLine(); //skip header row
        //line = scan.nextLine(); //skip header row

        for( ; count<countMax && scan.hasNextLine(); count++)
        {
          line = scan.nextLine().toLowerCase(); //skip header row
          //String[] lineToken = line.split("\t");
          if( line != null && line.length() > 0 )
          {
            ViaConstant.otherContextList.add( line + ViaConstant.otherMarker );
            
          }
        }
        scan.close();
        System.out.println( "other context list done");

      }
    }
    catch( Exception x) {
      x.printStackTrace();
    }
  }

  
  public void initEffectivedayList( String baseDataDirectory)
  {
    try
    {
      if( ViaConstant.effectivedayList.size() <= 0 )
      {
        String uploadPath = baseDataDirectory;
        uploadPath += File.separator + "public/effectivedate.txt";
        File downloadFile = new File(uploadPath );
        Scanner scan= new Scanner(downloadFile, "UTF-8"); 
        int count = 0;
        int countMax = 50000;
        String line=""; // no need to skip for this file scan.nextLine(); //skip header row
        //line = scan.nextLine(); //skip header row

        for( ; count<countMax && scan.hasNextLine(); count++)
        {
          line = scan.nextLine(); //skip header row
          //String[] lineToken = line.split("\t");
          if( line != null && line.length() > 0 )
          {
            ViaConstant.effectivedayList.add( line + " effective day" + ViaConstant.effectivedayMarker);
            
          }
        }
        scan.close();
        System.out.println( "effective day list done");

      }
    }
    catch( Exception x) {
      x.printStackTrace();
    }
  }
  
  public void initUniqueStreetNameList( String baseDataDirectory)
  {
    try
    {
      if( ViaConstant.uniqueStreetNameList.size() <= 0 )
      {
        String uploadPath = baseDataDirectory;
        uploadPath += File.separator + "public/streets.tsv";
        File downloadFile = new File(uploadPath );
        Scanner scan= new Scanner(downloadFile, "UTF-8"); 
        int count = 0;
        int countMax = 50000;
        String line=""; // no need to skip for this file scan.nextLine(); //skip header row
        line = scan.nextLine(); //skip header row

        for( ; count<countMax && scan.hasNextLine(); count++)
        {
          line = scan.nextLine(); //skip header row
          //String[] lineToken = line.split("\t");
          if( line != null && line.length() > 0 )
          {
            ArrayList<String> streetsList = new ArrayList<String>();
            String[]streetsParts = line.toLowerCase().split("\t");
            String cityState = streetsParts[0]+","+streetsParts[1];
            String[]streetNames = streetsParts[2].split(":");
            for(int i=0; i<streetNames.length; i++)
            {
              streetsList.add(streetNames[i].trim() + ViaConstant.addressMarker);
            }
            // create a look up for city,state
            ViaConstant.streetsMap.put( cityState, streetsList );
            //create a dummy entry with just state for small cities
            ViaConstant.streetsMap.put( streetsParts[1], streetsList );

          }
        }
        scan.close();
        System.out.println( "streetsMap  done");

      }
    }
    catch( Exception x) {
      x.printStackTrace();
    }
  }


}
