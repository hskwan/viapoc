package com.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.ArrayList;

public class Command
{

  public static void main(String[] args)
  {
    // TODO Auto-generated method stub

  }
  // execute java exec command without a log file
  public ArrayList<String> execCommand(String command)
  {
      return execCommandLog(command, null);
  }
public ArrayList<String> execCommandLog(String command, OutputStreamWriter oswLMLog)
{
  ArrayList<String> response= new ArrayList<String>();
     
    //StringBuffer output = new StringBuffer();
  Process p=null;
  try 
  {
    System.out.println(" execCommandLog: command=" +  command );
    p = Runtime.getRuntime().exec(command);
        
    BufferedReader reader =
      new BufferedReader(new InputStreamReader(p.getInputStream()));
    BufferedReader err_reader =
              new BufferedReader(new InputStreamReader(p.getErrorStream()));
  
      String line = "";
      int outputCount =0;
      if( oswLMLog == null) // if no log file, save output into string buffer, note that it may have memory limits
      {
          while( line != null )
          {
            line = reader.readLine();
            if( line != null)
            {
              System.out.println(" execCommandLog: line=" +  line );
              if( line.indexOf( "NOTE: Picked up JDK" ) > 0)
                line = line.substring(0,  line.indexOf( "NOTE: Picked up JDK"));
              if( outputCount <1000)  // only append 1000 lines, to save memory
                response.add(line);
                //output.append(line + "\n");
              
              ++outputCount;
            }
          }
          //get error if any
          line = "";
          while( line != null )
          {
            line = err_reader.readLine();
            if( line != null)
            {
              System.out.println(" error: line=" +  line );
              //if( outputCount <1000)
              //  output.append(line + "\n");
              ++outputCount;
            }
          }

          //response = output.toString();
          return response;      
      }
      long startTime = System.currentTimeMillis();
      long stopTime = startTime + 3*60 * 1000; //3 second timeout
      String repeatedLine="";
      while( p.isAlive() && System.currentTimeMillis() < stopTime)
      {
          if( reader.ready())
          {
            String timeline = System.currentTimeMillis() + ": ";
            line = reader.readLine();
            if( line != null)
            {
              
              //output.append(line + "\n");
              response.add(line);
              if(oswLMLog!=null)
              {
            oswLMLog.write( timeline + line + "\n");
            oswLMLog.flush();
              }

            }
            
          }
          if( err_reader.ready())
          {
            String timeline = System.currentTimeMillis() + ": ";
            line = err_reader.readLine();
            if( line != null)
        {
          //output.append("ES: " + line + "\n");
          response.add("ES: " + line);
          if(oswLMLog!=null)
          {
            if( repeatedLine.contains(line) )
              oswLMLog.write("E");
            else
            {
              oswLMLog.write( "ES: " + timeline + line + "\n");
              repeatedLine += line;
            }
            oswLMLog.flush();
          }
        }
          }
          Thread.sleep(200);
      }
      if( p.isAlive())
        response.add("Warning timeout");
          //output.append("Warning timeout.\n");
      long elapseTime = System.currentTimeMillis() - startTime;
          response.add("Elapse time = " + elapseTime + "ms");
      //output.append("Elapse time = " + elapseTime + "ms\n");
  
  } catch (Exception e) 
  {
    e.printStackTrace();
    //output.append("Runtime.getRuntime().exec(command); exception:"+ e.getMessage());
  }
  if(p!=null)
    p.destroy();
  //response = output.toString();

     
    return response;

} //end of execCommand

public String execCommandLoggerShell( String command, PrintStream dplog)
{
  String response="";

  try
  {

    File tempFile = File.createTempFile("dp_", ".sh");
    if( tempFile == null )
      return "tempFile == null";
     java.io.OutputStreamWriter tmpfileout = 
                new OutputStreamWriter(new FileOutputStream(tempFile));
    if( tmpfileout == null )
      return "tmpfileout == null";

     tmpfileout.write(command+"\n");
     tmpfileout.flush();
     tmpfileout.close();
     tempFile.setExecutable(true);
     //logger.debug( "tmpfileout=" + tempFile.getAbsolutePath() );
     System.out.println( "tmpfileout=" + tempFile.getAbsolutePath());
     response = execCommandLogger(  tempFile.getAbsolutePath(), dplog );
     if( response == null )
       return "respone == null ";
     System.out.println("execCommandLoggerShell response=" + response);
     if( dplog != null )
       dplog.println( response );
     //tempFile.delete();
  }
  catch( Exception x)
  {
    x.printStackTrace();
     response = "execCommandLoggerShell exception msg=" + x.getMessage();
  }
  
  if( dplog != null )
    return "execCommandLogger done";
  else
    return response;
  
}
public String execCommandLogger(String command, PrintStream dplog)
{
  String response="";
     
  Process p=null;
  try 
  {
    if( dplog != null)
      dplog.println(" execCommandLog: command=" +  command );
    else
      System.out.println(" execCommandLog: command=" +  command );
    p = Runtime.getRuntime().exec(command);
    //Thread.sleep(10000);    
    BufferedReader reader =
      new BufferedReader(new InputStreamReader(p.getInputStream()));
    BufferedReader err_reader =
              new BufferedReader(new InputStreamReader(p.getErrorStream()));
  
      String line = "";
      long startTime = System.currentTimeMillis();
      long stopTime = startTime + 60*60 * 1000; //60 second timeout
      String repeatedLine="";
      while( p.isAlive() && System.currentTimeMillis() < stopTime)
      {
        //curQueue = (Queue<String>) request.getSession().getAttribute( MyUtils.dpReport);
        //dplog.println( "p.isAlive()" + "\n");
          if( reader.ready())
          {
            String timeline = System.currentTimeMillis() + ": ";
            line = reader.readLine();
            if( line != null)
            {
              if( dplog != null )
                dplog.println( line );
              else
              {
                System.out.println("execCommandLogger:line=" +line);
                response += line;
              }

            }
            
          }
          if( err_reader.ready())
          {
            String timeline = System.currentTimeMillis() + ": ";
            line = err_reader.readLine();
            if( line != null)
            {
              if( dplog != null )
                dplog.print("ES: " + line + "\n");
              else
              {
                response += "ES: " + line + "\n";
                System.out.println("execCommandLogger:err line=" +line);
              }
            }
        }
        
          Thread.sleep(2000);
      } //end of while p.isAlive() 
      if( p.isAlive())
      {
          dplog.print("Warning timeout.\n");
      }
      long elapseTime = System.currentTimeMillis() - startTime;
      if( dplog != null)
        dplog.print("Elapse time = " + elapseTime + "ms\n");
      else
        System.out.println("Elapse time = " + elapseTime + "ms\n");
  
  } catch (Exception e) 
  {
    e.printStackTrace();
    //e.printStackTrace( dplog);
  }
  if(p!=null)
    p.destroy();

     
    return response;

} //end of execCommand



   public String execCommand(String command, OutputStreamWriter oswOutLog, boolean debug)
   {
      String response="";
        
        {
            StringBuffer output = new StringBuffer();
          Process p=null;
      try {
        p = Runtime.getRuntime().exec(command);
        //p.waitFor();
        BufferedReader reader =
                              new BufferedReader(new InputStreamReader(p.getInputStream()));
  
                          String line = "";
        while ((line = reader.readLine())!= null) {
          output.append(line + "\n");
        }
  
      } catch (Exception e) {
        e.printStackTrace();
        output.append("Runtime.getRuntime().exec(command); exception:"+ e.getMessage());
      }
      if(p!=null)
        p.destroy();
      response = output.toString();
        }
     
        return response;

    } //end of execCommand

 
   
}
