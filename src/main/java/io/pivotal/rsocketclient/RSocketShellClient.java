package io.pivotal.rsocketclient;


import io.pivotal.rsocketclient.data.QuoteRequest;
import io.pivotal.rsocketclient.data.QuoteMessage;
import io.rsocket.SocketAcceptor;
import io.rsocket.metadata.WellKnownMimeType;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.messaging.rsocket.RSocketStrategies;
import org.springframework.messaging.rsocket.annotation.support.RSocketMessageHandler;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PreDestroy;
import java.time.Duration;
import java.util.UUID;

@Slf4j
@ShellComponent
public class RSocketShellClient {

    private static final String CLIENT = "Client";
    private static final String REQUEST = "Request";
    private static final String FIRE_AND_FORGET = "Fire-And-Forget";
    private static final String STREAM = "Stream";
    private static final String CLIENT_ID = UUID.randomUUID().toString();
    private static final MimeType SIMPLE_AUTH = MimeTypeUtils.parseMimeType(WellKnownMimeType.MESSAGE_RSOCKET_AUTHENTICATION.getString());
    private static Disposable disposable;

    private RSocketRequester rsocketRequester;
    private RSocketRequester.Builder rsocketRequesterBuilder;
    private RSocketStrategies rsocketStrategies;

    @Autowired
    public RSocketShellClient(RSocketRequester.Builder builder,
                              @Qualifier("rSocketStrategies") RSocketStrategies strategies) {
        this.rsocketRequesterBuilder = builder;
        this.rsocketStrategies = strategies;
    }

    @ShellMethod("Login with your username and password.")
    public void startService() {
        //log.info("Connecting using client ID: {} and username: {}", CLIENT_ID, username);
        SocketAcceptor responder = RSocketMessageHandler.responder(rsocketStrategies, new ClientHandler());
        //UsernamePasswordMetadata user = new UsernamePasswordMetadata(username, password);
        this.rsocketRequester = rsocketRequesterBuilder
                .setupRoute("shell-client")
                .setupData(CLIENT_ID)
                //.setupMetadata(user, SIMPLE_AUTH)
                //.rsocketStrategies(builder ->
                //        builder.encoder(new ()))
                .rsocketConnector(connector -> connector.acceptor(responder))
                .connectTcp("localhost", 7000)
                .block();

        this.rsocketRequester.rsocket()
                .onClose()
                .doOnError(error -> System.out.println("WARNING:Connection CLOSED"))
                .doFinally(consumer -> System.out.println("Client DISCONNECTED"))
                .subscribe();
    }

    @PreDestroy
    @ShellMethod("Logout and close your connection")
    public void logout() {
        if (isClientRunning()) {
            this.s();
            this.rsocketRequester.rsocket().dispose();
            System.out.println("Logged out.");
        }
    }

    private boolean isClientRunning() {
        if (null == this.rsocketRequester || this.rsocketRequester.rsocket().isDisposed()) {
            startService();
            return true;
        }
        return true;
    }

    @ShellMethod("Create a quote")
    public void getQuote() throws InterruptedException {
        if (isClientRunning()) {

            JSONObject inquiryRequest = new JSONObject();
            inquiryRequest.put("partner.key", "a8c03fb0-d54d-4f13-81f3-cc7879990803");
            inquiryRequest.put("partner.channel", "Mobile");
            inquiryRequest.put("address.1a345678.city", "Chicago");
            inquiryRequest.put("address.1a345678.line1", "3655 North Artesian Ave");
            inquiryRequest.put("address.1a345678.state", "IL");
            inquiryRequest.put("address.1a345678.zipcode", "60618");
            inquiryRequest.put("person.1ab45678.dob", "1991-02-02");
            inquiryRequest.put("person.1ab45678.email", "xyz.abcde@test.com");
            inquiryRequest.put("person.1ab45678.firstName", "xyz");
            inquiryRequest.put("person.1ab45678.lastName", "lastnameabcd");
            inquiryRequest.put("person.1ab45678.phone", "524-341-8225");
            inquiryRequest.put("policy.home.effectiveDate", "2020-07-06");
            inquiryRequest.put("primaryInsured.address.lock", "true");
            inquiryRequest.put("primaryInsured.address.ref", "address.1a345678");
            inquiryRequest.put("primaryInsured.person.lock", "true");
            inquiryRequest.put("primaryInsured.person.ref", "person.1ab45678");
            inquiryRequest.put("product.lob", "home");
            inquiryRequest.put("primaryInsured.person.creditDisclosureFlag", "true");

            System.out.println("\nSending one request. Waiting for one response...");
            QuoteMessage message = this.rsocketRequester
                    .route("get-quote")
                    .data(inquiryRequest)
                    .retrieveMono(QuoteMessage.class)
                    .block();
            System.out.println("\nResponse was: {}"+ message);
        }
    }

    @ShellMethod("Send one request. One response will be printed?")
    public void getDummyQuote() throws InterruptedException {

        if (isClientRunning()) {
            System.out.println("\nSending one request. Waiting for one response...");
            QuoteMessage message = this.rsocketRequester
                    .route("get-dummy-quote")
                    .retrieveMono(QuoteMessage.class)
                    .block();
            System.out.println("\nResponse was: {}"+ message);
        }
    }


    //COMING SOON
    /*@ShellMethod("Send one request. No response will be returned.")
    public void fireAndForget() throws InterruptedException {
        if (isClientRunning()) {
            System.out.println("\nFire-And-Forget. Sending one request. Expect no response (check server console log)...");
            this.rsocketRequester
                    .route("get-dummy-request")
                    //.data(new QuoteRequest(CLIENT, FIRE_AND_FORGET))
                    .send()
                    .block();
        }
    }

    @ShellMethod("Send one request. Many responses (stream) will be printed.")
    public void stream() {
        if (isClientRunning()) {
            System.out.println("\n\n**** Request-Stream\n**** Send one request.\n**** Log responses.\n**** Type 's' to stop.");
            disposable = this.rsocketRequester
                    .route("stream")
                    .data(new QuoteRequest(CLIENT, STREAM))
                    .retrieveFlux(QuoteRequest.class)
                    .subscribe(quoteRequest -> System.out.println("Response: {} \n(Type 's' to stop.)", quoteRequest));
        }
    }

    @ShellMethod("Stream some settings to the server. Stream of responses will be printed.")
    public void channel() {
        if (isClientRunning()) {
            System.out.println("\n\n***** Channel (bi-directional streams)\n***** Asking for a stream of messages.\n***** Type 's' to stop.\n\n");

            Mono<Duration> setting1 = Mono.just(Duration.ofSeconds(1));
            Mono<Duration> setting2 = Mono.just(Duration.ofSeconds(3)).delayElement(Duration.ofSeconds(5));
            Mono<Duration> setting3 = Mono.just(Duration.ofSeconds(5)).delayElement(Duration.ofSeconds(15));

            Flux<Duration> settings = Flux.concat(setting1, setting2, setting3)
                    .doOnNext(d -> System.out.println("\nSending setting for a {}-second interval.\n", d.getSeconds()));

            disposable = this.rsocketRequester
                    .route("channel")
                    .data(settings)
                    .retrieveFlux(QuoteRequest.class)
                    .subscribe(quoteRequest -> System.out.println("Received: {} \n(Type 's' to stop.)", quoteRequest));
        }
    }*/

    @ShellMethod("Stops Streams or Channels.")
    public void s() {
        if (isClientRunning() && null != disposable) {
            System.out.println("Stopping the current stream.");
            disposable.dispose();
            System.out.println("Stream stopped.");
        }
    }
}

@Slf4j
class ClientHandler {

    @MessageMapping("client-status")
    public Flux<String> statusUpdate(String status) {
        System.out.println("Connection {}" + status);
        return Flux.interval(Duration.ofSeconds(5)).map(index -> String.valueOf(Runtime.getRuntime().freeMemory()));
    }
}
