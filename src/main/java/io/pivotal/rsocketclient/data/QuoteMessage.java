package io.pivotal.rsocketclient.data;

import lombok.*;

import java.util.Date;

@Getter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Setter
public class QuoteMessage {
    public String first_name;
    public String last_name;
    public String middle_initial;
    public String birthday;
    public String email;
    public String address_1;
    public String address_2;
    public String city;
    public String state;
    public String zipCode;
    public String home_rent;
    public Integer units;
    public Integer people;
    public String company;
    public String deductible;
    public Double coverage_a;
    public Double coverage_b;
    public Double coverage_c;
    public Double coverage_d;
    public Double coverage_e;
    public Double coverage_f;
    public String effective_date;
    public Double premium;
}
