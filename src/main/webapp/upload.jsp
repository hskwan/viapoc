<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Upload</title>
 </head>
 <body>
 
 <%@ page import="com.utils.ViaConstant" %>
 
 <%@ page session="true" %>
 
    <h3>Upload File</h3>
 
  <p>Select file to be upload to your folder on the server:<br>

 	<form method="POST" action="uploadFile" enctype="multipart/form-data">
 	
 	      Select file to upload:
        <input type="file" name="uploadFile" />
        <br/>
        <input type="submit" value="Upload" />
  </form>
  <%
  String statusString =(String) session.getAttribute(ViaConstant.statusString);
  if(statusString!=null )
      out.print("Status:<br>" + statusString );

  %>
   
</body>
</html>