<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
* {
  box-sizing: border-box;
}

body {
  font: 14px;  
}

/*the container must be positioned relative:*/
.autocomplete {
  position: relative;
  display: inline-block;
}

input {
  border: 1px solid transparent;
  background-color: #f1f1f1;
  padding: 6px;
  font-size: 14px;
}

input[type=text] {
  background-color: #f1f1f1;
  width: 100%;
}

input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
  cursor: pointer;
}

.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}

.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}

/*when hovering an item:*/
.autocomplete-items div:hover {
  background-color: #e9e9e9; 
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}
</style>
</head>     
<body>

<!--Make sure the form has the autocomplete function switched off:-->
<form autocomplete="off"  id="myForm" method="POST" action="DoUserSearch2">

  Question: 
  <div class="autocomplete" style="width:300px;">
    <input  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"id="myInput" type="text" name="userCity" value="${sessionScope.userCity}" placeholder="${sessionScope.userCity}">
  </div>
</form>

<br>

<script>
function autocomplete(inp, arr) 
{
	/*the autocomplete function takes two arguments,
	  the text field element and an array of possible autocompleted values:*/
	var currentFocus;
	/*execute a function when someone writes in the text field:*/
	inp.addEventListener
	("input", function(e) 
	{
		var a, b, i, val = this.value;
	    /*close any already open lists of autocompleted values*/
	    closeAllLists();
	    if (!val) { return false;} 
	    currentFocus = -1;
	    /*create a DIV element that will contain the items (values):*/
	    a = document.createElement("DIV");
	    a.setAttribute("id", this.id + "autocomplete-list");
	    a.setAttribute("class", "autocomplete-items");
	    /*append the DIV element as a child of the autocomplete container:*/
	    this.parentNode.appendChild(a);
	    /*for each item in the array...*/
	    var suggestCount=0;
	    /* alert(  val[i].indexOf( " ") + "," + arr[i].indexOf( val )); */
	    /* alert( arr.indexOf( val )) ; */
	    var space = " ";
	    var xhttp = new XMLHttpRequest();
	    xhttp.withCredentials = true;
	    var val_url = val.replace(' ', '+')
	    //var cururl = "http://ec2-3-83-216-154.compute-1.amazonaws.com:8080/al_simulator/endpointv5?accity=" + val_url;
	    var cururl = "${sessionScope.serverUrl}:8443/via/autocomplete?accity=" + val_url;
	    var localurl = "${sessionScope.serverLocalUrl}:8080/via/autocomplete?accity=" + val_url;
	    //test for 8443 or local
	    xhttp.open("GET", cururl, false);
	    if( xhttp.readyState != 4 )
	    {
	    	  cururl = localurl;
	    }
	      
	    if ("withCredentials" in xhttp) 
	    {
	        // XHR for Chrome/Firefox/Opera/Safari.
	        xhttp.open("GET", cururl, false);
	    } else if (typeof XDomainRequest != "undefined") 
	    {
	        // XDomainRequest for IE.
	        xhttp = new XDomainRequest();
	        xhttp.open("GET", cururl);
	    } else {
	        // CORS not supported.
	        alert( "CORS not supported.");
	    }
	    /* xhttp.open("GET", cururl, true); */
	    /* xhttp.withCredentials = true; */
	      
	    xhttp.setRequestHeader("Content-type", "text/html");
	    xhttp.send();
	    for (i = 0; i < 200; i++) 
	    { 
			wait( 50);
		    if(xhttp.readyState == 4)
		   	{
		    	var s= xhttp.responseText;
			    arr = s.split("\t") ;  
		   	  	break;
		   	}
	    }  
	      
	    //alert( "arr.length=" + arr.length);
	    altVal = "";
	    if( val.length == 3 && val.substr(2,3) == " " )
	    	  altVal = " " + val.substr(0, 2);
	    for (i = 0; i < arr.length; i++) 
	    {

	   		{
	        	suggestCount= suggestCount + 1;
	        	if(suggestCount > 100)
	        		break;
		        /*create a DIV element for each matching element:*/
		        b = document.createElement("DIV");
		        /*make the matching letters bold:*/
		        b.innerHTML = ""; 
		        b.innerHTML += arr[i];
       	        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>"; 
	        	b.addEventListener
	          	("click", function(e) 
	          	{
	              /*insert the value for the autocomplete text field:*/
	              inp.value = this.getElementsByTagName("input")[0].value;
	              /*close the list of autocompleted values,
	              (or any other open lists of autocompleted values:*/
	              closeAllLists();
	              var myform = document.getElementById('myForm');
	              myform.submit();

	         	}
	          	); 
	          /* end of b.addEventListener */
	          a.appendChild(b);
	        }
	        /* end of if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) */
	      }/* end of for (i = 0; i < arr.length; i++)  */
	}/* end of function of  inp.addEventListener("input", function(e) */
	);/* end of function inp.addEventListener */
	/*execute a function presses a key on the keyboard:*/
	inp.addEventListener
	("keydown", function(e)
		{
	      var x = document.getElementById(this.id + "autocomplete-list");
	      if (x) x = x.getElementsByTagName("div");
	      if (e.keyCode == 40) {
	        /*If the arrow DOWN key is pressed,
	        increase the currentFocus variable:*/
	        currentFocus++;
	        /*and and make the current item more visible:*/
	        addActive(x);
	      } else if (e.keyCode == 38) { //up
	        /*If the arrow UP key is pressed,
	        decrease the currentFocus variable:*/
	        currentFocus--;
	        /* and make the current item more visible:*/
	        addActive(x);
	      } else if (e.keyCode == 13) 
	      {
	        /*If the ENTER key is pressed, prevent the form from being submitted,*/
	        e.preventDefault();
	        if (currentFocus > -1) {
	          /*and simulate a click on the "active" item:*/
	          if (x) x[currentFocus].click();
	        }
	      }
	 	 }
	  		/* end if function(e) */
	  );
    /* end if function inp.addEventListener */
	  function addActive(x) 
	  {
	    /*a function to classify an item as "active":*/
	    if (!x) return false;
	    /*start by removing the "active" class on all items:*/
	    removeActive(x);
	    if (currentFocus >= x.length) currentFocus = 0;
	    if (currentFocus < 0) currentFocus = (x.length - 1);
	    /*add class "autocomplete-active":*/
	    x[currentFocus].classList.add("autocomplete-active");
	  }
	  function removeActive(x) 
	  {
	    /*a function to remove the "active" class from all autocomplete items:*/
	    for (var i = 0; i < x.length; i++) {
	      x[i].classList.remove("autocomplete-active");
	    }
	  }
	  function closeAllLists(elmnt) 
	  {
	    /*close all autocomplete lists in the document,
	    except the one passed as an argument:*/
	    var x = document.getElementsByClassName("autocomplete-items");
	    for (var i = 0; i < x.length; i++) 
	    {
	      if (elmnt != x[i] && elmnt != inp) {
	        x[i].parentNode.removeChild(x[i]);
	      }
	    }
	  }
	  /*execute a function when someone clicks in the document:*/
	  document.addEventListener("click", function (e)
		{
	      closeAllLists(e.target);
	  	}
	  );
}
/* end of function autocomplete(inp, arr)  */

/*An array containing all the city names in this country:*/
var countries = ""; 
function wait(ms)
{
	var start = new Date().getTime();
	var end = start;
	while(end < start + ms) 
	{
	     end = new Date().getTime();
	}
}

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("myInput"), countries);
</script>

</body>
</html>
