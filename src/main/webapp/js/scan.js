AWS.config.region = 'us-east-1';
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-east-1:658b22b0-24bd-41bc-8d24-f7d1fc9fb815'
});
var bucket = "discover-lab-dec-page";

var lambda = new AWS.Lambda();

function returnResult( ) {
    document.getElementById('scanbutton').disabled = true;
    // var bucket = document.getElementById('bucket');
    // var file = document.getElementById('file');
    var doc = document.getElementById("pdffile");
    var files = doc.files;
    var file = files[0];
    var fileName = file.name;
    var input;
    if (bucket == null || bucket == '' || fileName == null || fileName == ''){
        input = {};
    }else {

        input = {
            bucket: 'discover-lab-dec-page',
            file: fileName
        };
    }
    lambda.invoke({
        FunctionName: 'pdftextract-PDFFunction-1GK9F8JST3PZ3',
        Payload: JSON.stringify(input)
    }, function (err, data) {
        var result = document.getElementById('result');
        if (err){
            console.log(err, err.stack);
            result.innerHTML = '<div class="alert alert-danger">' + err + '</div>';
        }else{
            //var output = JSON.parse(data.Payload);
            var output = JSON.stringify(data, undefined, 4);
            var textarea = document.getElementById('actualJson');
            result.innerHTML = '<div class="alert alert-success">' + output + '</div>';
        }
        document.getElementById("scanbutton").disabled = false;
    });
}

function addToS3() {
    var doc = document.getElementById("pdffile");
    var files = doc.files;
    if (!files.length){
        return alert("File is not supported");
    }
    var file = files[0];
    var fileName = file.name;
    var filePath = 'discover-lab-dec-page/' + fileName;

    AWS.config.update({
        region: "us-east-1",
        credentials: new AWS.CognitoIdentityCredentials({
            IdentityPoolId: 'us-east-1:658b22b0-24bd-41bc-8d24-f7d1fc9fb815'
        })
    });

    var s3 = new AWS.S3({
        apiVersion: '2006-03-01',
        params: {Bucket: 'discover-lab-dec-page'}
    });
    s3.upload({
        Key: filePath,
        Body: file,
        ACL: 'public-read'
    }, function(err, data) {
        if(err) {
            reject('error');
        }
        else{
           //var promis = returnResult('discover-lab-dec-page', fileName);
            console.log("success");
            returnResult();

        }
    }).on('httpUploadProgress', function (progress) {
        var uploaded = parseInt((progress.loaded * 100) / progress.total);
        $("progress").attr('value', uploaded);
    });

}

