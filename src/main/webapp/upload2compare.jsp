<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Compare price</title>
	<link rel="stylesheet" href="css/comparison.css">
	<title>Adjust Coverage</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/styles.min.css">
	<link rel="stylesheet" href="css/index2.css">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<link rel="stylesheet" href= "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/velocity.min.js"></script>

	<style>
		.card {
			background: #ffffff;
			margin: 50px 160px;
		}

		.col-first {
			border: 1px solid #ccc;
			border-right: none;
		}

		.col-first-tit {
			margin: 0 -15px;
			padding: 15px;
			height: 160px;
		}

		.col-first-tit .col-first-tit-p1 {
			font-size: 24px;
			font-weight: bold;
		}

		.col-first-tit .col-first-tit-input {
			padding: 8px 3px;
			font-size: 16px;
			border: 0px solid #FFFFFF;
			width: 100%;
		}
		
		.col-first-tit .col-first-tit-input:focus {
		  /* removing the input focus colored box. */
		  outline: none;
		}
		
		.col-first-tit .col-first-tit-form {
		  /* This bit sets up the horizontal layout */
		  display:flex;
		  flex-direction:row;
		  
		  /* This bit draws the box around it */
		  border:1px solid lightgrey;
	
		  padding:2px;
		  margin-top:10%;
		}

		.col-first-tit .col-first-tit-form .col-first-tit-form-btn {
		  /* Just a little styling to make it pretty */
		  border:0px solid white;
		  background:white;
		  color:white;
		}

		.col-first-tit .col-first-tit-p2 {
			font-size: 20px;
			font-weight: bold;
			color: #00008f
		}

		.col-first .col-first-item {
			position: relative;
			margin: 0 -15px;
			padding: 0 5px;
			border-top: 1px solid #ccc;
		}

		.col-first .col-first-item .tit {
			line-height: 40px;
			height: 40px;
		}

		.col-first .col-first-item .con {
			padding: 20px 40px;
			display: none;
			min-height: 200px;
		}

		.col-first .col-first-item .tit .icon-up {
			background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAH2klEQVR4Xu2dy25bZRSFdygURAbldZggBIU2oQJegVFnSHWHCCkSA2aNxAvwCgwKFHERFYjXQZ0U0kuMHJQoFzs+a51Ljs/6Omz3PvH6lr/89rHSbBV/IACBlQS2YAMBCKwmgCA8OyBwCQEE4ekBAQThOQABjwAniMeNrRACCBJSNDE9AgjicWMrhACChBRNTI8Agnjc2AohgCAhRRPTI4AgHje2QgggSEjRxPQIIIjHja0QAggSUjQxPQII4nFjK4QAgoQUTUyPAIJ43NgKIYAgIUUT0yOAIB43tkIIIEhI0cT0CCCIx42tEAIIElI0MT0CCOJxYyuEAIKEFE1MjwCCeNzYCiGAICFFE9MjgCAeN7ZCCCBISNHE9AggiMeNrRACCBJSNDE9AgjicWMrhACChBRNTI8Agnjc2AohgCAhRRPTI4AgHje2QgggSEjRxPQIIIjHja0QAggSUjQxPQII4nFjK4QAgoQUTUyPAIJ43NgKIYAgIUUT0yOAIB43tkIIIEhI0cT0CCCIx42tEAIIElI0MT0CCOJxYyuEAIKEFE1MjwCCeNzYCiGAICFFE9MjgCAeN7ZCCCBISNHE9AggiMeNrRACCBJSNDE9AgjicWMrhACChBRNTI8Agnjc2AohgCAhRRPTI4AgHje2QgggSEjRxPQIIIjHja0QAggSUjQxPQII4nFjK4QAgoQUTUyPAIJ43NgKIYAgIUUT0yOAIB43tkIIIEhI0cT0CCCIx42tEAIIElI0MT0CCOJxYyuEAIKEFE1MjwCCrOT24Kuq7a+r7j710E5969s3qv7+omr25ZSTIsiFdvderbrxXVV9VFV/Vs0/rJr9M+UngZ7tm9erXv5QVe9V1c9VT3ar9l7o1xn/BoKc6ehIjkXxH5z6ayQ5w+iMHMf/spDkTtXes/E/5bVHiCAnvJbKcfyvSHJEYqkcx4x+r3pya2qSIMhRvXvXq248PHdynP9WEy7JpXJMVhIE+V+On6rq3QaHb6gkjeSYpCThgkhyhL7ckuQ4JclbO1Wf/dvgm86oR8IF2X+7qv7SG5o/rtrenf4t4KNbuQ+rtt43GL1TNftD3xvXRrggizL2b1bV4s7Va2I1E3+5ZZ0cVTV/UTX/pOr+gunG/0GQowrbSHLtZtXnBxv/TFh/K7dBxGnJsQiMICe125L8VnVtdzqScHKc/k6AIGe+L6ZLghznj0kEufDCIVUS5Fj2GhJBlr6yTpMEOVa9wUKQlW89UyRBjsvuPiDIpfdmpi4Jcqy7NYcg6wj5t4BHfncLOdZWz23eJohafU4yUkmQo2nznCBNSU3mJHHlqOdVh59O5RPyprUjSFNS7T5xH8lJ0kaO2q2694uEawLDCCKXuKlv3JFDrpr3IA6yTXxPghxu05wgLrmNeU+CHHbFnCBt0G3CSYIcbRvmBGlLcLQnCXK0rpYTpAuEYzxJkKOrZjlBuiI5mpMEOTqrlBOkS5RtT5I3b1fdfd7uESFHO34XtzlBuiZqnyTzR1XbH/uSIEfnVXKC9IG0zUniSoIcfTXJCdIX2cFOEuTorUJOkD7RDnGSIEffDXKC9E24t5MEOXqvjhNkCMR9nCTIMVRznCBDke7sJEGOwSrjBBkSdRcnycErp36zk/LgF5+vRP48hwJp2SwnSFuC8v6D21VbP8prVd9X1fU1v8NkxWXnO1WzR8bXjF9BkCt5Ctg/dKU+Wk4Oldi5eQRpCdBf710S5PDLOdlEkA4g+pfoTZLF/zZ/J/FnyP0ulm8iSNdE5et1LslB1fxW1eyx/FBYuEAAQUbxpOhMEuTouE8E6Riof7nWkiCHD3/lJoL0ANW/pC0JcvjQL91EkJ7A+peVJUEOH/baTQRZi+gqBhpLghw914MgPQP2L79WEuTw4TbeRJDGqK5icKUkyDFQHQgyEGj/y1yQBDl8mPImgsjIrmLhRJJDPgQclj+CDMu7xVfb36k6fFZ1/9cWF2FVJIAgIjDGswggSFbfpBUJIIgIjPEsAgiS1TdpRQIIIgJjPIsAgmT1TVqRAIKIwBjPIoAgWX2TViSAICIwxrMIIEhW36QVCSCICIzxLAIIktU3aUUCCCICYzyLAIJk9U1akQCCiMAYzyKAIFl9k1YkgCAiMMazCCBIVt+kFQkgiAiM8SwCCJLVN2lFAggiAmM8iwCCZPVNWpEAgojAGM8igCBZfZNWJIAgIjDGswggSFbfpBUJIIgIjPEsAgiS1TdpRQIIIgJjPIsAgmT1TVqRAIKIwBjPIoAgWX2TViSAICIwxrMIIEhW36QVCSCICIzxLAIIktU3aUUCCCICYzyLAIJk9U1akQCCiMAYzyKAIFl9k1YkgCAiMMazCCBIVt+kFQkgiAiM8SwCCJLVN2lFAggiAmM8iwCCZPVNWpEAgojAGM8igCBZfZNWJIAgIjDGswggSFbfpBUJIIgIjPEsAgiS1TdpRQIIIgJjPIsAgmT1TVqRAIKIwBjPIoAgWX2TViSAICIwxrMIIEhW36QVCSCICIzxLAIIktU3aUUCCCICYzyLAIJk9U1akQCCiMAYzyKAIFl9k1YkgCAiMMazCCBIVt+kFQkgiAiM8SwCCJLVN2lFAggiAmM8iwCCZPVNWpEAgojAGM8igCBZfZNWJIAgIjDGswggSFbfpBUJIIgIjPEsAgiS1TdpRQL/Af1NsdjSc/ZtAAAAAElFTkSuQmCC") left center no-repeat;
			padding: 12px 20px;
			background-size: 100%;
			cursor: pointer;
		}

		.col-first .col-first-item.up .tit .icon-up {
			background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAIoklEQVR4Xu2cvYsdZRyFzzWilZjgFyZBUURBGwtLCawBRRFM4SdBRBD7TRA7SSMoQVejlVamiZ1YpLJQ82WhhYIW+leYjYIku7kyMXfdm927M++5Mzd75zzb7ntm5vec99mZuZvsQHxBAAITCQxgAwEITCaAIOwOCGxBAEHYHhBAEPYABDwC3EE8bqRCCCBISNGM6RFAEI8bqRACCBJSNGN6BBDE40YqhACChBTNmB4BBPG4kQohgCAhRTOmRwBBPG6kQgggSEjRjOkRQBCPG6kQAggSUjRjegQQxONGKoQAgoQUzZgeAQTxuJEKIYAgIUUzpkcAQTxupEIIIEhI0YzpEUAQjxupEAIIElI0Y3oEEMTjRiqEAIKEFM2YHgEE8biRCiGAICFFM6ZHAEE8bqRCCCBISNGM6RFAEI8bqRACCBJSNGN6BBDE40YqhACChBTNmB4BBPG4kQohgCAhRTOmRwBBPG6kQgggSEjRjOkRQBCPG6kQAggSUjRjegQQxONGKoQAgoQUzZgeAQTxuJEKIYAgIUUzpkcAQTxupEIIIEhI0YzpEUAQjxupEAIIElI0Y3oEEMTjRiqEAIKEFM2YHgEE8biRCiGAICFFM6ZHAEE8bqRCCCBISNGM6RFAEI8bqRACCBJSNGN6BBDE40YqhACChBTNmB4BBPG4kQohgCAhRTOmRwBBPG6kQgggSEjRjOkRQBCPG6kQAggSUjRjegQQxONGKoQAgoQUzZgeAQTxuJEKIYAgIUUzpkcAQTxupEIIIEhI0YzpEUAQjxupEAIIElI0Y3oEEMTj1lJq6aA0/F069FNLB+QwLRNAkJaBNj/cRy9JwxOSLkiX90mHf2meZeWsCCDIrEiPnWdNjhH/ZSS5LkXUnhRBahG1vWCDHKMTIEnbqFs4HoK0ALH5ISbKgSTNIc50JYLMDHetHEgysy6anwhBmrOaYmVjOZBkCspdRBGkC6pbv5A3PSPvJE1JdbgOQTqEKxXfOa69GiTptJ/6gyNIPSNzxdRy8Lhlkm8zhiBt0lw7VmtyIEkn/TQ/KII0Z9VwZetyIElD8l0sQ5BWqXYmB5K02lPzgyFIc1Y1KzuXA0la66r5gRCkOastVi69Kum4cahfJe2VtLMsO7wg6Qn+FXAZNWc1gjjUxjLunWP4mzR4XNqxW1o9I2lX4aXwEXAhMGc5gjjU1jJX7hxfSCrkOJJj8c//DnXsYWnle2lwe+HlIEkhsNLlhcWWHr7P69uSY8TogwelwVkk2V57BkGsPlw59LOkBWl057j25Ehi1dFhCEGK4U4jx8V90tvVC/YWX0hSXEmHAQQpgtu1HDxuFdUxg8UI0hjyrORAksaVzGAhgjSCbMvxo3Rxf/1j1aSLqB63bjgl6a5Gl/n/omVpuJ/fkxRS22Q5gtQynEaOlQXprb9rT7HlgqP3STf+YEjylzRcQJLp6CPIlvyutxyji0OS6ba5n0aQiey2ixxI4m/v6ZMIsinD7SYHkky/1b0jIMgGbq4cw3PS6pPTv3PUFcnjVh2hNr+PIGM0p5Fj137p9X/aLGfysSpJdpyWBnsKz8eLeyEwBFkDNi9yjC7443uk1XNIUrjjC5cjyBVg8yYHkhTuc3s5gsytHEhi7/qCYLgg7p1D30k7n57dO0ddozxu1RFyvx8syDRynH9KOnLRhd5NDkm64BoqSN/kGHvcOiUN7i3cLHy6NQFYoCB9lWPU8Ce7pUvVp1tIUvhTYrPlYYL0XQ4kacGJsUMECWLL8Y10/tnt985RtxW4k9QRavL9EEGW3pD0WflfH1ElxzPSkZUmMLffGiSZtpMAQa7I8bkBas7l4HHL6HxDpOeCpMuxXpKV05LuL9w08Z9u9VgQ5BiX4eidV/9nIpIU/JToqSC2HCel8wfm952jrnkkqSN07fd7KMiHL0uDE6UgpOHX0qED5bl5SyzdLQ2rXyY+UHbl1R/MHj4mHf6jLDffq3soyNJOaXhGGjxSUM1Jac9z0ourBZk5XurcSYZfSssHpSOX53jw4kvvoSAVgyJJwuQY7ZESSSo5Dr1SvLt6EOipII0lCZVjTJLq7249NHkv58pRMemxICNJ9K2kRzdugOFX0t4Xch6rJinw6W3SpbMTJDkuLb7WgxuBPULPBam4vH+LdFP1U3KdJJUcy8+nPU9P3iWbShIvR8AdZLQl1kuCHJuLMiYJclyFFHAHGW2H926Vbj4sLb5j3297Hzx2h7T6prT4bu9HbThgkCANibAMAusIIAjbAQJbEEAQtgcEEIQ9AAGPAHcQjxupEAIIElI0Y3oEEMTjRiqEAIKEFM2YHgEE8biRCiGAICFFM6ZHAEE8bqRCCCBISNGM6RFAEI8bqRACCBJSNGN6BBDE40YqhACChBTNmB4BBPG4kQohgCAhRTOmRwBBPG6kQgggSEjRjOkRQBCPG6kQAggSUjRjegQQxONGKoQAgoQUzZgeAQTxuJEKIYAgIUUzpkcAQTxupEIIIEhI0YzpEUAQjxupEAIIElI0Y3oEEMTjRiqEAIKEFM2YHgEE8biRCiGAICFFM6ZHAEE8bqRCCCBISNGM6RFAEI8bqRACCBJSNGN6BBDE40YqhACChBTNmB4BBPG4kQohgCAhRTOmRwBBPG6kQgggSEjRjOkRQBCPG6kQAggSUjRjegQQxONGKoQAgoQUzZgeAQTxuJEKIYAgIUUzpkcAQTxupEIIIEhI0YzpEUAQjxupEAIIElI0Y3oEEMTjRiqEAIKEFM2YHgEE8biRCiGAICFFM6ZHAEE8bqRCCCBISNGM6RFAEI8bqRACCBJSNGN6BBDE40YqhACChBTNmB4BBPG4kQohgCAhRTOmRwBBPG6kQgggSEjRjOkRQBCPG6kQAggSUjRjegQQxONGKoQAgoQUzZgeAQTxuJEKIYAgIUUzpkcAQTxupEIIIEhI0YzpEfgXgYDG2J13asAAAAAASUVORK5CYII=") left center no-repeat;
			padding: 12px 20px;
			background-size: 100%;
			cursor: pointer;
			/*Added margin to align the up and down chevrons*/
			margin-left:2px;
			
		}

		.col-first .col-first-item.up .con {
			display: block;
		}

		.col-second {
			border: 1px solid rgba(204, 204, 204, 0.5);
			margin-left: -1px;
		}

		.col-second:last-child {
			border-right: 1px solid #ccc;;
		}

		.col-second-tit {

			text-align: center;
			margin: 0 -15px;
			color: #ffffff;
			padding: 15px 5px;
			height: 160px;
		}

		.col-second-tit.c1 {
			background-color: rgba(49, 142, 154, 1);
		}

		.col-second-tit.c2 {
			background-color: rgba(0, 114, 129, 1);
		}

		.col-second-tit.c3 {
			background-color: rgba(0, 90, 102, 1);
		}

		.col-second-tit .col-second-tit-p0 {
			font-size: 14px;
			display: none;
		}

		.col-second-tit .col-second-tit-p1 {
			font-size: 16px;
			font-weight: bold;
		}

		.col-second-tit .col-second-tit-p2 {
			font-size: 24px;
			font-weight: bold;
		}

		.col-second-tit .col-second-tit-btn {
			width: 200px;
			height: 40px;
			line-height: 40px;
			background-color: #f07662;
			color: #ffffff;
			text-align: center;
			font-size: 14px;
			cursor: pointer;
			margin: 0 auto;
			font-weight: 700;
			border-bottom: 2px solid #ff1721;
		}

		.col-second-tit .col-second-tit-btn-disabled {
			width: 200px;
			height: 40px;
			line-height: 40px;
			background-color: #CD9086;
			color: #D4D4D4;
			text-align: center;
			font-size: 14px;
			cursor: pointer;
			margin: 0 auto;
			font-weight: 700;		
		}
		
		.col-second.choose {
			/*Decreased the margin-top to -31px to align selected column with other 2 columns*/
			border: 2px solid #027180;
			margin-top: -31px;
		}

		.col-second.choose .col-second-tit {
		/*Increased the height from 180px to 190px to align selected column with other 2 columns*/
			height: 190px;
		}

		.col-second.choose .col-second-tit-p0 {
			display: block;
		}

		.col-second .col-second-item {
			position: relative;
			margin: 0 -15px;
			padding: 0 5px;
			border-top: 1px solid #ccc;
		}

		.col-second .col-second-item .tit {
			line-height: 40px;
			height: 40px;
			text-align: center;
		}

		.col-second .col-second-item .con {
			padding: 25px;
			display: none;
			min-height: 200px;
			text-align: center;
			font-weight: bold;
		}

		.icon-oppsite {
			background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAJf0lEQVR4Xu3dy2sdRBTH8ZnbhYUqpv+GbtwURETUUkU3CoLWhVhXoiC2Uh+reONGS8WkO1dC3ajoVlBLkUqhiF3oH+HaB4ZaaTNyG2OTNrl35sz7zNf1nHn8zvlwK01ureE/EiCBPROwZEMCJLB3AgBhOkhgTgIAYTxIACDMAAnIEuATRJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGs0zZQkARJYbVYMkAJBBGt31M0+ePmA+fHO9xhsAUiN1zvRP4Nh0vz1ozhtnfnO/3vOU+fLZ6/7F8SsBEp8hO+RK4D8c1pgHZkc4Z74ujQQguZrLvnEJ3IJja7PSSAAS10aqcySwB44aSACSo8HsKU9gAY7SSAAibyWVqRPwxFESCUBSN5n9ZAkE4tiBZMk8babTa7KD51cBJEeq7BmWgBDHNiTn3JJ5MgcSgIS1ktWpE4jEkRsJQFI3nP38E0iEIycSgPi3k5UpE0iMIxcSgKRsOnv5JZAJRw4kAPFrKatSJXDy9AF7bf0ba82DqbbcdR/nvthYWzkaewZAYhOk3j+BzJ8cNz9B3FVn9z1uVpcv+F9u95UAiU2Qer8EOsQxexhA/NrLqpgEOsUBkJimU+uXQMc4AOLXYlZJE+gcB0CkjaducQIKcABkcZtZIUlACQ6ASJpPzfwEFOEACMOeNgFlOACSdjzG3k0hDoCMPdLpXq8UB0DSjci4O5XCYcwVZyZPpPjxkZBm8TfpIWmxdmcCJXG4yWGztnypdAsAUjpxLecNgIM/YmkZ1tLvGAQHQEoPlobzBsIBEA0DW/INg+EASMnh6v2sAXEApPehLXX/QXEApNSA9XzOwDgA0vPglrj74DgAUmLIej0DHDc6x18U9jrAOe8Njv/TBUjOQetxb3Ds6BpAehziXHeefanb9fXvtv5NwFzHuNkPHlb62arQNwEkNDGt6ze/8fB7a82hnE/sCQf/D5JzEnraGxx7dotPkJ4GOcddwTE3VYDkGLpe9gTHwk4BZGFESheAw6uxAPGKSdkicHg3FCDeUSlZCI6gRgIkKK7OF4MjuIEACY6s0wJwiBoHEFFsnRWBQ9wwgIij66QQHFGNAkhUfI0XgyO6QQCJjrDRDcCRpDEASRJjY5uAI1lDAJIsykY2AkfSRgAkaZyVNwNH8gYAJHmklTYER5bgAZIl1sKbgiNb4ADJFm2hjcGRNWiAZI038+bgyBwwX/uTPeBsB4AjW7TbN677CfLK+wfN/quvmdXpe0Veq+WQV6d32jvMtwW+feRvt2GOmDPTi1qiC31HPSDHp0vWmovWmHuNM59urE1fDL38kOv55Cja9jpAtuPYei5IFjceHIszSryiPJDdcIBkcVvBsTijDCvKApmHAyR7txccGUbfb8tyQHxwgOT2roHDb5IzrSoDJAQHSG62GhyZxt5/2/xAJDhAYgw4/Kc448q8QGJwjIwEHBlHPmzrfEBS4BgRCTjCJjjz6jxAUuIYCUkpHM785czkMbO2fCnzfHW/fXogOXCMgKQkDuseMasrl7uf3gIPSAskJw7NSMBRYNRlR6QF8sbKMxPnvpJdJajq7Mbq9FhQRauLN3/w8Jw15v6cV3TGrDs3OcIfq8JSTgtkdvbxlRes2ThrrU2/9863fb6xOn0+7LmNreaTo7GG3H6dPEMMksWNB8fijBpYkQcInyTzWwuOBkbf7wr5gIBk9w6Aw28yG1mVFwhIdrYZHI2Mvf818gMByWY3wOE/lQ2tLANkdCTgaGjkw65SDsioSMARNpGNrS4LZDQk4Ghs3MOvUx7IKEjAET6NDVbUAaIdCTgaHHXZleoB0YoEHLJJbLSqLhBtSMDR6JjLr1UfiBYkb526y/5z5by15pC8HYsr3eyXnfh9jsVBJVrRBpDekYAj0Ti2t007QHpFsonjB2vNfTnbyydHznT33rstIL0hAUedqS14antAekECjoJjWu+oNoG0jgQc9Sa28MntAmkVCTgKj2jd49oG0hoScNSd1gqntw+kFSTgqDCe9Y/sA0htJOCoP6mVbtAPkFpIwFFpNNs4ti8gpZFcMy/bfeZCkb8EdJNHzZnln9oYC26xlUB/QGY3P7Hy0sS4T3K30Tnzu7VmKec5N77xcGNy2JxZ/jHnOewtS6BPILO3vr7ynLUbnxX4BkdZsh5V/PiIR0iVl/QLpHMk4Kg8+Z7H9w2kUyTg8JzOBpb1D6QzJOBoYOoDrqADSCdIwBEwmY0s1QOkcSTgaGTiA6+hC0ijSMAROJUNLdcHpDEk4Gho2gVX0QmkESTgEExkYyV6gVRGAo7GJl14Hd1AKiEBh3AaGyzTD6QwEnA0OOURVxoDSCEk4IiYxEZLxwGSGQk4Gp3wyGuNBSQTEnBETmHD5eMBSYzEOfOns+6wWV253HCfuZowgTGBJEICDuHUdVQ2LpBIJDdwTCYPmY+Wf+mo31w1MIGxgQiRgCNwyjpeDpBAJODoeNoFVwfIVmgev+MODsGEdV4CkO0NnIMEHJ1PuvD6ALk1uF2QgEM4XQrKALJbE7chAYeCKY94AkD2Cu/Eu0etsR87Zx42a9OfIzKmtOMEADKveW9/cLc59c4fHfeXq0cmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10UmAJDIACnXnQBAdPeX10Um8C9b9JkU6884WQAAAABJRU5ErkJggg==") left center no-repeat;
			padding: 12px 20px;
			background-size: 55%;
		}

		.col-second .col-second-item.up .con {
			display: block;
		}

		.btns-wrap {
			margin: 50px auto 0;
		}

		.btns-wrap .tr {
			text-align: right
		}

		.btn-confirm {
			display: inline-block;
			font-size: 16px;
			color: #ffffff;
			font-weight: bold;
			background-color: #00008f;
			line-height: 50px;
			padding: 0 20px;
			letter-spacing: 2px;
		}

		.wrapper-content2 {
			padding-top: 50px;
		}
		.close {
			color: #00008F;
			font-size: 16px;
			opacity: 1;
			padding-top: 5px;
			float: right;
		}

		
	</style>
	<script>
	
	// Following lines 271 to 295 represent redundant AJAX code that was initially planned to be leveraged for populating data in OPTION 2 of Comparison Page
	// Instead implemented the function for populating data in OPTION 2 inside Upload2compareServlet
	var xhr = new XMLHttpRequest();
	//xhr.open("POST", 'http://localhost:8081/v1/quotegenerator/get-quote');
	//xhr.open("GET", 'https://cors-anywhere.herokuapp.com/https://learnwebcode.github.io/json-example/animals-1.json', true);
	xhr.open("GET", 'https://learnwebcode.github.io/json-example/animals-1.json', true);
	//Send the proper header information along with the request
	xhr.setRequestHeader("Content-Type", "application/json");

	xhr.onload = function() { // Call a function when the state changes.
	    //if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
	        // Request finished. Do processing here.
			var ourData = xhr.responseText;
	        //console.log(ourData);
	    //}
	}
	
	var bodyString = '{"address.1a345678.city":"Chicago","address.1a345678.line1":"3655NorthArtesianAve","address.1a345678.state":"IL","address.1a345678.zipcode":"60618","person.1ab45678.dob":"1990-12-31","person.1ab45678.email":"test@email.com","person.1ab45678.firstName":"JohnBhaiya","person.1ab45678.lastName":"Lalla","policy.home.effectiveDate":"2020-08-06","primaryInsured.person.creditDisclosureFlag":true,"product.lob":"home","partner.key": "7e7db4b4-d5ee-4ccc-9836-cd68a079de1a","partner.channel": "Mobile","address.1a345678.city": "Chicago","address.1a345678.line1": "3655 North Artesian Ave","address.1a345678.line2": "","address.1a345678.state": "IL","address.1a345678.zipcode": "60618","exteriorWall.8atcioxv.constructionMaterial": "Vinyl siding","exteriorWall.8atcioxv.constructionMaterialPercentage": "100","floor.8atcioxv.percentage": "100","floor.8atcioxv.type": "Hardwood","foundation.8atcioxv.foundationType": "Slab","foundation.8atcioxv.percentage": "100","heatingSource.1a345678.material": "Prefabricated/Zero Clearance","heatingSource.1a345678.type": "Built-in fireplace","interiorWall.8atcioxv.material": "Drywall/Veneer Plaster","interiorWall.8atcioxv.percentage": "100","person.1ab45678.dob": "1991-02-02","person.1ab45678.email": "xyz.abcde@test.com","person.1ab45678.firstName": "xyz","person.1ab45678.lastName": "lastnameabcd","person.1ab45678.phone": "524-341-8225","policy.renters.effectiveDate": "2020-09-29","primaryInsured.address.lock": "true","primaryInsured.address.ref": "address.1a345678","primaryInsured.person.lock": "true","primaryInsured.person.ref": "person.1ab45678","property.airConditioning": "true","property.ceiling.cathedralOrVaulted": "0","property.ceiling.height": "8 feet or less (most common)","property.ceiling.roomsWithCrownMolding": "0","property.exteriorWall.ref": "exteriorWall.8atcioxv","property.floor.ref": "floor.8atcioxv","property.foundation.percentageFinished": "0","property.foundation.ref": "foundation.8atcioxv","property.fullBaths": "2","property.garage.size": "1 Car","property.garage.type": "Attached","property.halfBaths": "0","property.heating.primaryType": "Electric","property.heatingSource.ref": "heatingSource.1a345678","property.homeBuiltOnSlope": "false","property.interiorWall.ref": "interiorWall.8atcioxv","property.kitchenCountertopMaterial": "Tile","property.livingArea": "1900","property.numberOfStories": "One","property.roof.installOrReplace": "2012","property.roof.material": "Slate","property.roof.shape": "Gable","property.singleOrMultiFamily": "1 Single Family","property.styleOfHome": "Colonial","property.yearBuilt": "1991","product.lob": "renters","property.fireplace": "no","primaryInsured.person.creditDisclosureFlag": "true"}';
	
	var body = JSON.parse(bodyString);
	//xhr.send(body);

	xhr.send();
	// xhr.send(new Int8Array()); 
	// xhr.send(document);
	// xhr.send(new Int8Array()); 
	// xhr.send(document);
        //click up/down arrow
        $(function () {
            $(".col-second-tit-btn").off('click').on('click', function () {
                var _index = $(this).parents('.col-second').index()
                $(".col-second").removeClass('choose')
                $(this).parents('.col-second').addClass('choose')
            })
			
            $(".col-second-tit-btn-disabled").attr("disabled", "disabled").off('click');
            
            $(".col-first-item .icon-up").off('click').on('click', function () {
                var _pindex = $(this).parents('.col-first-item').attr('data-id')
                $(".col-first-item").removeClass('up')
                $(".col-second-item").removeClass('up')
                $(this).parents('.col-first-item').addClass('up')
                $('.col-second-item').each(function () {
                    var _this = $(this)
                    if (_this.attr('data-id') == _pindex) {
                        _this.addClass('up')
                    }
                })
            })
        })
	</script>
</head>

<%@ page import = "com.utils.ViaConstant, 
					org.json.simple.JSONObject, 
					org.json.simple.parser.JSONParser, 
					java.util.HashMap,
					java.text.NumberFormat,
					java.util.regex.*" %> 
					
<%@ page session="true" %>

<%!
//Function to extract non-null values from current policy document
private String getCurrentValueIfNotEmpty(HashMap<String, String> map, String key, String searchType){
	// Extract "Current" policy's Coverage A to F and Deductible
	if(searchType == "coverage"){
		String value = map.get(key);
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		nf.setMinimumFractionDigits( 0 );
		// $ 130000 is converted to format $130,000
		// replaced $ and comma and converted to currency format
		// Then added "$" character explicilitly as the AWS was unable to render the $ symbol
		try
		 {return value == null ? "" : "$"+nf.format(Integer.parseInt(map.get(key).substring(0, map.get(key).indexOf(";")).replaceAll(",","").replaceAll("\\s","").replace("$",""))).substring(1);
			
		 }catch(NumberFormatException ex){
			 return map.get(key).substring(0, map.get(key).indexOf(";"));
		 }
	}
	// Extract address details: address_1, address_2, city, state, zip
	else if (searchType == "other"){
		String value = map.get(key);
		return value == null ? "" : map.get(key).substring(0, map.get(key).indexOf(";"));
	}
	// Extract premium amount for "current policy"
	else if (searchType == "premium"){
		String value = map.get(key);
		if (value != null){
			// Premium amount starts with '$' symbol e.g. $819 
			//if(value.charAt(0) == '$'){
				//processPremiumAmount(map.get(key).substring(0, map.get(key).indexOf(";")).replace("$",""), "current");
				 return map.get(key).substring(0, map.get(key).indexOf(";")).replace("$","");
			//}
			// Premium amount starts with '$' symbol e.g. 1400
			//else{
				//processPremiumAmount(map.get(key).substring(0, map.get(key).indexOf(",")), "current");
			//}
		}
		else return "";
	}
	return "";
} 


//Convert the premium amount to Currency format "$###,###.##/year"
private static String processPremiumAmount(String premium, String typeOfPremium){
	// Process premium amount for "Best Fit" 
	if(typeOfPremium == "best"){
	NumberFormat nf = NumberFormat.getCurrencyInstance();
	nf.setMinimumFractionDigits( 0 );
	//best premium is of the format 1342/year
	String before_decimal = premium.substring(0, premium.indexOf("/"));
	return "$"+nf.format(Integer.parseInt(before_decimal)).substring(1) + "/year";
	}
	// Process premium amount for "Option 2" third column policy 
	if(typeOfPremium == "thirdQuote"){
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		nf.setMinimumFractionDigits( 0 );
		//thirdQuote premium is of the format 742.0
		// pick the digits before decimal point
		String before_decimal = premium.substring(0, premium.indexOf("."));
		// Add the relevant formatting
		return "$"+nf.format(Integer.parseInt(before_decimal)).substring(1) + "/year";
		}
	// Process premium amount for "Current" policy 
	// if the original string has "/mo" then multiply by 12, else just insert "/year"
	else if(typeOfPremium == "current"){
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		nf.setMinimumFractionDigits( 0 );
		// if the current premium amount is of format "/year"
		if(premium.contains("/year")){
			// premium is of the format 1400.00/year -> premium_amount contains "1400"
			String premium_amount_digits_only = extractPremiumAmount(premium);
			// Convert 1400 to Currency format "$1,400.00"
			return "$"+nf.format(Integer.parseInt(premium_amount_digits_only)).substring(1) + "/year";
		}
		// if the current premium amount is of format "/mo"
		else if(premium.contains("/mo")){
			// premium is of the format 100.00/mo -> premium_amount contains "100"
			String premium_amount_digits_only = extractPremiumAmount(premium);
			// Convert monhtly amount (100) to yearly amount and then convert to
			// Currency format "$1,200.00"
			return "$" + nf.format(Integer.parseInt(premium_amount_digits_only)*12).substring(1) + "/year";
		}
		// if the current premium amount doesn't specify "/mo" or "/year"
		else{
			// premium is of the format 1400.00 -> premium_amount contains "1400"
			String premium_amount_digits_only = extractPremiumAmount(premium);
			// Convert amount (1400) to currency format "$1,400.00"
			return "$" + nf.format(Integer.parseInt(premium_amount_digits_only)).substring(1) + "/year";
		}
	}
	return "";
}

//Function to extract the actual amount of premium from the response -> extract "1400.00 / year" to "1400"
private static String extractPremiumAmount(String premium){


// remove trailing "/mo" and "/year" and "$" at the starting
//premium.replaceAll("[^a-zA-Z/]","");
//String extracted_premium_non_alphabets = premium.replace(".","");
String extracted_premium_non_alphabets = premium.replaceAll("[a-zA-Z/$\\s]","");
// replace tralining ".00" if present
String extracted_premium_digits = extracted_premium_non_alphabets.replaceAll("\\.\\d*$","");

return extracted_premium_digits;
}
// Function to extract non-null values from "Best" suggested Policy 
private String getBestValueIfNotEmpty(JSONObject json, String key, String searchType){
	// Extract "Best" policy's Coverage A to F 
	if(searchType == "coverage"){
		// Object created to convert coverage amounts to Currency format "$###,###.##"
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		nf.setMinimumFractionDigits( 0 );
    	try{
    		String value = (String) json.get(key);
        	return value == null ? "" : "$"+nf.format(Integer.parseInt((String) json.get(key))).substring(1);
    	} catch(NumberFormatException ex){
    		return "";
    	}
	}
	// Extract "Best" policy's  Deductible
		if(searchType == "deductible"){
			// Object created to convert coverage amounts to Currency format "$###,###.##"
			NumberFormat nf = NumberFormat.getCurrencyInstance();
			nf.setMinimumFractionDigits( 0 );
	    	try{
	    		//String value = (String) json.get(key);
	        	String value = (String) json.get(key);
				// Converted the dedcutible amount to the currency format including $ symbol
	    		return value == null ? "" : "$" + nf.format(Math.round(Double.parseDouble(value))).substring(1);
	    	} catch(NumberFormatException ex){
	    		return "";
	    	}
		}
	// Extract "Name", "Address", "units", "people", "home_rent" details of user
	else if (searchType == "other"){
		String value = (String) json.get(key);
    	return value == null ? "" : (String) json.get(key);
	}
	return "";
	}

// Function to print the message in
private String getColumn3ValueIfNotEmpty(JSONObject json, String key, String searchType){
	// Extract "Best" policy's Coverage A to F and deductible
	if(searchType == "coverage"){
		// Object created to convert coverage amounts to Currency format "$###,###.##"
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		nf.setMinimumFractionDigits( 0 );
    	try{
    		// double is returned from PATCH request for all coverages, so convert to string
    		String value = String.valueOf(json.get(key));
        	return value == null ? "" : "$" + nf.format(json.get(key)).substring(1);
    	} catch(NumberFormatException ex){
    		return "";
    	}
	}
	// Extract "Name", "Address", "units", "people", "home_rent" details of user
	else if (searchType == "other"){
		String value = (String) json.get(key);
    	return value == null ? "" : (String) json.get(key);
	}
	return "";
	}
%>

<% 
//Retrieve the user details from "userProfile" which in turn has been generated by scanning the document using Attestiv's API
HashMap<String, String> userProfile =(HashMap<String, String>) session.getAttribute("userProfile");

// Store the details of the response "statusString" received from "Quote Generator" -> Best quote's details
String statusString =(String) session.getAttribute(ViaConstant.statusString);
String regenQuoteResponse =(String) session.getAttribute("regenQuoteStatusString");
System.out.print("This is response in JSP from POST request");
System.out.print(statusString);
String email =(String) session.getAttribute(ViaConstant.email);

// Convert "String" to "JSON object" so as to fetch the name, value pairs easily
JSONParser parser = new JSONParser();
JSONObject json = (JSONObject) parser.parse(statusString);
System.out.print("regenQuoteStatusString");
System.out.print(regenQuoteResponse);
JSONObject regenQuoteResponseJson = (JSONObject) parser.parse(regenQuoteResponse);

// Variables to store the Customer's name and status of the "Quote Generator" response
String status = getBestValueIfNotEmpty(json, ViaConstant.status, "other");
String first_name = getBestValueIfNotEmpty(json, ViaConstant.first_name, "other");
System.out.print("This is JSON value of PATCH request (OPTION 2)");
System.out.print(json);
String middle_name = getBestValueIfNotEmpty(json, "middle_name", "other");
String last_name = getBestValueIfNotEmpty(json, ViaConstant.last_name, "other");

//Variables to store the address of user
String address_1 = getBestValueIfNotEmpty(json, ViaConstant.address_1, "other");
String address_2 = getBestValueIfNotEmpty(json, ViaConstant.address_2, "other");
String city = getBestValueIfNotEmpty(json, ViaConstant.city, "other");
String state = getBestValueIfNotEmpty(json, ViaConstant.state, "other");
String zip = getBestValueIfNotEmpty(json, ViaConstant.zip, "other");
String home_rent = getBestValueIfNotEmpty(json, ViaConstant.home_rent, "other");
String units = getBestValueIfNotEmpty(json, ViaConstant.units, "other");
String people = getBestValueIfNotEmpty(json, ViaConstant.people, "other");
String company = getBestValueIfNotEmpty(json, ViaConstant.company, "other");

//Variables to store user's best fit policy's information -> Coverage A to F, Deductible and Premium
String best_coverage_a = getBestValueIfNotEmpty(json, ViaConstant.coverage_a, "coverage");
String best_coverage_b = getBestValueIfNotEmpty(json, ViaConstant.coverage_b, "coverage");
String best_coverage_c = getBestValueIfNotEmpty(json, ViaConstant.coverage_c, "coverage");
String best_coverage_d = getBestValueIfNotEmpty(json, ViaConstant.coverage_d, "coverage");
String best_coverage_e = getBestValueIfNotEmpty(json, ViaConstant.coverage_e, "coverage");
String best_coverage_f = getBestValueIfNotEmpty(json, ViaConstant.coverage_f, "coverage");
String best_deductible = getBestValueIfNotEmpty(json, ViaConstant.deductible, "deductible");
String best_effective_date = getBestValueIfNotEmpty(json, ViaConstant.deductible, "other");
// Handle the best_premium separately as the value returned contained "/year"
String best_premium = processPremiumAmount(getBestValueIfNotEmpty(json, ViaConstant.premium, "other"), "best");


//Variables to store user's best fit policy's information -> Coverage A to F, Deductible and Premium
String column3_coverage_a = String.valueOf(getColumn3ValueIfNotEmpty(regenQuoteResponseJson, ViaConstant.coverage_a, "coverage"));
String column3_coverage_b = String.valueOf(getColumn3ValueIfNotEmpty(regenQuoteResponseJson, ViaConstant.coverage_b, "coverage"));
String column3_coverage_c = String.valueOf(getColumn3ValueIfNotEmpty(regenQuoteResponseJson, ViaConstant.coverage_c, "coverage"));
String column3_coverage_d = String.valueOf(getColumn3ValueIfNotEmpty(regenQuoteResponseJson, ViaConstant.coverage_d, "coverage"));
String column3_coverage_e = String.valueOf(getColumn3ValueIfNotEmpty(regenQuoteResponseJson, ViaConstant.coverage_e, "coverage"));
String column3_coverage_f = String.valueOf(getColumn3ValueIfNotEmpty(regenQuoteResponseJson, ViaConstant.coverage_f, "coverage"));
//fix issue ...page doesn't load if string passed
String column3_deductible = String.valueOf(getColumn3ValueIfNotEmpty(regenQuoteResponseJson, ViaConstant.deductible, "coverage"));
String column3_effective_date = getColumn3ValueIfNotEmpty(json, ViaConstant.deductible, "other");
//Handle the best_premium separately as the value returned contained "/year"
String column3_premium =  processPremiumAmount(String.valueOf(regenQuoteResponseJson.get("premium")), "thirdQuote");

// Variables to store user's current policy information -> Coverage A to F, Deductible and Premium
String current_coverage_a = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_a, "coverage");
String current_coverage_b = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_b, "coverage");
String current_coverage_c = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_c, "coverage");
String current_coverage_d = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_d, "coverage");
String current_coverage_e = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_e, "coverage");
String current_coverage_f = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_f, "coverage");
String current_deductible = getCurrentValueIfNotEmpty(userProfile, ViaConstant.deductible, "coverage");
String current_premium =  processPremiumAmount(getCurrentValueIfNotEmpty(userProfile, ViaConstant.premium, "premium").replaceAll(" ", ""),"current");
%>


<!-- Disabled the top white bar on page 3, 4 and 5 to make the UI simpler for the purpose of PoC demo on 12th August, 2020 -->
<!--<div class="container-fluid justify-content-center text-center">
	<label class="text-center common-title">
		<span style="font-weight: bold; font-size: 24px">Policy scan</span> | <span><%out.print(first_name + " " + last_name + "," + address_1 + address_2 + "," + city + "," + state ); %></span>
		<img src="img/QuestionMark.png" style="width: 20px; height: 20px; margin-left: 100px; margin-right: 10px">
		<span style="color:  #00008F;" >Help</span>
		<span style="color: #00008f; margin-left: 200px"> <a href="index.html"> Cancel</a></span>
	</label>
</div>-->


<div class="container-fluid  navbar2">
	<div class="navbar2-item"></div>
	<div class="navbar2-item crr" onclick="window.location='./upload2'"><span>&check;</span>Upload Policy</div>
	<div class="navbar2-item crr"><span>2</span>Compare quotes</div>
	<div class="navbar2-item"><span>3</span>Switch and Save</div>
</div>

<div class=" wrapper-content  clearfix" style="margin-top: 20px">
	<div class="container">
		<div class="block-heading text-center">
			<h2 class="text-center" style="font-size: 50px; font-weight: normal; font-family: Garamond, serif">
				We found you better coverage on your policy
			</h2>
		</div>
	</div>
	<div class=" wrapper-content2  clearfix ">
		<div class="container-fluid">
			<div class="card ">
				<div class="row">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-first">
						<div class="col-first-tit">
							<p class="col-first-tit-p1">Quote ID: GE5626 </p>
							<p>
								<form class="col-first-tit-form">
									<input type="text" class="col-first-tit-input" placeholder="Email" value="<%out.print(email); %>" name="form_email">  
									<button class="col-first-tit-form-btn"><i class="fa fa-paper-plane-o"style="font-size:24px; color:darkblue; "></i></button>
								</form>
							</p>
						</div>
						<div class="col-first-item" data-id="0">
							<div class="tit"><i class="icon-up"></i>Dwelling</div>
							<div class="con">
								If something goes wrong, we'll pay up to this amount for you to fix or rebuild your
								home.
							</div>
						</div>
						<div class="col-first-item" data-id="1">
							<div class="tit"><i class="icon-up"></i>Other structures</div>
							<div class="con">
								If something goes wrong, we'll pay up to this amount for you to fix or rebuild a
								detached structure such as a garage or shed.
							</div>
						</div>

						<div class="col-first-item" data-id="2">
							<div class="tit"><i class="icon-up"></i>Personal Property</div>
							<div class="con">
								We'll cover your stuff, like your clothing, TVs, and appliances up to this amount – if
								they’re stolen or damaged.
							</div>
						</div>
						<div class="col-first-item" data-id="3">
							<div class="tit"><i class="icon-up"></i>Temporary Housing Expense</div>
							<div class="con">
								If you are unable to live in your home due to damage or ongoing repairs, your temporary
								living arrangements are covered up to this amount.
							</div>
						</div>
						<div class="col-first-item" data-id="4">
							<div class="tit"><i class="icon-up"></i>Personal Liability</div>
							<div class="con">
								You're covered up to this amount if you accidentally injure someone on your property or
								damage their property.
							</div>
						</div>

						<div class="col-first-item" data-id="5">
							<div class="tit"><i class="icon-up"></i>Medical Payments</div>
							<div class="con">
								If someone is injured on your property, even if you’re not at fault, we'll cover their
								medical expenses up to this amount.
							</div>
						</div>
						<div class="col-first-item" data-id="6">
							<div class="tit"><i class="icon-up"></i>Standard Deductible</div>
							<div class="con">
								The amount you’d have to pay out of pocket before your insurance kicks in, if something
								goes wrong.
							</div>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-second">
						<div class="col-second-tit c1" style="background: #A9A9A9">
							<p class="col-second-tit-p0">Selected</p>
							<p class="col-second-tit-p1">CURRENT POLICY</p>
							<p class="col-second-tit-p2"><%out.print(current_premium); %></p>
							<div class="col-second-tit-btn-disabled">CURRENT POLICY</div>
						</div>
						<div class="col-second-item" data-id="0">
							<div class="tit"><%out.print(current_coverage_a); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="1">
							<div class="tit"><%out.print(current_coverage_b); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="2">
							<div class="tit"><%out.print(current_coverage_c); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="3">
							<div class="tit"><%out.print(current_coverage_d); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="4">
							<div class="tit"><%out.print(current_coverage_e); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="5">
							<div class="tit"><%out.print(current_coverage_f); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="6">
							<div class="tit"><%out.print(current_deductible); %></div>
							<div class="con">
								
							</div>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-second choose">
						<div class="col-second-tit c2">
							<p class="col-second-tit-p0">Selected</p>
							<p class="col-second-tit-p1">OPTION 1</p>
							<p class="col-second-tit-p2"><%out.print(best_premium); %></p>
							<div class="col-second-tit-btn">CHOOSE</div>
						</div>
						<div class="col-second-item" data-id="0">
							<div class="tit"><%out.print(best_coverage_a); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="1">
							<div class="tit"><%out.print(best_coverage_b); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="2">
							<div class="tit"><%out.print(best_coverage_c); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="3">
							<div class="tit"><%out.print(best_coverage_d); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="4">
							<div class="tit"><%out.print(best_coverage_e); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="5">
							<div class="tit"><%out.print(best_coverage_f); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="6">
							<div class="tit"><%out.print(best_deductible); %></div>
							<div class="con">
								
							</div>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-second">
						<div class="col-second-tit c3">
							<p class="col-second-tit-p0">Selected</p>
							<p class="col-second-tit-p1">OPTION 2</p>
							<p class="col-second-tit-p2"><%out.print(column3_premium); %></p>
							<div class="col-second-tit-btn">CHOOSE</div>
						</div>
						<div class="col-second-item" data-id="0">
							<div class="tit"><%out.print(column3_coverage_a); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="1">
							<div class="tit"><%out.print(column3_coverage_b); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="2">
							<div class="tit"><%out.print(column3_coverage_c); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="3">
							<div class="tit"><%out.print(column3_coverage_d); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="4">
							<div class="tit"><%out.print(column3_coverage_e); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="5">
							<div class="tit"><%out.print(column3_coverage_f); %></div>
							<div class="con">
								
							</div>
						</div>
						<div class="col-second-item" data-id="6">
							<div class="tit"><%out.print(column3_deductible); %></div>
							<div class="con">
								
							</div>
						</div>

					</div>
				</div>
				<div class="btns-wrap row">

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tr">
						<span class="btn-confirm">CHECKOUT</span>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

</body>
</html>
