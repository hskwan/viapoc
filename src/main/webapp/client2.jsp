<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Declaration Upload</title>
 </head>
 <body>
 
 <%@ page import="com.utils.ViaConstant" %>
 
 <%@ page session="true" %>
 
    <h3>Upload Declaration File</h3>
 
  <p>Select your declaration page:<br>

  <form method="POST" action="client2servlet" enctype="multipart/form-data">
  
        Select file to upload:
        <input type="file" name="uploadFile" />
        <br/>
        <input type="submit" value="Upload" />
  </form>
  <%
  String statusString =(String) session.getAttribute(ViaConstant.statusString);
  if(statusString!=null )
      out.print("Status:<br>" + statusString );

  %>
   
</body>
</html>