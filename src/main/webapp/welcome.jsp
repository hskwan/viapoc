<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<meta name="renderer" content="webkit">
	<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<meta name="format-detection" content="telephone=no"/>
	<meta http-equiv="Cache-control" content="no-cache">
	<title>Discovery Lab</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<link rel="stylesheet" href="css/main.css">
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-select.js"></script>
	<script src="js/velocity.min.js"></script>
</head>
<body>
<div class="fixed-nav">

	<nav class="navbar navbar-static-top navbar-fixed-top py-2" role="navigation">
		<div class="navbar-header">
			<a target="_blank" href="" class="navbar-brand d-inline-block">
				<img src="img/logo.svg	" style="padding: 5px">
			</a>
		</div>
		<div class="navbar-right clearfix">
			<ul class="nav navbar-nav">
				<li class="active"><a href="" class="anav" target="_blank">HOME <br>INSURANCE</a></li>
				<li><a href="javascript:" class="anav" target="_blank">RENTERS <br> INSURANCE</a></li>
				<li><a href="javascript:" class="anav" target="_blank">ALL <br>PRODUCTS</a></li>
			</ul>
			<div class="search">
				<div class="btn-search"></div>
			</div>
		</div>
	</nav>
</div>

<div class="wrapper wrapper-content  clearfix ">
	<div class="container-fluid">
		<div class="row row1">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title1 justify-content-center">
				<p class="p2">Compare your current policy and save on</p>
				<p class="p2"> homeowners insurance</p>
				<p class="p3"> Upload. Compare. Save.</p>

			</div>

		</div>
		<div class="container" >
			<div >
			<button type="button" class="block" onclick="window.location.href='upload2'"><img src="img/form_upload.svg" class="upload-img"><p class="p4">UPLOAD YOUR POLICY</p></button>
			<button type="button" class="block1" onclick="window.location.href='upload2'"><img src="img/form.svg" class="upload-img"><p class="p4">FILL OUT THE FORM</p></button>
			</div>
			
			<div><img src="img/image.svg" class="img-background"></div>
			
		
		
			<!--   <div class="btn-next land-front" onclick="window.location.href='upload2'"> UPLOAD YOUR POLICY</div> -->
			<!-- <div class="btn2" style="position:absolute; top: 47%; left: 60%; z-index: 2;">I don't have policy yet -> -->
		</div>
</div>
<div class="footer-container">
<!-- Site footer -->
    <footer class="site-footer">
        <div class="row">
          <div class="col-xs-12 col-md-3">
            <h6>QUICK ACCESS</h6>
            <ul class="footer-links">
              <li><a href="#">Call Us</a></li>
              <li><a href="#">Policy Scan</a></li>
              <li><a href="#">Quote Review</a></li>
            </ul>
          </div>

          <div class="col-xs-12 col-md-3">
            <h6>LOREM</h6>
            <ul class="footer-links">
              <li><a href="#">About Us</a></li>
              <li><a href="#">Careers</a></li>
              <li><a href="#">Policy Documents</a></li>
            </ul>
          </div>
          
           <div class="col-xs-12 col-md-3">
            <h6>USEFUL LINKS</h6>
            <ul class="footer-links">
              <li><a href="#">Blog</a></li>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Download App</a></li>
            </ul>
          </div>
          
          <div class="col-xs-12 col-md-3">
            <h6>FOLLOW LOREM</h6>
            <ul class="social-icons">
            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
             <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>  
            <li><a class="youtube" href="#"><i class="fa fa-youtube-play"></i></a></li>
            <li><a class="instagram" href="#"><i class="fa fa-instagram"></i></a></li>  
            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
            </ul>
          </div>
          
          
        </div>
        <hr>
            <p class="copyright-text"> &copy; 2020 Lorem. All Rights Reserved
            </p>
</footer>
</div>
</div>

</body>
<script>
    $(function () {
        var removeMenu = function (restoreNav) {
            $('body').css({
                overflow: '',
                width: ''
            });
            $('#sidenav-overlay').velocity({opacity: 0}, {
                duration: 200,
                queue: false, easing: 'easeOutQuad',
                complete: function () {
                    $(this).remove();
                }
            });
            $("#slide-out").velocity({'margin-left': [-1 * 250, 0]}, {
                duration: 200,
                queue: false,
                easing: 'easeOutCubic'
            });
            $(".menu-mobile-button").removeClass('is-open')
        };
        $(".menu-mobile-button").off("click").on("click", function () {
            $("#slide-out").velocity({'margin-left': [0, -1 * 250]}, {
                duration: 300,
                queue: false,
                easing: 'easeOutQuad'
            });
            var $body = $('body');
            var $overlay = $('<div id="sidenav-overlay"></div>');
            var oldWidth = $body.innerWidth();
            $body.css('overflow', 'hidden');
            $body.width(oldWidth);
            $overlay.css('opacity', 0)
                .click(function () {
                    removeMenu();
                    $overlay.velocity({opacity: 0}, {
                        duration: 300, queue: false, easing: 'easeOutQuad',
                        complete: function () {
                            $(this).remove();
                        }
                    });

                });
            $('body').append($overlay);
            $overlay.velocity({opacity: 0.7}, {
                duration: 300, queue: false, easing: 'easeOutQuad',
            });
            $(".menu-mobile-button").addClass('is-open')
        });

    })
</script>

</html>
