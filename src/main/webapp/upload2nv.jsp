<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import = "com.utils.ViaConstant, 
				   java.util.HashMap,
				   java.text.NumberFormat,
				   java.util.ArrayList,
				   java.lang.Integer" %> 



<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<meta name="renderer" content="webkit">
	<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
	<meta name="format-detection" content="telephone=no"/>
	<meta http-equiv="Cache-control" content="no-cache">
	<title>Verification</title>
	<link rel="stylesheet" href="css/index2.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/velocity.min.js"></script>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

	<!--	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=EB+Garamond"/>-->
	<style>
		body {
			font-family: 'Open Sans', sans-serif;
		
		}
		.card {
			background: #ffffff;
			box-shadow: 0px 0px 8px 0px rgba(153, 154, 157, 0.3);
			padding: 20px 40px;
			margin: 50px 160px;
		}

		.card .tit {
			font-size: 18px;
			font-weight: bold;
		}

		.card .btn-wrap {
			text-align: right;
			margin-top: 60px;
		}

		.card .btn-wrap .btn-edit {
			display: inline-block;
			color: #00008f;
			font-weight: bold;
			font-size: 18px;
			cursor: pointer;
		}
		.card .btn-wrap .btn-done {
			display: none;
			color: #00008f;
			font-weight: bold;
			font-size: 18px;
			cursor: pointer;
		}

		.card .input-wrap {
			border-bottom: 1px solid #ddd;
			position: relative;
			vertical-align: middle;
			margin-top: 10px;
		}

		.card .input-wrap.nbr {
			border-bottom: none
		}

		.card .input-wrap .mylabel {
			float: left;
			display: block;
			padding: 0px 15px;
			line-height: 45px;
			text-align: left;
			color: #ccc;
			font-size: 14px;
		}

		.card .input-wrap .myspan {
			float: right;
			line-height: 45px;
			height: 35px;
			padding: 0 15px;
			font-size: 14px;
			text-align: right;
			display: none;
			cursor: not-allowed;
		}
		
		/*Updated the display attribute to "in-block", hid the /myspan and removed the "EDIT INFORMATION" button - 08/04/20 by Gagan*/
		.card .input-wrap .myinput {
			display: inline-block;
			float: right;
			line-height: 45px;
			height: 38px;
			padding: 0 15px;
			border: none;
			font-size: 14px;
			outline: none;
			text-align: right;
			width:300px;
			/*max-width: 230px;*/
		}

		.btns-wrap {
			margin: 50px auto 0;
		}

		.btns-wrap .tl {
			text-align: left
		}

		.btns-wrap .tr {
			text-align: right
		}

		.btns-wrap .btn-cancel {
			padding: 0 15px;
			height: 60px;
			line-height: 60px;
			color: #00008f;
			text-align: center;
			font-size: 24px;
			font-weight: bold;
			display: inline-block;
			cursor: pointer;
		}

		.btns-wrap .btn-confirm {
			display: inline-block;
			font-size: 16px;
			color: #ffffff;
			font-weight: bold;
			background-color: #00008f;
			line-height: 50px;
			padding: 0 20px;
			letter-spacing: 2px;
			margin-top: 6px;
			margin-right: 160px;
		}

		.common-title {
			display: block;
			margin-top: 10px;
			margin-bottom: 10px;
		}

		.btn-next {
			font-size: 16px;
			color: #ffffff;
			font-weight: bold;
			background-color: #00008f;
			line-height: 40px;
			height: 40px;
			width: 200px;
			padding: 0 20px;
			margin-top: 6px;
			margin-right: 160px;
			text-align: center;
			letter-spacing: 2px;
		}

		.person-header {
			margin-left: -40px;
			margin-top: -20px;
			margin-right: -40px;
			font-size: 25px;
			font-weight: bold;
		}
		/*Added a wait-spinner to showacse that the next page "Comparison" is loading*/
		.loader {
			
			 display:none;
 			 border: 16px solid #4976BA;
			  border-radius: 50%;
			  border-top: 16px solid #DBDBDB;
			  border-bottom: 16px solid #DBDBDB;
			  width: 120px;
			  height: 120px;
			  -webkit-animation: spin 2s linear infinite;
			  animation: spin 2s linear infinite;
			  position:absolute;
			  top:50%;
			  right:47%;
			}

		@-webkit-keyframes spin {
		  0% { -webkit-transform: rotate(0deg); }
		  100% { -webkit-transform: rotate(360deg); }
		}

		@keyframes spin {
		  0% { transform: rotate(0deg); }
		  100% { transform: rotate(360deg); }
		}
		.gear{
			width: 20px;
			height: 20px;
			margin-right: 10px;
			margin-bottom: 5px;"
		}
		.close {
			color: #00008F;
			font-size: 16px;
			opacity: 1;
			padding-top: 5px;
		}
	</style>
	<script>
        $(function () {
            $(".btn-edit").off("click").on("click", function () {
                $(this).parents('.card').find('.myspan').hide()
                $(this).parents('.card').find('.myinput').show()
                $(this).hide()
                $(".btn-done").show()
            })
            $(".btn-cancel").off("click").on("click", function () {
                $('.card').find('.myspan').show()
                $('.card').find('.myinput').hide()
                $(".btn-edit").show()
            })
            $(".btn-done").off("click").on("click", function () {
                $('.card').find('.myspan').each(function () {
                    var _this = $(this)
                    var _word = _this.next('.myinput').val()
                    _this.text(_word)
                })
                $('.card').find('.myspan').show()
                $('.card').find('.myinput').hide()
                $(".btn-edit").show()
	            $(".btn-done").hide()
            })

            $(".btn-confirm").off("click").on("click", function () {
            	var spinner= $(".loader");
            	spinner.show();
                $('.card').find('.myspan').each(function () {
                    var _this = $(this)
                    var _word = _this.next('.myinput').val()
                    _this.text(_word)
                })
                $('.card').find('.myspan').show()
                $('.card').find('.myinput').hide()
                $(".btn-edit").show()
            })

        })
	</script>

</head>
<body>



<%!
// Convert the premium amount to Currency format "$###,###.##/year"
private String processPremiumAmount(String premium, String typeOfPremium){
	// Process premium amount for "Best Fit" 
	if(typeOfPremium == "best"){
	NumberFormat nf = NumberFormat.getCurrencyInstance();
	nf.setMinimumFractionDigits( 0 );
	// best premium is of the format 1342/year
	String before_decimal = premium.substring(0, premium.indexOf("/"));
	return "$" + nf.format(Integer.parseInt(before_decimal)).substring(1) + "/year";
	}
	// Process premium amount for "Current" policy 
	else if(typeOfPremium == "current"){
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		nf.setMinimumFractionDigits( 0 );
		//current premium is of the format 1400.00/year
		// try if the value is a number, otherwise return the expression as it is, e.g. $X
		try{
			String before_decimal = premium.substring(0, premium.indexOf("."));
			return "$" + nf.format(Integer.parseInt(before_decimal)).substring(1) + "/year";
		}catch(StringIndexOutOfBoundsException ex){
			return premium;
			}
		}
	return "";
}

// Function to extract non-null values from current policy document
private String getCurrentValueIfNotEmpty(HashMap<String, String> map, String key, String searchType){
	// Extract "Current" policy's Coverage A to F and Deductible
	if(searchType == "coverage"){
		String value = map.get(key);
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		nf.setMinimumFractionDigits( 0 );
		//$130,000 is converted to format $130,000
		// replaced $ and comma and converted to currency format. Then added "$" character explicilitly as the AWS was unable to render the $ symbol
		try
			{return value == null ? "" : "$" + nf.format(Integer.parseInt(map.get(key).substring(0, map.get(key).indexOf(";")).replaceAll(",","").replaceAll("\\s","").replace("$",""))).substring(1);
		 }catch(NumberFormatException ex){
			 return map.get(key).substring(0, map.get(key).indexOf(";"));
		 }
	}
	// Extract address details: address_1, address_2, city, state, zip
	else if (searchType == "other"){
		String value = map.get(key);
 		return value == null ? "" : map.get(key).substring(0, map.get(key).indexOf(";"));
	}
	// Extract premium amount for "current policy"
	else if (searchType == "premium"){
		String value = map.get(key);
		if (value != null){
			// Premium amount starts with '$' symbol e.g. $819 
			//if(value.charAt(0) == '$'){
				//processPremiumAmount(map.get(key).substring(0, map.get(key).indexOf(";")).replace("$",""), "current");
				 return map.get(key).substring(0, map.get(key).indexOf(";")).replace("$","");
			//}
			// Premium amount starts with '$' symbol e.g. 1400
			//else{
				//processPremiumAmount(map.get(key).substring(0, map.get(key).indexOf(",")), "current");
			//}
		}
		else return "";
	}
	return "";
} 
%>

<%
// Retrieve the user details from "userProfile" which in turn has been generated by scanning the document using Attestiv's API
HashMap<String, String> userProfile =(HashMap<String, String>) session.getAttribute("userProfile");

// Variables to store the address details of user
String first_name = getCurrentValueIfNotEmpty(userProfile, ViaConstant.first_name, "other");
System.out.println("First Name in jsp");
String last_name = getCurrentValueIfNotEmpty(userProfile, ViaConstant.last_name, "other");
String address_1 = getCurrentValueIfNotEmpty(userProfile, ViaConstant.address_1, "other").replaceAll("\\[", "");
String address_2 = getCurrentValueIfNotEmpty(userProfile, ViaConstant.address_2, "other");
String city = getCurrentValueIfNotEmpty(userProfile, ViaConstant.city, "other");
String state = getCurrentValueIfNotEmpty(userProfile, ViaConstant.state, "other");
String zip = getCurrentValueIfNotEmpty(userProfile, ViaConstant.zip, "other").replaceAll("\\]", "");
String email = getCurrentValueIfNotEmpty(userProfile, ViaConstant.email, "other").replaceAll("\\[", "").replaceAll("\\]","");
String birthday = getCurrentValueIfNotEmpty(userProfile, ViaConstant.birthday, "other");

//Split the birthday into day, month, year, if non-empty
if (birthday != ""){
	String[] monthDayYear = birthday.split("/");
	System.out.println(monthDayYear);
	//remove "[" if present e.g. Kin document has the dob in format [mm/dd/yyyy]
	String selectedMonth = monthDayYear[0].replaceAll("\\[", "").replaceAll("\\]","");
	String selectedDay = monthDayYear[1].replaceAll("\\[", "").replaceAll("\\]","");
	String selectedYear = monthDayYear[2].replaceAll("\\[", "").replaceAll("\\]","");
	System.out.println(selectedMonth);
	System.out.println(selectedDay);
	System.out.println(selectedYear);
	
	//set attribute so that can use in selecting the dob
	request.setAttribute("selectedMonth", selectedMonth);
	request.setAttribute("selectedDay", selectedDay);
	request.setAttribute("selectedYear", selectedYear);
}

//if birthday is not returned in the response of attestiv API i.e. API is unable to scan/ dob is not present in document
else{
	String selectedMonth = "0";
	String defaultMonth = "--MONTH--";
	String selectedDay = "0";
	String selectedYear = "0";
	
	//set attribute so that can use in selecting the dob
	request.setAttribute("selectedMonth", selectedMonth);
	request.setAttribute("selectedDay", selectedDay);
	request.setAttribute("selectedYear", selectedYear);
}

// Variables to store user's current policy information -> Coverage A to F, Deductible and Premium
String current_coverage_a = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_a, "coverage");
String current_coverage_b = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_b, "coverage");
String current_coverage_c = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_c, "coverage");
String current_coverage_d = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_d, "coverage");
String current_coverage_e = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_e, "coverage");
String current_coverage_f = getCurrentValueIfNotEmpty(userProfile, ViaConstant.coverage_f, "coverage");
String current_deductible = getCurrentValueIfNotEmpty(userProfile, ViaConstant.deductible, "coverage");
String current_premium = getCurrentValueIfNotEmpty(userProfile, ViaConstant.premium, "premium");

%>

<!-- Disabled the top white bar on page 3, 4 and 5 to make the UI simpler for the purpose of PoC demo on 12th August, 2020 -->
<!--<div class="container-fluid justify-content-center ">-->
<!--	<label class="text-center common-title font-weight-bold"><span style="font-size: 24px">Policy scan</span> | <span> <%out.print(address_1 + address_2 +"," + city + "," + state ); %></span>-->
<!--			<img src="img/QuestionMark.png" style="width: 20px; height: 20px; margin-left: 100px; margin-right: 10px"><span style="color: #00008F;" >Help</span>-->
<!--			<div>Cancel-->
<!--				<button type="button" class="close" aria-label="Close" style="height: 20px">Cancel<span aria-hidden="true" onclick="window.location.href='index.html'">&times;</span>-->
<!--				</button>-->
<!--			</div>-->
<!--		</label>-->
<!--</div>-->

<div class="container-fluid  navbar2">
	<div class="navbar2-item"></div>
	<div class="navbar2-item crr" onclick="window.location='./upload2'"><span>1</span>Upload Policy</div>
	<div class="navbar2-item"><span>2</span>Compare quotes</div>
	<div class="navbar2-item"><span>3</span>Switch and Save</div>
</div>

<div id="slide-out" class="side-nav left overlay">
	<ul class="slide-ul">
		<li class=" crr"><a class="nav-link" href="home.html">MENU ITEM 1</a></li>
		<li><a class="nav-link" href="about.html">MENU ITEM 2</a></li>
		<li><a class="nav-link" href="contacts.html">MENU ITEM 3</a></li>
		<li><a class="nav-link" href="contacts.html">MENU ITEM 4</a></li>
		<li><a class="nav-link" href="contacts.html">MENU ITEM 5</a></li>
	</ul>
</div>

<div class=" wrapper-content  clearfix" style="margin-top: 20px">
	<div class="container">
		<div class="block-heading text-center">
			<h2 class="text-center" style="font-size: 50px; font-weight: normal; font-family: Garamond, serif">
				Here’s what we found on your current policy </h2>
			<p style="font-size: 20px; font-family: 'Sans Source Pro', sans-serif">Confirm that everything looks good below. Your comparison is ready on the next page.</p>
			<!--<p style="font-size: 20px; font-family: 'Sans Source Pro', sans-serif">To make changes, edit the fields below.</p>-->

		</div>
	</div>
	<div class="container-fluid">
	<form  method="POST" action="./upload2compare" >
	<!-- <form  method="POST" action="../java/com/servlet/Upload2compareServlet" >-->
	
		<div style="color: #FF0000; background-color: #FFCCCB; text-align: center; font-weight: bold; font-size: 20px;font-family: 'Sans Source Pro', sans-serif;">${errorMessage}</div>
		<div class="card">
			<div class="tit">Personal Details</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<div class="input-wrap clearfix">
						<label class="mylabel">First Name</label>
						<span class="myspan"><%out.print(first_name); %></span>
						<input type="text" class="myinput" value="<%out.print(first_name); %>" name="form_first_name">
						
					</div>
					<div class="input-wrap clearfix">
						<label class="mylabel">Address</label>
						<span class="myspan"><%out.print(address_1); %></span>
						<input type="text" class="myinput" value="<%out.print(address_1); %>" name="form_address_1"/>
					</div>
					<div class="input-wrap clearfix">
						<label class="mylabel">State</label>
						<span class="myspan"><% out.print(state);%></span>
						<input type="text" class="myinput" value="<% out.print(state);%>" name="form_state"/>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<div class="input-wrap clearfix">
						<label class="mylabel">Last Name</label>
						<span class="myspan"><% out.print(last_name);%></span>
						<input type="text" class="myinput" value="<% out.print(last_name);%>" name="form_last_name"/>
					</div>
					<div class="input-wrap clearfix">
						<label class="mylabel">City</label>
						<span class="myspan"><% out.print(city);%></span>
						<input type="text" class="myinput" value="<% out.print(city); %>" name="form_city"/>
					</div>
					<div class="input-wrap clearfix">
						<label class="mylabel">Zip</label>
						<span class="myspan"><% out.print(zip);%></span>
						<input type="text" class="myinput" value="<% out.print(zip);%>" name="form_zip"/>
					</div>
				</div>
			</div>
			
			<!-- Removed the "EDIT INFORMATION" button on 08/04/20 -->
			<!--<div class="btn-wrap">
				<img src="img/gear_blue.png" class="gear" >
				 <span class="btn-edit">EDIT INFORMATION</span>
				<span class="btn-done">DONE</span>
			</div> -->
		</div>
		<div class="card">
			<div class="tit">Coverage Details</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<div class="input-wrap clearfix">
						<label class="mylabel">A. Dwelling</label>
						<span class="myspan"><% out.print(current_coverage_a); %></span>
						<input type="text" class="myinput" value="<% out.print(current_coverage_a); %>"/>
					</div>
					<div class="input-wrap clearfix">
						<label class="mylabel">B. Other Structures on Insured Location</label>
						<span class="myspan"><% out.print(current_coverage_b); %></span>
						<input type="text" class="myinput" value="<% out.print(current_coverage_b); %>"/>
					</div>
					<div class="input-wrap clearfix">
						<label class="mylabel">C. Personal Property</label>
						<span class="myspan"><% out.print(current_coverage_c); %></span>
						<input type="text" class="myinput" value="<% out.print(current_coverage_c); %>"/>
					</div>
					<div class="input-wrap clearfix">
						<label class="mylabel">D. Loss of Use of Insured Location</label>
						<span class="myspan"><% out.print(current_coverage_d); %></span>
						<input type="text" class="myinput" value="<% out.print(current_coverage_d); %>"/>
					</div>

				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<div class="input-wrap clearfix">
						<label class="mylabel">E. Personal Liability</label>
						<span class="myspan"><% out.print(current_coverage_e); %></span>
						<input type="text" class="myinput" value="<% out.print(current_coverage_e); %>"/>
					</div>
					<div class="input-wrap clearfix">
						<label class="mylabel">F. Medical Payments to Others</label>
						<span class="myspan"><% out.print(current_coverage_f); %></span>
						<input type="text" class="myinput" value="<% out.print(current_coverage_f); %>"/>
					</div>
					<div class="input-wrap clearfix">
						<label class="mylabel">Standard Deductible</label>
						<span class="myspan"><% out.print(current_deductible); %></span>
						<input type="text" class="myinput" value="<% out.print(current_deductible); %>"/>
					</div>
				</div>
			</div>
			<!-- Removed the "EDIT INFORMATION" button on 08/04/20 -->
			<!--<div class="btn-wrap">
				<img src="img/gear_blue.png" class="gear" >
				 <span class="btn-edit">EDIT INFORMATION</span>
				<span class="btn-done">DONE</span>
			</div> -->
		</div>
		<div class="card">
			<div class="card-header text-center person-header">We need some more information to finalize your quote
			</div>
			<div class="row" style="margin-top: 20px">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<div class="form-row">
						<div class="form-group">
							<label>Date of Birth (MM/DD/YYYY)</label>
							<div class="input-group">
							
								<select class="form-control" style="width: 120px" name="form_birthday_month" required>   
								    <option value='' selected>--Month--</option>
								    <c:forEach var="counter" begin="1" end="12">
								        <option value="${counter}" <c:if test="${counter == Integer.parseInt(selectedMonth) }"> selected </c:if>>${counter}</option>
								    </c:forEach>
								    
								</select>	
							
							
								<label class="input-group-text"> / </label>
								<select class="form-control" style="width: 110px" name="form_birthday_day" required>   
								    <option value='' selected>--Day--</option>
								    <c:forEach var="counter" begin="1" end="31">
								        <option value="${counter}" <c:if test="${counter == Integer.parseInt(selectedDay) }"> selected </c:if>>${counter}</option>
								    </c:forEach>
								</select>
								<label class="input-group-text"> / </label>
								<select class="form-control" style="width: 110px" name="form_birthday_year" required>   
								    <!-- If selected year is 0 i.e. nothing returned from api, default option is selected -->
								    <option value='' selected>--Year--</option>
								    <c:forEach var="counter" begin="1930" end="2020" step="1">
								     	<option value="${loop.end-counter+loop.begin}" <c:if test="${counter == Integer.parseInt(selectedYear) }"> selected </c:if>>${counter}</option>
								    </c:forEach>
								</select>
									</div>
						</div>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="text" class="form-control" placeholder="Email" style="width: 400px" value="<%if (email != null) out.print(email); %>" name="form_email" required>
						
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center" style="font-size: 20px; padding-top: 20px">
					<div>In order to provide you with the most accurate quote, we need your date of birth.
					</div>
				</div>
			</div>
		</div>
		<div class="btns-wrap row">
			<div class="ml-auto col-auto">
			
				
					<input class="btn-confirm" type="submit" value="COMPARE QUOTE" />
					
					
				
			</div>
		</div>
		</form>
	</div>

</div>
<br>
<div class="loader"></div>
</body>
<script>
    $(function () {
        var removeMenu = function (restoreNav) {
            $('body').css({
                overflow: '',
                width: ''
            });
            $('#sidenav-overlay').velocity({opacity: 0}, {
                duration: 200,
                queue: false, easing: 'easeOutQuad',
                complete: function () {
                    $(this).remove();
                }
            });
            $("#slide-out").velocity({'margin-left': [-1 * 250, 0]}, {
                duration: 200,
                queue: false,
                easing: 'easeOutCubic'
            });
            $(".menu-mobile-button").removeClass('is-open')
        };
        $(".menu-mobile-button").off("click").on("click", function () {
            $("#slide-out").velocity({'margin-left': [0, -1 * 250]}, {
                duration: 300,
                queue: false,
                easing: 'easeOutQuad'
            });
            var $body = $('body');
            var $overlay = $('<div id="sidenav-overlay"></div>');
            var oldWidth = $body.innerWidth();
            $body.css('overflow', 'hidden');
            $body.width(oldWidth);
            $overlay.css('opacity', 0)
                .click(function () {
                    removeMenu();
                    $overlay.velocity({opacity: 0}, {
                        duration: 300, queue: false, easing: 'easeOutQuad',
                        complete: function () {
                            $(this).remove();
                        }
                    });

                });
            $('body').append($overlay);
            $overlay.velocity({opacity: 0.7}, {
                duration: 300, queue: false, easing: 'easeOutQuad',
            });
            $(".menu-mobile-button").addClass('is-open')
        });

    })
</script>
</html>