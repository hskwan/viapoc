<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<title>Upload</title>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/styles.min.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<link rel="stylesheet" href="css/main.css">
	<style>
		body{
			font-family: 'Open Sans', sans-serif;
		}
		.box {
			font-size: 1.25rem; /* 20 */
			background-color: white;
			background: url(img/Path.png) ;
			position: relative;
			padding: 135px 60px;
			/*border-style: solid;*/
			/*border-width: 2px;*/
		}

		.box.has-advanced-upload {
			-webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
			transition: outline-offset .15s ease-in-out, background-color .15s linear;
		}

		.box.is-dragover {
			outline-offset: -20px;
			outline-color: #c8dadf;
			background-color: #fff;
		}

		.box__dragndrop,
		.box__icon {
			display: none;
		}

		.box.has-advanced-upload .box__dragndrop {
			display: inline;
		}

		.box.has-advanced-upload .box__icon {
			width: 100%;
			height: 80px;
			fill: #92b0b3;
			display: block;
		}

		.box.is-uploading .box__input,
		.box.is-success .box__input,
		.box.is-error .box__input {
			visibility: hidden;
		}

		.box__uploading,
		.box__success,
		.box__error {
			display: none;
		}

		.box.is-uploading .box__uploading,
		.box.is-success .box__success,
		.box.is-error .box__error {
			display: block;
			position: absolute;
			top: 50%;
			right: 0;
			left: 0;

			-webkit-transform: translateY(-50%);
			transform: translateY(-50%);
		}

		.box__uploading {
			font-style: italic;
		}

		.box__success {
			-webkit-animation: appear-from-inside .25s ease-in-out;
			animation: appear-from-inside .25s ease-in-out;
		}

		@-webkit-keyframes appear-from-inside {
			from {
				-webkit-transform: translateY(-50%) scale(0);
			}
			75% {
				-webkit-transform: translateY(-50%) scale(1.1);
			}
			to {
				-webkit-transform: translateY(-50%) scale(1);
			}
		}

		@keyframes appear-from-inside {
			from {
				transform: translateY(-50%) scale(0);
			}
			75% {
				transform: translateY(-50%) scale(1.1);
			}
			to {
				transform: translateY(-50%) scale(1);
			}
		}

		.box__restart {
			font-weight: 700;
		}

		.box__restart:focus,
		.box__restart:hover {
			color: #39bfd3;
		}
		

		.js .box__file {
			width: 0.1px;
			height: 0.1px;
			opacity: 0;
			overflow: hidden;
			position: absolute;
			z-index: -1;
		}

		.js .box__file + label {
			max-width: 80%;
			text-overflow: ellipsis;
			white-space: nowrap;
			cursor: pointer;
			display: inline-block;
			overflow: hidden;
		}

		.js .box__file + label:hover strong,
		.box__file:focus + label strong,
		.box__file.has-focus + label strong {
			color: #39bfd3;
		}

		.js .box__file:focus + label,
		.js .box__file.has-focus + label {
			outline: 1px dotted #000;
			outline: -webkit-focus-ring-color auto 5px;
		}

		.js .box__file + label * {
			/* pointer-events: none; */ /* in case of FastClick lib use */
		}

		.no-js .box__file + label {
			display: none;
		}

		.no-js .box__button {
			display: block;
		}
		
		.button2 {
  		background-color: rgba(0,0,0,0); 
  		color: blue; 
  		border: 2px solid blue;
 		text-align:center;
 		font-size: 20px;
  		position:relative;
  		width:200%;
  		right:60px;
		}
		

		.box__button {
			font-weight: 700;
			color: #e5edf1;
			background-color: #39bfd3;
			display: none;
			padding: 8px 16px;
			margin: 40px auto 0;
		}

		.box__button:hover,
		.box__button:focus {
			background-color: #0f3c4b;
		}
		input[type="file"] {
			display: none;
		}
		.custom-file-upload {
			border: 1px solid #ccc;
			display: inline-block;
			padding: 6px 12px;
			cursor: pointer;
		}
		
		.upload12 {
			border: 1px solid rgb(0,0,136);
			display: inline-block;
			color:rgb(0,0,136);
			padding: 6px 60px;
			margin-left:-25px;
			margin-top:15px;
			cursor: pointer;
		}
		
		.a7{
			font-size: 15px;
    		font-weight: bold;
    		color:#2425AA;
			margin-left:120px;
			cursor: pointer;
		}
		
		.points{
		display: inline;
    	height: 60px;
   		 width: 60px;
   		 padding-left:8px;
   		 padding-right:8px;
        line-height: 60px;
		 -moz-border-radius: 35px; /* or 50% */
    	 border-radius: 35px; /* or 50% */
    	 background-color: 	rgb(72,118,186);
    	color: white;
    	text-align: center;
    	font-size: 18px;
    	margin-left:-40px;
    	margin-right:10px;
    	position:relative;
    	top:-2px
		
		}

	</style>
	<script type="text/javascript">(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>

</head>

<body>


<div class="fixed-nav">

	<nav class="navbar navbar-static-top navbar-fixed-top py-2" role="navigation">
		<div class="navbar-header">
			<a target="_blank" href="" class="navbar-brand d-inline-block">
				<img src="img/logo.svg" style="padding: 5px">
			</a>
		</div>
		<div class="navbar-right clearfix">
			<ul class="nav navbar-nav">
				<li class="active"><a href="" class="anav" target="_blank">HOME <br>INSURANCE</a></li>
				<li><a href="javascript:" class="anav" target="_blank">RENTERS<br> INSURANCE</a></li>
				<li><a href="javascript:" class="anav" target="_blank">ALL <br>PRODUCTS</a></li>
			</ul>
			<div class="search">
				<div class="btn-search"></div>
			</div>
			<div class="menu-mobile-button  toggle-menu-button">
				<i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i>
			</div>
		</div>
	</nav>
</div>
<div id="slide-out" class="side-nav left overlay">
	<ul class="slide-ul">
		<li class=" crr"><a class="nav-link" href="home.html">HOME INSURANCE</a></li>
		<li><a class="nav-link" href="about.html">MENU ITEM 2</a></li>
		<li><a class="nav-link" href="contacts.html">MENU ITEM 3</a></li>
		<li><a class="nav-link" href="contacts.html">MENU ITEM 4</a></li>
		<li><a class="nav-link" href="contacts.html">MENU ITEM 5</a></li>
	</ul>
</div>

<div class="wrapper wrapper-content  clearfix ">
	<div class="container-fluid" style="margin-bottom:40px">
		<div class="row row1">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title1 justify-content-center">
				<p class="p2">Compare and Save </p>
				<p class="p3">Upload your current policy and we will compare it with our new<br>quote - saving you time and money.</p>
			</div>
		</div>
		<div class="container">
			<div class="row align-items-center" style="margin-bottom: 20px; margin-top: 20px">
				<div class="col-md-6">
					<div style="margin-right: 100px;margin-left: 100px;">
						<h3 style="font-weight: bold; font-size: 24px"><span class="points">1</span>Upload your policy details</h3>
						<p style="margin-top:-20px;font-size:17px">Your carrier most likely sent this to you via email when you purchased your policy.
						<br><br>The document should be called your 'policy summary' or 'declaration page'.</p>
					</div>
					<div style="margin-right: 100px;margin-left: 100px;">
						<h3 style="font-weight: bold; font-size: 24px"><span class="points">2</span>Compare your quotes</h3>
						<p style="margin-top:-20px;font-size:17px">Compare your current quote with what we have to offer.</p>
					</div>
					<div style="margin-right: 100px;margin-left: 100px;">
						<h3 style="font-weight: bold; font-size: 24px"><span class="points">3</span>Switch & Save</h3>
						<p style="margin-top:-20px;font-size:17px">If you like what you see, you can customize your policy and switch in 5 minutes or less.</p>
					</div>
				</div>
				<div class="col-md-6">
					<form method="post"
					      enctype="multipart/form-data" novalidate class="box" action="upload2">
						<div class="box__input">
							<img src="img/upload2page.svg" width="100px" height="123px" style="margin-left: 170px;margin-top:-20px;">
							<div style="position: absolute; left: 35%">
							<label  class="custom-filfilee-upload"><strong>Drag files here to upload</strong><span class="box__dragndrop"> </span></label><br>
							<input type="file" name="files[]" id="file" class="upload_button"  data-multiple-caption="{count} files selected" multiple/>
							<label  for="file" class="custom-filfilee-upload"><strong class="upload12">CHOOSE A FILE</strong><span class="box__dragndrop"> </span></label>
							
							</div>
						</div>
						<div class="box__uploading">Uploading&hellip;</div>
						<div class="box__success">Done! <a
								href="upload2"
								class="box__restart" role="button">Upload more?</a></div>
						<div class="box__error">Error! <span></span>. <a
								href="upload2" class="box__restart"
								role="button">Try
							again!</a></div>
					</form>
					<br>
					<a class="a7" > SHOW ME A QUOTE WITHOUT COMPARING<span> &rarr;</span>  </a> 
				</div>
			</div>
		</div>

	</div>

</div>
<div>
<!-- Site footer -->
    <footer class="site-footer">
        <div class="row">
          <div class="col-xs-6 col-md-3">
            <h6>QUICK ACCESS</h6>
            <ul class="footer-links">
              <li><a href="#">Call Us</a></li>
              <li><a href="#">Policy Scan</a></li>
              <li><a href="#">Quote Review</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>LOREM</h6>
            <ul class="footer-links">
              <li><a href="#">About Us</a></li>
              <li><a href="#">Careers</a></li>
              <li><a href="#">Policy Documents</a></li>
            </ul>
          </div>
          
           <div class="col-xs-6 col-md-3">
            <h6>USEFUL LINKS</h6>
            <ul class="footer-links">
              <li><a href="#">Blog</a></li>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Download App</a></li>
            </ul>
          </div>
          
          <div class="col-xs-6 col-md-3">
            <h6>FOLLOW LOREM</h6>
            <ul class="social-icons">
            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
             <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>  
            <li><a class="youtube" href="#"><i class="fa fa-youtube-play"></i></a></li>
            <li><a class="instagram" href="#"><i class="fa fa-instagram"></i></a></li>  
            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
            </ul>
          </div>
          
          
        </div>
        <hr>
            <p class="copyright-text"> &copy; 2020 Lorem. All Rights Reserved
            </p>
</footer>
</div>
<script type="text/javascript">

    'use strict';

    ( function ( document, window, index )
    {
        // feature detection for drag&drop upload
        var isAdvancedUpload = function()
        {
            var div = document.createElement( 'div' );
            return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
        }();


        // applying the effect for every form
        var forms = document.querySelectorAll( '.box' );
        Array.prototype.forEach.call( forms, function( form )
        {
            var input		 = form.querySelector( 'input[type="file"]' ),
                label		 = form.querySelector( 'label' ),
                errorMsg	 = form.querySelector( '.box__error span' ),
                restart		 = form.querySelectorAll( '.box__restart' ),
                droppedFiles = false,
                showFiles	 = function( files )
                {
                    label.textContent = files.length > 1 ? ( input.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', files.length ) : files[ 0 ].name;
                },
                triggerFormSubmit = function()
                {
                    var event = document.createEvent( 'HTMLEvents' );
                    event.initEvent( 'submit', true, false );
                    form.dispatchEvent( event );
                };

            // letting the server side to know we are going to make an Ajax request
            var ajaxFlag = document.createElement( 'input' );
            ajaxFlag.setAttribute( 'type', 'hidden' );
            ajaxFlag.setAttribute( 'name', 'ajax' );
            ajaxFlag.setAttribute( 'value', 1 );
            form.appendChild( ajaxFlag );

            // automatically submit the form on file select
            input.addEventListener( 'change', function( e )
            {
                showFiles( e.target.files );


                triggerFormSubmit();


            });

            // drag&drop files if the feature is available
            if( isAdvancedUpload )
            {
                form.classList.add( 'has-advanced-upload' ); // letting the CSS part to know drag&drop is supported by the browser

                [ 'drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop' ].forEach( function( event )
                {
                    form.addEventListener( event, function( e )
                    {
                        // preventing the unwanted behaviours
                        e.preventDefault();
                        e.stopPropagation();
                    });
                });
                [ 'dragover', 'dragenter' ].forEach( function( event )
                {
                    form.addEventListener( event, function()
                    {
                        form.classList.add( 'is-dragover' );
                    });
                });
                [ 'dragleave', 'dragend', 'drop' ].forEach( function( event )
                {
                    form.addEventListener( event, function()
                    {
                        form.classList.remove( 'is-dragover' );
                    });
                });
                form.addEventListener( 'drop', function( e )
                {
                    droppedFiles = e.dataTransfer.files; // the files that were dropped
                    showFiles( droppedFiles );


                    triggerFormSubmit();

                });
            }


            // if the form was submitted
            form.addEventListener( 'submit', function( e )
            {
                // preventing the duplicate submissions if the current one is in progress
                if( form.classList.contains( 'is-uploading' ) ) return false;

                form.classList.add( 'is-uploading' );
                form.classList.remove( 'is-error' );
                form.submit();

            });


            // restart the form if has a state of error/success
            Array.prototype.forEach.call( restart, function( entry )
            {
                entry.addEventListener( 'click', function( e )
                {
                    e.preventDefault();
                    form.classList.remove( 'is-error', 'is-success' );
                    input.click();
                });
            });

            // Firefox focus bug fix for file input
            input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
            input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });

        });
    }( document, window, 0 ));

</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="9c33a94d455af7f0ec378514-|49" defer=""></script></body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="assets/js/script.min.js"></script>
</body>

</html>
